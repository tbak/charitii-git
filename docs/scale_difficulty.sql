THE CODE TO CALL IN PHPMYSQL IS IN ADMIN/CHECK_DIFFICULTY.PHP
   LOOK FOR:
   (FEB 4, 2009: THIS IS THE ONE TO USE)


DELIMITER $$
DROP PROCEDURE IF EXISTS `scale_difficulty`$$
CREATE PROCEDURE `scale_difficulty`()
BEGIN

	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE o INT;
	DECLARE ct FLOAT;
	DECLARE factor FLOAT;
	DECLARE cur CURSOR	
	FOR
	SELECT id FROM puzzles ORDER BY difficulty_calculated;

	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done=1;

	SET factor = 60 / 10000;
	SET ct = 0;

	OPEN cur;

	-- Loop through all rows
	REPEAT

		-- Get order number
		FETCH cur INTO o;

		UPDATE puzzles set difficulty_scaled = ct*factor WHERE id = o;

		SET ct = ct+1;

	-- End of loop
	UNTIL done END REPEAT;

	-- Close the cursor
	CLOSE cur;
    END$$

DELIMITER ;