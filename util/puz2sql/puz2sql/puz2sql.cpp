// puz2sql.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "puzfile.h"


#include <stdio.h>
#define W32_LEAN_AND_MEAN
#include <winsock2.h>
#include <time.h>
#include "my_global.h"
#include "mysql.h"

// change these to suit your setup
#define TABLE_OF_INTEREST "puzzles"
#define SERVER_NAME "localhost"
#define DB_USER "puz"
#define DB_USERPASS "puz"
#define DB_NAME "puz" 

int InsertPuzFileIntoDatabase();
int OpenDatabaseConnection( MYSQL **hnd );
void CloseDatabaseConnection( MYSQL *hnd );
bool IsClueSolutionPairInDatabase( MYSQL *hnd, int index );
void FormAddDateString( char *pAddDate, int stringSize );
void FormSourceDateString( char *pSourceDate, int stringSize );
int AddClueSolutionPairToDatabase( MYSQL *hnd, int index, const char *pSourceDate, const char *pAddDate );

// Old
/*
void ShowTables(MYSQL*);
void ShowContents(MYSQL*,const char*);
void TestSQL();
*/

// I'm so bloody lazy...
const char *gpFileName;

int _tmain(int argc, const char* argv[])
{
	//TestSQL();
	
	if (argc < 2) {
		printf("Usage: Acl2TeX <puzzle file>\n");
		return 1;
	}

	PuzFile::TestOpenFile( argv[1] );

	// Strip away path crap
	// We store the base filename in the database
	gpFileName = argv[1];
	const char *pF = gpFileName;
	while( pF = strstr(pF, "\\") )
	{
		pF++;
		gpFileName = pF;
	}

	return InsertPuzFileIntoDatabase();
}

int OpenDatabaseConnection( MYSQL **hnd )
{
	const char *sinf=NULL; // mysql server information

	*hnd = mysql_init(NULL);
	if (NULL == mysql_real_connect(*hnd,SERVER_NAME,DB_USER,DB_USERPASS,DB_NAME,0,NULL,0))
	{
		fprintf(stderr,"Problem encountered connecting to the %s database on %s.\n",DB_NAME,SERVER_NAME);
		return -1;
	}

	//fprintf(stdout,"Connected to the %s database on %s as user '%s'.\n",DB_NAME,SERVER_NAME,DB_USER);
	sinf = mysql_get_server_info(*hnd);

	if (sinf == NULL)
	{
		fprintf(stderr,"Failed to retrieve the server information string.\n");
		mysql_close(*hnd);
		return -1;
	}

	//fprintf(stdout,"Got server information: '%s'\n",sinf);

	return 0;
}

void CloseDatabaseConnection( MYSQL *hnd )
{
	mysql_close(hnd);
}

bool IsClueSolutionPairInDatabase( MYSQL *hnd, int index )
{
	MYSQL_RES *res=NULL;

	const int SQL_STATEMENT_SIZE = 1024;
	char sql[SQL_STATEMENT_SIZE];

	sprintf_s(sql, SQL_STATEMENT_SIZE, "select * from puzzles where clue = '%s'  AND answer = '%s' limit 1", PuzFile::GetClueSolutionPair(index)->clue, PuzFile::GetClueSolutionPair(index)->solution );
	//sprintf_s(sql, SQL_STATEMENT_SIZE, "select clue from puzzles" );
	//fprintf(stdout,"Using sql statement: '%s' to extract all rows from the specified table.\n",sql);

	if (!mysql_query(hnd,sql))
	{
		// TB - Use mysql_store_result() instead of mysql_use_result() so that num_rows is immediately available...
		res = mysql_store_result(hnd);
		if (res)
		{
			if( mysql_num_rows(res) > 0 )
			{
				mysql_free_result(res);
			
				//printf("Dupe: %s -- %s", PuzFile::GetClueSolutionPair(index)->clue, PuzFile::GetClueSolutionPair(index)->solution );

				return true;
			}
			mysql_free_result(res);
		}
	}
	else
	{
		fprintf(stderr,"Something hosed: (%s)(%s)\n", mysql_error(hnd), sql );
	}

	return false;
}

int AddClueSolutionPairToDatabase( MYSQL *hnd, int index, const char *pSourceDate, const char *pAddDate, char *pGiantSql, int giantSqlSize )
{
	static int numAdded = 0;

	if( strstr(PuzFile::GetClueSolutionPair(index)->clue, "-Across") != NULL ||
		strstr(PuzFile::GetClueSolutionPair(index)->clue, "-across") != NULL )
	{
		//printf(" (ACROSS) ");
		return 0;
	}
	if( strstr(PuzFile::GetClueSolutionPair(index)->clue, "-Down") != NULL ||
		strstr(PuzFile::GetClueSolutionPair(index)->clue, "-down") != NULL )
	{
		//printf(" (DOWN) ");
		return 0;
	}

	if( strstr(PuzFile::GetClueSolutionPair(index)->clue, "this puzzle") != NULL ||
		strstr(PuzFile::GetClueSolutionPair(index)->clue, "This puzzle") != NULL )
	{
		//printf(" (DOWN) ");
		return 0;
	}

	// Faster - Do this with one SQL comand after all entries are added...?
	//
	// Get rid of dupes with this:
	// ALTER IGNORE TABLE puzzles ADD UNIQUE INDEX(clue,answer);
	// Just keep the unique index, and then just use INSERT IGNORE to not insert dupes! OK!
	// ALTER TABLE puzzles DROP index clue;
	// ALTER TABLE puzzles DROP index answer;
	// Test with this:
	// SELECT *, count(*) cnt FROM puzzles GROUP BY clue, answer HAVING cnt > 1;
	/*
	if( IsClueSolutionPairInDatabase(hnd,index) )
	{
		//printf("Skipping because clue/solution pair is already in database...\n");
		printf(" (DUPE) ");
		return 0;
	}
	*/

	//const int SQL_STATEMENT_SIZE = 1024;
	//char sql[SQL_STATEMENT_SIZE];

	// Enabled by default for now... but eventually new stuff should be disabled by default...
	// Now everything is disabled by default!
	int enabled = 0;

	//sprintf_s(sql, SQL_STATEMENT_SIZE, "INSERT INTO puzzles (clue, answer, source_date, add_date, source_name, file_name, difficulty_original, enabled, answer_strlen) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %d, %d, %d )", PuzFile::GetClueSolutionPair(index)->clue, PuzFile::GetClueSolutionPair(index)->solution, pSourceDate, pAddDate, PuzFile::sourceName, gpFileName, PuzFile::difficulty, enabled, strlen(PuzFile::GetClueSolutionPair(index)->solution) );
	sprintf_s( pGiantSql + strlen(pGiantSql), giantSqlSize-strlen(pGiantSql), "%c('%s', '%s', '%s', '%s', '%s', '%s', %d, %d, %d )", numAdded==0?' ':',', PuzFile::GetClueSolutionPair(index)->clue, PuzFile::GetClueSolutionPair(index)->solution, pSourceDate, pAddDate, PuzFile::sourceName, gpFileName, PuzFile::difficulty, enabled, strlen(PuzFile::GetClueSolutionPair(index)->solution) );

	// Just append to the giant SQL string, and then insert everything in 1 go. It's a lot faster...
	/*
	if( mysql_query(hnd,sql) )
	{
		fprintf(stderr,"Failed to insert. Ensure table is valid! (%s)\n", mysql_error(hnd) );
		return -1;
	}
	else
	{
		printf( "%d - ", index );
	}
	*/
	//printf("%d - ", index);
	numAdded++;

	return 0;
}

void FormAddDateString( char *pAddDate, int stringSize )
{
	time_t t = time(NULL);
	tm goodTime;
	localtime_s(&goodTime, &t);

	sprintf_s( pAddDate, stringSize, "%d-%d-%d %d:%d:%d", 1900+goodTime.tm_year, goodTime.tm_mon+1, goodTime.tm_mday, goodTime.tm_hour, goodTime.tm_min, goodTime.tm_sec );
}

void FormSourceDateString( char *pSourceDate, int stringSize )
{
	tm *pGoodTime = &PuzFile::sourceTime;

	sprintf_s( pSourceDate, stringSize, "%d-%d-%d %d:%d:%d", 1900+pGoodTime->tm_year, pGoodTime->tm_mon+1, pGoodTime->tm_mday, pGoodTime->tm_hour, pGoodTime->tm_min, pGoodTime->tm_sec );
}

int InsertPuzFileIntoDatabase()
{
	MYSQL *hnd=NULL; // mysql connection handle

	if( OpenDatabaseConnection(&hnd) )
	{
		return -1;
	}

	const int DATE_STRING_SIZE = 50;
	char addDate[DATE_STRING_SIZE];
	FormAddDateString( addDate, DATE_STRING_SIZE );

	char sourceDate[DATE_STRING_SIZE];
	FormSourceDateString( sourceDate, DATE_STRING_SIZE );

	const int GIANT_SQL_SIZE = 50000;
	char giantSQLString[50000];

	// form the start of the giant insert statement
	// Use IGNORE so that dupes are silently ignored without hosing the entire SQL statement.
	sprintf_s(giantSQLString, GIANT_SQL_SIZE, "INSERT IGNORE INTO puzzles (clue, answer, source_date, add_date, source_name, file_name, difficulty_original, enabled, answer_strlen) VALUES ");

	for( int i = 0; i < PuzFile::GetNumClueSolutionPairs(); i++ )
	{
		if( AddClueSolutionPairToDatabase(hnd,i,sourceDate,addDate,giantSQLString,GIANT_SQL_SIZE) )
		{
			return -1;
		}
	}

	// Insert everything in one go
	//printf("%s", giantSQLString);
	if( mysql_query(hnd,giantSQLString) )
	{
		fprintf(stderr,"Failed to insert. Ensure table is valid! (%s)\n", mysql_error(hnd) );
		return -1;
	}

	return 0;
}



/*
void TestSQL()
{
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information

	hnd = mysql_init(NULL);
	if (NULL == mysql_real_connect(hnd,SERVER_NAME,DB_USER,DB_USERPASS,DB_NAME,0,NULL,0))
	{
		fprintf(stderr,"Problem encountered connecting to the %s database on %s.\n",DB_NAME,SERVER_NAME);
	}
	else
	{
		fprintf(stdout,"Connected to the %s database on %s as user '%s'.\n",DB_NAME,SERVER_NAME,DB_USER);
		sinf = mysql_get_server_info(hnd);

		if (sinf != NULL)
		{
			fprintf(stdout,"Got server information: '%s'\n",sinf);
			ShowTables(hnd);
			ShowContents(hnd,TABLE_OF_INTEREST);
		}
		else
		{
			fprintf(stderr,"Failed to retrieve the server information string.\n");
		}

		mysql_close(hnd);
	}

}

void ShowTables(MYSQL *handle)
{
	MYSQL_RES *result=NULL; // result of asking the database for a listing of its tables
	MYSQL_ROW row; // one row from the result set

	result = mysql_list_tables(handle,NULL);
	row = mysql_fetch_row(result);
	fprintf(stdout,"Tables found:\n\n");
	while (row)
	{
		fprintf(stdout,"\t%s\n",row[0]);
		row = mysql_fetch_row(result);
	}
	mysql_free_result(result);

	fprintf(stdout,"\nEnd of tables\n");
}

void ShowContents( MYSQL *handle, const char *tbl )
{
	MYSQL_RES *res=NULL; // result of querying for all rows in table
	MYSQL_ROW row; // one row returned
	const int sqlStatementSize = 1024;
	char sql[sqlStatementSize], // sql statement used to get all rows
	commastr[2]; // to put commas in the output
	int i,numf=0; // number of fields returned from the query

	sprintf_s(sql, sqlStatementSize, "select * from %s",tbl);
	fprintf(stdout,"Using sql statement: '%s' to extract all rows from the specified table.\n",sql);

	if (!mysql_query(handle,sql))
	{
		res = mysql_use_result(handle);
		if (res)
		{
			numf = mysql_num_fields(res);
			row = mysql_fetch_row(res);
			fprintf(stdout,"Rows returned:\n\n");
			while (row)
			{
				commastr[0]=commastr[1]=(char)NULL;
				for (i=0;i<numf;i++)
				{
					if (row[i] == NULL)
					{
						fprintf(stdout,"%sNULL",commastr);
					}
					else
					{
						fprintf(stdout,"%s%s",commastr,row[i]);
					}
					commastr[0]=',';
				}
				fprintf(stdout,"\n");

				row = mysql_fetch_row(res);
			}
			fprintf(stdout,"\nEnd of rows\n");

			mysql_free_result(res);
		}
		else
		{
			fprintf(stderr,"Failed to use the result acquired!\n");
		}
	}
	else
	{
		fprintf(stderr,"Failed to execute query. Ensure table is valid!\n");
	}

} 
*/