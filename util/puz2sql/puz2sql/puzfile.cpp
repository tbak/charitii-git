
#include "stdafx.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>  
#include <ctype.h> 
#define ROWSIZE 256

FILE *infile;
int width, height, j, k, ccount, mcount, anum, dnum;
//int apos, dpos;

char inbuf[ROWSIZE];

/* Here is a lazy programmer's waste of space. */
char aspace[ROWSIZE][ROWSIZE];
char dspace[ROWSIZE][ROWSIZE];

const int ROWTEXT_MAX_SIZE = 10 * ROWSIZE;
char rowtext[ROWTEXT_MAX_SIZE];
char tmp[ROWSIZE];

const int MATRIX_MAX_SIZE = ROWSIZE * ROWSIZE * 10;
char matrix[MATRIX_MAX_SIZE];
//const int ADCLUES_MAX_SIZE = ROWSIZE * 15 * 16;
//char aclues[ADCLUES_MAX_SIZE];
//char dclues[ADCLUES_MAX_SIZE];

// TB

#include "puzfile.h"

PuzFile::ClueSolutionPair PuzFile::clueSolutionPairs[MAX_CLUE_SOLUTION_PAIRS];
int PuzFile::numClueSolutionPairs;
char PuzFile::title[INFO_MAX_SIZE];
char PuzFile::author[INFO_MAX_SIZE];
char PuzFile::copyright[INFO_MAX_SIZE];
tm PuzFile::sourceTime;
char PuzFile::sourceName[INFO_MAX_SIZE];
int PuzFile::difficulty;

void PuzFile::TestOpenFile( const char *pFileName )
{

	numClueSolutionPairs = 0;

  fopen_s(&infile, pFileName, "rb");
  if (infile == NULL) {
	printf("Error opening %s.\n");
	return;
  }

/* From here on we're assuming a non-corrupt puzzle file.
   An enhancement later could be to check for this. */

/* Read in the puzzle size, skipping the stuff 
   we don't understand. */
   fread(inbuf, 46, 1, infile);
   width =  (int)(inbuf[44]);
   height = (int)(inbuf[45]);
     
/* Read in the solution, assuming(!) it is not scrambled.
   This may be a bad idea but we do it anyhow. */
   fread(inbuf, 6, 1, infile);  /* more basura */
   for (j=0; j<height; j++) fread(&aspace[j][0], width, 1, infile);

/* Read in the diagram info separately.  It's safer to use
   this to deduce the numbering scheme. */
   for (j=0; j<height; j++) fread(&dspace[j][0], width, 1, infile);

   /* Now start to separate some of the text headers. */
   //apos=dpos=0;
   fgetaline(title, infile, INFO_MAX_SIZE);
   fgetaline(author, infile, INFO_MAX_SIZE);
   fgetaline(copyright, infile, INFO_MAX_SIZE);

/* Now we are ready to produce the LaTeX stuff for the
   puzzle diagram.  Again we are assuming we have an unscrambled
   solution.  How can we know this?  Put on TODO list. */

/* We write to standard out.  Lazy but effective. */

   /*
   printf("\\documentclass{article}\n");
   printf("\\usepackage{cwpuzzle}\n");
   printf("\\title{%s}\n", title);
   printf("\\author{%s}\n", author);
   printf("\\date{%s}\n", copyright);
   printf("\\voffset -1in\n");
   printf("\\hoffset -1in\n");
   printf("\\begin{document}\n");
   */
  
   // TB - Is it a source we know?
   bool isNYTimes = false;
   strcpy_s( sourceName, INFO_MAX_SIZE, "unknown" );
   if( strstr(title, "NY Times") != NULL )
   {
	   strcpy_s( sourceName, INFO_MAX_SIZE, "NYT" );
	   isNYTimes = true;
   }

	sourceTime.tm_wday = 0;
	sourceTime.tm_mday = 0;
	sourceTime.tm_year = 0;
	sourceTime.tm_mon = 0;

	if( strstr(title, "Jan") != NULL )
	{
		sourceTime.tm_mon = 0;
	}
	else if( strstr(title, "Feb") != NULL )
	{
		sourceTime.tm_mon = 1;
	}
	else if( strstr(title, "Mar") != NULL )
	{
		sourceTime.tm_mon = 2;
	}
	else if( strstr(title, "Apr") != NULL )
	{
		sourceTime.tm_mon = 3;
	}
	else if( strstr(title, "May") != NULL )
	{
		sourceTime.tm_mon = 4;
	}
	else if( strstr(title, "Jun") != NULL )
	{
		sourceTime.tm_mon = 5;
	}
	else if( strstr(title, "Jul") != NULL )
	{
		sourceTime.tm_mon = 6;
	}
	else if( strstr(title, "Aug") != NULL )
	{
		sourceTime.tm_mon = 7;
	}
	else if( strstr(title, "Sep") != NULL )
	{
		sourceTime.tm_mon = 8;
	}
	else if( strstr(title, "Oct") != NULL )
	{
		sourceTime.tm_mon = 9;
	}
	else if( strstr(title, "Nov") != NULL )
	{
		sourceTime.tm_mon = 10;
	}
	else if( strstr(title, "Dec") != NULL )
	{
		sourceTime.tm_mon = 11;
	}

    // TB - Try to get the source date from the title string
	if( strstr(title, "Sun") != NULL )
	{
		sourceTime.tm_wday = 0;
	}
	else if( strstr(title, "Mon") != NULL )
	{
		sourceTime.tm_wday = 1;
	}
	else if( strstr(title, "Tue") != NULL )
	{
		sourceTime.tm_wday = 2;
	}
	else if( strstr(title, "Wed") != NULL )
	{
		sourceTime.tm_wday = 3;
	}
	else if( strstr(title, "Thu") != NULL )
	{
		sourceTime.tm_wday = 4;
	}
	else if( strstr(title, "Fri") != NULL )
	{
		sourceTime.tm_wday = 5;
	}
	else if( strstr(title, "Sat") != NULL )
	{
		sourceTime.tm_wday = 6;
	}
   
	const char *pYearStart = strstr(title, "19");
	if( pYearStart != NULL && (*(pYearStart+2) >= '0' && *(pYearStart+2) <= '9') )
	{
		sourceTime.tm_year = (*(pYearStart+2)-'0')*10 + *(pYearStart+3)-'0';
	}
	else
	{
		const char *pYearStart = strstr(title, "20");
		if( pYearStart != NULL && (*(pYearStart+2) >= '0' && *(pYearStart+2) <= '9') )
		{
			sourceTime.tm_year = 100 + (*(pYearStart+2)-'0')*10 + *(pYearStart+3)-'0';
		}
	}

	// NYTimes special difficulty processing
	if( isNYTimes )
	{
		if( sourceTime.tm_wday == 0 )
		{
			// Sunday = mixed bag
			difficulty = 35;
		}
		else
		{
			difficulty = sourceTime.tm_wday * 10;
		}

	}


	bool lastWasSpace = true;
	char *pChar = title;
	char number[3];
	number[0] = 0;
	number[1] = 0;
	number[2] = 0;
	int numSize =0;
	while( *pChar )
	{

		if( lastWasSpace == true )
		{
			if( *pChar >= '0' && *pChar <= '9' )
			{
				if( numSize == 2 )
				{
					lastWasSpace = false;
					number[0] = '\n';
					number[1] = '\n';
				}
				else
				{
					number[numSize] = *pChar;
					numSize++;
				}
			}
		}

		if( *pChar == ' ' || *pChar == '\\' || *pChar == '/' || *pChar == ',' )
		{
			if( numSize == 2 )
			{
				// Found a 2 digit number!
				sourceTime.tm_mday = atoi(number);
				break;
			}
			lastWasSpace = true;
			numSize = 0;
		}

		pChar++;
	}

   ccount = 0;
   mcount = 0;

   // TB
   char soltmp[MAX_CLUE_STRING_SIZE];

   for (j=0; j<height; j++) {
	strcpy_s(rowtext,ROWTEXT_MAX_SIZE,"\0");
	for (k=0; k<width; k++) {
		/* Analyze position for across-number */
		/* Left edge non-black followed by non-black */
		anum = 0;
		if (  (k==0 && dspace[j][k]=='-' && dspace[j][k+1]=='-') ||
		/* Previous black - nonblack - nonblack */
		 (  (k+1)<width && (k-1)>=0 && dspace[j][k]=='-' 
                              && dspace[j][k-1]=='.'
                              && dspace[j][k+1]=='-'  )
                ){
			ccount++;   
			anum = ccount;
                 }
		/* Analyze position for down-number */
		dnum = 0;
		/* Top row non-black followed by non-black */
		if (  (j==0 && dspace[j][k]=='-' && dspace[j+1][k]=='-') ||
		/* Black above - nonblack - nonblack below */
		 (  (j-1)>=0 && (j+1)<height && dspace[j][k]=='-'
                               && dspace[j-1][k]=='.'
                               && dspace[j+1][k]=='-' )
                ){
			/* Don't double number the same space */
			if (anum == 0) ccount++;
			dnum = ccount;
		 }
		/* Now, if necessary, pick up some clues and save them. */
		if (anum != 0) 	
		{
			bool wordIsValid = true;

			fgetaline(tmp, infile, ROWSIZE);
			//sprintf_s(&aclues[apos], ADCLUES_MAX_SIZE-apos, "\\Clue{%d}{}{%s}\\\\\n", anum, tmp);
			//apos = (int)strlen(aclues);

			// Filter out non-english
			// The clue can have special characters... PHP's utf8_encode will fix this...
			// But the answers can not since PHP will screw up strlen and adding blanks...
			/*
			int nonEngLen = (int)strlen(tmp);
			for( int nonEngPos = 0; nonEngPos < nonEngLen; nonEngPos++ )
			{
				if( tmp[nonEngPos] < 32 || tmp[nonEngPos] > 126 )
				{
					wordIsValid = false;
				}
			}
			*/

			// TB
			int solpos = 0;
			while( k+solpos < width && dspace[j][k+solpos] == '-' )
			{
				// Check for non english letters. I don't feel like dealing with it.
				if( aspace[j][k+solpos] < 32 || aspace[j][k+solpos] > 126 )
				{
					wordIsValid = false;
				}
					
				// TB TODO - Do I want all caps or mixed case or what?
				soltmp[solpos] = toupper(aspace[j][k+solpos]);
				//soltmp[solpos] = solpos == 0 ? aspace[j][k+solpos] : tolower(aspace[j][k+solpos]);
				solpos++;
			}
			soltmp[solpos] = '\0';

			if( wordIsValid )
			{
				strcpy_s( clueSolutionPairs[numClueSolutionPairs].clue, MAX_CLUE_STRING_SIZE, tmp );
				strcpy_s( clueSolutionPairs[numClueSolutionPairs].solution, MAX_CLUE_STRING_SIZE, soltmp );
				numClueSolutionPairs++;
			}
			else
			{
				printf("Invalid: %s", soltmp );
			}
		}
		if (dnum != 0)  {
			bool wordIsValid = true;

			fgetaline(tmp, infile, ROWSIZE);  
			//sprintf_s(&dclues[dpos], ADCLUES_MAX_SIZE-dpos, "\\Clue{%d}{}{%s}\\\\\n", dnum, tmp);
			//dpos = (int)strlen(dclues);

			// Filter out non-english
			// The clue can have special characters... PHP's utf8_encode will fix this...
			// But the answers can not since PHP will screw up strlen and adding blanks...
			/*
			int nonEngLen = (int)strlen(tmp);
			for( int nonEngPos = 0; nonEngPos < nonEngLen; nonEngPos++ )
			{
				if( tmp[nonEngPos] < 32 || tmp[nonEngPos] > 126 )
				{
					wordIsValid = false;
				}
			}
			*/

			// TB
			int solpos = 0;
			while( j+solpos < height && dspace[j+solpos][k] == '-' )
			{
				// Check for non english letters. I don't feel like dealing with it.
				if( aspace[j+solpos][k] < 32 || aspace[j+solpos][k] > 126 )
				{
					wordIsValid = false;
				}

				// TB TODO - Do I want all caps or mixed case or what?
				soltmp[solpos] = toupper(aspace[j+solpos][k]);
				//soltmp[solpos] = solpos == 0 ? aspace[j+solpos][k] : tolower(aspace[j+solpos][k]);
				solpos++;
			}
			soltmp[solpos] = '\0';

			if( wordIsValid )
			{
				strcpy_s( clueSolutionPairs[numClueSolutionPairs].clue, MAX_CLUE_STRING_SIZE, tmp );
				strcpy_s( clueSolutionPairs[numClueSolutionPairs].solution, MAX_CLUE_STRING_SIZE, soltmp );
				numClueSolutionPairs++;
			}
			else
			{
				printf("Invalid: %s", soltmp );
			}
		}
		/* Concat the current info to the current row. */
		strcat_s(rowtext, ROWTEXT_MAX_SIZE, "|");
		if (dspace[j][k] == '.') strcat_s(rowtext, ROWTEXT_MAX_SIZE, "*");
		else {
			/* see if we need a grid number */
			if (dnum != 0) 	{
				sprintf_s(tmp, ROWSIZE, "[%d]", dnum);
				strcat_s(rowtext, ROWTEXT_MAX_SIZE, tmp);
			}
			else if (anum != 0)  {
				sprintf_s(tmp, ROWSIZE, "[%d]", anum); 
				strcat_s(rowtext, ROWTEXT_MAX_SIZE, tmp);
			}
			/* Put in solution letter */
			strncat_s(rowtext, ROWTEXT_MAX_SIZE, &aspace[j][k], 1);
		}
        }
	/* End of the row.  Put in marker and output it. */
	strcat_s(rowtext, ROWTEXT_MAX_SIZE, "|.");
	sprintf_s(&matrix[mcount], MATRIX_MAX_SIZE-mcount, "%s\n", rowtext);
	mcount = (int)strlen(matrix);
   }
   /* That's it for the puzzle. */

   /*
   // Now output the clue section
   printf("\\begin{PuzzleClues}{\\textbf{Across}}\\\\\n");
   printf("%s", aclues);
   printf("\\end{PuzzleClues}\n");
   printf("\\begin{PuzzleClues}{\\textbf{Down}}\\\\\n");
   printf("%s", dclues);
   printf("\\end{PuzzleClues}\n");

   // Now output the diagram
   printf("\\newpage\n");
   printf("\\maketitle\n");
   printf("\\begin{Puzzle}{%d}{%d}\n", width, height);
   printf("%s", matrix);
   printf("\\end{Puzzle}\n");

   
   printf("\\end{document}\n");
   */

	
	// TB
   /*
	for( int i = 0; i < numClueSolutionPairs; i++ )
	{
		printf( "%s - %s\n", clueSolutionPairs[i].solution, clueSolutionPairs[i].clue );
	}
	*/
	   

}

void PuzFile::fgetaline(char *foo, FILE *infile, int fooLength ) 
{

	//unsigned char i;
	int numRead = 0;
	char i;
	int  oe;
	oe = 0;
	strcpy_s(foo, fooLength, "\0");
	while (1) 
	{
		fread(&i, 1, 1, infile);
		numRead++;

		if( numRead >= fooLength )
		{
			// TB TODO - This is lazy... Fix this properly? 
			// Just bail out if a string is too long...
			exit(0);
		}

		if (feof(infile) || (i=='\0')) break;
		if (i == 169) {
			strcat_s(foo, fooLength, "\\copyright ");
		}
		else 
		{
			if (i == '_' || i == '&' || i == '#' || i== '$')
			{
				// TB - Wtf is this?
				//strcat(foo, "\\");
			}
			// TB - Space out dashes a little?
			if( i == '_' )
			{
				strcat_s(foo, fooLength, " ");
			}
			if (i == '\'') 
			{
				strcat_s(foo, fooLength, "\'\'");
			}
			else if (i == '"') 
			{
				strcat_s(foo, fooLength, "\"");
				/*
				if ( oe == 0) {
					oe = 1;
					strcat(foo, "``");
				}
				else {
					oe = 0;
					strcat(foo, "''");
				}
				*/
			}       
			else 
			{
				strncat_s(foo, fooLength, &i, 1);
			}
		}
    }
}



