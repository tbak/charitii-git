/*
PuzFile.h
*/

#ifndef _PUZFILE_H_
#define _PUZFILE_H_

#define MAX_CLUE_SOLUTION_PAIRS 256
#define MAX_CLUE_STRING_SIZE 256
#define INFO_MAX_SIZE 80

#include "time.h"

class PuzFile
{
public:

	struct ClueSolutionPair
	{
		char clue[MAX_CLUE_STRING_SIZE];
		char solution[MAX_CLUE_STRING_SIZE];
	};

private:
	static void fgetaline(char *foo, FILE *infile, int fooLength);

	static ClueSolutionPair clueSolutionPairs[MAX_CLUE_SOLUTION_PAIRS];
	static int numClueSolutionPairs;

public:

	static void TestOpenFile( const char *pFileName );

	static int GetNumClueSolutionPairs() { return numClueSolutionPairs; };
	static ClueSolutionPair *GetClueSolutionPair( int i ) { return &clueSolutionPairs[i]; };

	static char title[INFO_MAX_SIZE];
	static char author[INFO_MAX_SIZE];
	static char copyright[INFO_MAX_SIZE];

	// The newspaper or whatnot
	// These values are achieved by parsing the title...
	static char sourceName[INFO_MAX_SIZE];
	static tm sourceTime;

	// 10 (mon) to 60 (sat)
	// sun = 35
	static int difficulty;
};

#endif