<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require 'minify_page_start.php';
require('common.php');
generic_page_start('privacy');
?>

<h1>Privacy</h1>

<h2>Using Charitii.com is safe and anonymous!</h2>

<p>
This website does not collect any personal information about you. You do not need to provide any personal data in order to use this website.
</p>

<p>
We use cookies to keep track of water donations and to remember any options that were set in the options page. We do not use cookies to store any personal information on your computer.
</p>

<p>
This web site contains links to other sites. Please be aware that we are not responsible for the content or privacy practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of any other site that collects personally identifiable information.
</p>

<br/>
<br/>

<?php 
generic_page_end();
$minify_file_name = __FILE__;
require 'minify_page_end.php';
