<?php
// Copyright SuperDonate, Inc. 2009

// Offer rewards should be 1000 points for $1 value.

function delete_old_cache($wildcard) {
	$expire_secs = 100 * 60;	
	
	// Find all files of the given file type
	foreach( glob($wildcard) as $filename ) {		
 	    $age = time() - filemtime($filename); 	    
	    if( $age > $expire_secs ) {	    	
	    	unlink($filename);
	    }
	}
}
// TB TODO - Boost this to something bigger! Don't need to do it all the bloody time.
$cleanup_probability = 100;
if( rand(0,$cleanup_probability) == 0 ) {
	delete_old_cache(dirname(__FILE__) . '/../cache/charitii/superrewards/*');
	delete_old_cache(dirname(__FILE__) . '/../cache/charitii/offerpal/*');
}
//exit();

// Get superrewards or offerpal data?
// For now just always get superrewards to get something working!
// Superrewards is not earning anything. Try offerpal???

$offer_provider = 1;

if( $offer_provider == 0 ) {
	require "offer_superrewards_inc.php";
}
else {
	require "offer_offerpal_inc.php";
}

if( $offer_fetch_successful == false ) {
	// TB TODO - Show SuperDonate ad
	echo "Fetch unsuccessful";
} 

// Replace the currency placeholder text with the correct unit
$currency_placeholder = "CHARITY POINTS";


// Capitalize the unit name if it's at the start a sentence. 
$offer_description = preg_replace( "/^".$currency_placeholder."/", ucfirst($_POST["unit_name"]), $offer_description );
$offer_requirements = preg_replace( "/^".$currency_placeholder."/", ucfirst($_POST["unit_name"]), $offer_requirements );
$offer_name = preg_replace( "/^".$currency_placeholder."/", ucfirst($_POST["unit_name"]), $offer_name );

$offer_description = str_replace( $currency_placeholder, $_POST["unit_name"], $offer_description );
$offer_requirements = str_replace( $currency_placeholder, $_POST["unit_name"], $offer_requirements );
$offer_name = str_replace( $currency_placeholder, $_POST["unit_name"], $offer_name );

?>


<div id="featuredOffers">

<div id="featuredOffersMenuBar">
<?php 
echo 'EARN BONUS ' . strtoupper($_POST["unit_name"]) . ' FOR ' . strtoupper($_POST["charity_name"]);
?>
</div>

<table width="730px">
<tr>
<td>

<?php //echo "yo: " . $offer_image; ?>


<?php if($offer_image) echo $offer_image; ?>

 
<div class="offerName">
<a href="<?php echo $offer_url;?>">
<?php echo $offer_name;?>
</a>
</div>

<?php echo $offer_description;?>
<br/>

<?php 
if( $offer_type == "free" ) {
	echo "<b>No purchase required to receive points</b><br/>";
}
?>

<div class="offerSteps">
<?php echo $offer_requirements; ?>
</div>

</td>

<td>

<a class="offerButton" href="<?php echo $offer_url;?>">

<span>
<?php 
if($offer_payout>0) {
	echo 'Earn '.number_format($offer_payout).' '.$_POST["unit_name"];	
}
else {
	echo 'PayDirect';	
}

?>
</span>

</a>


</td>

</tr>
</table>

</div>