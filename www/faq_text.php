<?php // Copyright 2009, SuperDonate.  All rights reserved. ?>

<div class="faqSection" id="faq1">
<h2>What is Charitii.com?</h2>
<p>Charitii.com is a brand new concept in donating to charity. Simply by playing a fun crossword-style word game, you will be donating valuable resources. In addition, you can choose which charity you want to play for. You can choose to donate clean water to communities living in extreme poverty, give food to malnourished children and families, provide school education for children around the world, or help protect our rainforests. By playing Charitii.com you are automatically helping, one word puzzle at a time.</p>
</div>

<div class="faqSection" id="faq2">
<h2>How do I play Charitii.com?</h2>
<p>Charitii.com plays like a multiple choice crossword puzzle. The game will present you with a clue and then 4 possible answers. The clue is written in the same format as a typical crossword puzzle, and the answers are possible words that you would be writing into the boxes of a crossword puzzle. As an example, you may see something like: </p>
<p>
<b>Tasty sweet treat:</b><br/>
COMPUTER<br/>
ICECREAM<br/>
PAPER<br/>
EXCITING<br/>
</p>
<p>To play, click on the answer that you think is correct for the clue. In the above example, "ICECREAM" best describes a tasty sweet treat, so that would be the correct answer. You will then be taken to the next page, which will tell you whether you were correct in addition to what the correct answer is. You will then be given another puzzle to play. Continue playing to see what level you can reach. </p>
<p>For each answer that you get correct, you will donate towards the charity that you have selected. For example, if you have chosen the water charity, and you get twenty questions correct, you will have donated two hundred ounces of water. It adds up very quickly. The donations are made automatically as you play the game. </p>
</div>

<div class="faqSection" id="faq3">
<h2>What are some tips for solving Charitii.com puzzles?</h2>
<p>The puzzles are presented as clues in a style found in popular crossword puzzles. The question topics can be about any subject matter. There are a few tips that can help you understand the clues better.</p>
<ul>
<li>Just like regular crossword puzzles, answers will have no spaces or punctuation between words. This means that if you see "ONSALE" as one of the answers, it should be read as "ON SALE". Knowing how to read the answers is part of the game!</li>
<li>Clues that are plural (more than one) must have a plural answer. For example, if the clue is "Squirrels", the answer must be plural. "RODENTS" could be an answer, but "RODENT" will never be correct since it's not plural. You can use this knowledge to filter out some of the possible answers.</li>
<li>A clue with three dashes indicates that you must fill in the blank with the correct answer. For example, "_ _ _ cheese" could be solved with "BLUE cheese", while "Cheese _ _ _" could be solved with "Cheese BURGER". </li>
<li>If a clue has an abbreviation, the answer must also be an abbreviation. If you see "Government org.", you must find an abbreviated government organization in the possible answers below, such as "FBI" or "CIA". Similarly, if a clue ends with "abbr.", the answer must be an abbreviation. </li>
<li>A clue that ends with a question mark (?) usually indicates a clever play on words or an unusual interpretation of the words. You may have to think outside of the box to come up with the right answer. </li>
<li>"e.g." stands for the Latin phrase "exempli gratia", which translates to "for example". It indicates that the clue is an example of one of the answers. "Football, e.g." is an example of a SPORT, so that is the answer. </li>
</ul>
</div>

<div class="faqSection" id="faq4">
<h2>How does the Charitii.com website work?</h2>
<p>Charitii.com uses a custom database that contains thousands of crossword-style puzzles. The puzzles vary in difficulty so that everyone can play! </p>
<p>As you play Charitii.com, the difficulty level automatically adjusts to your skill level. The game starts by presenting puzzles of different skill levels to determine your starting difficulty level. As you then play more, the program fine tunes your difficulty level. When you get two puzzles in a row correct, you go up one difficulty level. If you get a puzzle wrong, you go down a difficulty level. </p>
<p>If you get enough words in a row correct, the game will start hiding letters from the possible answers to give you an even bigger challenge! The end result is that you are presented with fun puzzles that closely match your skill level. As you play Charitii.com more and get better, you'll notice that you can advance to higher levels. </p>
<p>There are 100 difficulty levels, but getting higher than level 50 is very difficult. </p>
</div>

<div class="faqSection" id="faq15">
<h2>What is the CharitiiChooser?</h2>
<p>The CharitiiChooser lets you pick a particular charitable cause to play for.</p>
<p>Currently, there are four charities available. You can choose to donate ounces of clean drinking water, grains of wheat to feed malnourished families, or minutes of education time in a classroom. You can also choose to protect square inches of rainforest land.</p>
<p>The CharitiiChooser is located on the bottom of the main game page. To select the charity that you wish to play for, simply click on its corresponding button. Future donations made by playing the game will now go to the new charity that you have selected.</p>
</div>

<div class="faqSection" id="faq5">
<h2>How is the difficulty level for each puzzle determined?</h2>
<p>Charitii.com uses its custom database to keep track of how many people have gotten a puzzle right or wrong. Puzzles that people tend to get wrong will be assigned a higher difficulty level, while puzzles that most people get correct will be given an easier difficulty level. The difficulty of a puzzle adjusts itself automatically every time a player attempts a puzzle. As more people play Charitii.com, the difficulty levels will continue to become more accurate. </p>
</div>

<div class="faqSection" id="faq6">
<h2>How many puzzles are there?</h2>
<p>The Charitii.com database has over 10,000 unique puzzles right now. We are continuing to add new puzzles to the database. We will add puzzles for all difficulty levels, so everyone will benefit, and the game will stay fresh. </p>
<p>If you have an idea for a good puzzle, you can submit it to the Charitii.com database for others to play! Go to the <a href="submit.php">Submit</a> page and enter in your clue and answer into the fields provided. After the team at Charitii.com has evaluated and verified the puzzle, it will be included in the database for everyone to play. </p>
</div>

<div class="faqSection" id="faq7">
<h2>What is the Options page? </h2>

<p>On this page you can customize some settings for how you'd like to play Charitii.com. </p>
<ul>
<li>You can specify an initial starting level to play, rather than having the game try to automatically determine it. </li>
<li>You can indicate how many letters you'd like to hide in the answers below. The default is to automatically increase and decrease the number of hidden letters as you play. </li>
<li>You can specify how long the answer words should be. You can focus only on short words or long words if you prefer. </li>
</ul>
<p>Charitii.com stores a cookie on your computer so that these options will be remembered for the next time you play. </p>
</div>

<div class="faqSection" id="faq8">
<h2>Are there any banners or logos I can use to link to Charitii.com? </h2>

<p>You bet! You can go to the <a href="banners.php">Banners</a> page to view the available banners. Linking to the Charitii.com site with a banner is a great way to share it with others. Thank you for helping to get others involved in donating to charities simply by playing word games. </p>
</div>

<div class="faqSection" id="faq9">
<h2>How does Charitii.com keep track of my donations? </h2>

<p>Charitii.com stores the result of every puzzle answered in its custom database. When you click on an answer, the game determines whether your selected answer is correct or incorrect, and then creates a new record in the database. The total number of correct records is tallied for each day and from that the amount donated is calculated. </p>
<p>As soon as you click on a correct answer, the donation will be recorded.  You do not need to do anything else to save the donation. You can stop playing at any time and all of your donations will have been safely recorded. </p>
</div>

<div class="faqSection" id="faq12">
<h2>Where does the money for the donations come from?</h2>

<p>Sponsors on this web site pay for the charity donations. When you play Charitii.com, the advertising banners that you see on the site are what fund the donations to the charities. These sponsors are needed to support Charitii.com and the CharitiiChooser. Please visit a sponsor's web site to help. </p>
<p>If you are a company or organization interested in sponsoring Charitii.com donations through advertising banners, please contact us. You can go to the <a href="contact.php">contact</a> page to reach us.</p>
</div>

<div class="faqSection" id="faq13">
<h2>What is charity: water?</h2>

<p>charity: water is a nonprofit organization bringing clean, safe drinking water to people in developing nations. They give 100% of the money raised to direct project costs, funding sustainable clean water solutions in areas of greatest need. They also work to raise awareness of the water crisis through events, fundraising exhibitions and other public awareness campaigns.</p>
<p>Drilling a well can cost from $4,000 - $ 12,000 and many living on less than $1 a day cannot afford one in their community.  With the help of exemplary organizations on the ground, charity: water can drill wells and provide people with this basic, essential need. charity: water partners with local organizations in each country where they work, choosing the partners based on expertise and the ability to impact real, sustainable change in the communities they benefit. </p>
<p>The local community is engaged in the well building process, carrying out small tasks for free to reduce labor costs. This also encourages community participation and ensures community ownership after the project is complete. When the well is built, a water committee is formed. It generally consists of 6-8 people, half of them female. In the case of hospitals, the committee will generally consist of nurses and hospital staff. In schools, the committee would likely be composed of teachers. Since charity: water was founded and began activity in August 2006, they have funded the construction of more than 600 wells that, when completed, will provide clean drinking water to 250,000 people. </p>
<p>For further information about charity: water, please visit their <a href=" http://www.charitywater.org/">website</a>.</p>
</div>

<div class="faqSection" id="faq14">
<h2>Where has charity: water already donated water?</h2>

<p>In 18 months, charity: water has raised over $3 million and funded over 600 water projects.</p>
<ul>
<li>Kenya - 6 projects</li>
<li>Liberia - 72 projects</li>
<li>Central African Republic - 113 projects</li>
<li>Uganda - 34 projects</li>
<li>Tanzania - 18 projects</li>
<li>Ethiopia - 75 projects</li>
<li>Malawi - 66 projects</li>
<li>Rwanda - 11 projects</li>
<li>Bangladesh - 200 projects</li>
<li>Democratic Republic of Congo - 21 projects</li>
<li>India - 13 projects</li>
</ul>

<p>
To access more specific information about the water projects, including an interactive Google Earth display plotting every site, visit charity: water's <a href=" http://www.charitywater.org/projects/projects.htm">projects</a> page.
</p>
</div>

<div class="faqSection" id="faq10">
<h2>Why is water important? </h2>

<p>Right now, 1.1 billion people on the planet don't have access to safe, clean drinking water. That's one in six of us. </p>
<p>Unsafe water and lack of basic sanitation cause 80% of all sickness and disease, and kill more people every year than all forms of violence, including war. Many people in the developing world, usually women and children, walk more than three hours every day to fetch water that is likely to make them sick. Those hours are crucial, preventing many from working or attending school. Additionally, collecting water puts them at greater risk of sexual harassment and assault. Children are especially vulnerable to the consequences of unsafe water.  </p>
<p>Polluted water is the world's biggest killer of children under five. Of the 42,000 deaths that occur every week from unsafe water and a lack of basic sanitation, 90% are children under 5 years old. </p>
</div>


<div class="faqSection" id="faq19">
<h2>Why is hunger important? </h2>
<p>More than 840 million people in the world are malnourished - 799 million of them live in the developing world.</p>
<p>More than 153 million of the world's malnourished people are children under the age of 5.</p>
<p>Six million children under the age of 5 die every year as a result of hunger.</p>
<p>Malnutrition can severely affect a child's intellectual development. Malnourished children often have stunted growth and score significantly lower on math and language achievement tests than do well-nourished children.</p>
<p>Lack of dietary diversity and essential minerals and vitamins also contributes to increased child and adult mortality. Vitamin A deficiency impairs the immune system, increasing the annual death toll from measles and other diseases by an estimated 1.3 million-2.5 million children.</p>
<p>While every country in the world has the potential of growing enough food to feed itself, 54 nations currently do not produce enough food to feed their populations, nor can they afford to import the necessary commodities to make up the gap. Most of these countries are in sub-Saharan Africa.</p>
<p>Most of the widespread hunger in a world of plenty results from grinding, deeply rooted poverty. In any given year, however, between 5 and 10 percent of the total can be traced to specific events: droughts or floods, armed conflict, political, social and economic disruptions.</p>
</div>

<div class="faqSection" id="faq17">
<h2>What is The Oaktree Foundation?</h2>
<p>
The Oaktree Foundation is an entirely youth-run international aid and development organisation.
Their mission is to empower developing communities through education in a way that is sustainable.
</p>
<p>
Oaktree has a vision of equal educational opportunity for all young people across the world, as they believe that education is a key to breaking the poverty cycle. Oaktree believes that educated, empowered and motivated communities are better able to change their future. 
</p>
<p>
Over the last 4 years Oaktree has raised more than $1,000,000 for development projects in six countries to provide more than 40,000 young people with increased educational opportunities. 
</p>
<p>
Oaktree works in partnership with local organisations in developing communities to create and deliver development projects focused on education. This directly addresses some of the immediate needs facing developing communities. Projects that Oaktree have supported include vocational skills training, school building, teacher training and community resource centers.
</p>
<p>
To learn more about The Oaktree Foundation, please visit their <a href="http://theoaktree.org/">website</a>.
</p>
</div>

<div class="faqSection" id="faq18">
<h2>What is The Nature Conservancy?</h2>
<p>
The mission of The Nature Conservancy is to preserve the plants, animals and natural communities that represent the diversy of life on Earth by protecting the lands and waters they need to survive.
</p>
<p>
At Costa Rica's southwest corner, 400,000 forested acres of the Osa Peninsula jut into the Pacific Ocean. Only a strip of white sand beach separates the lush rainforest from the sea. The forest is alive with a wealth of magnificent plant and wildlife, including jaguar, puma, sloths and four species of monkey. You'll also find 4,000 plant species, including trees that can grow more than 200 feet tall.
</p>
<p>
Historically, the dense forests and rocky terrain of the Osa Peninsula shielded it from the outside world. But improved transportation and expanding development now threaten the peninsula. Osa's forests are starting to disappear as this once isolated land increasingly attracts farmers, hunters, developers, gold miners and tourists. Sadly, lands are being cleared for logging and agriculture at a higher rate than anywhere else in Costa Rica. Illegal logging, poaching and ill-conceived land-use activities further threaten the region. 
</p>
<p>
With the Costa Rican government and a strong network of partners, The Nature Conservancy is working to protect and maintain the Osa Peninsula's forests and coastal areas. They are doing this in a variety of ways, such as:
</p>
<ul>
<li>Acquiring and protecting land</li>
<li>Strengthening the management of protected areas</li>
<li>Hiring and training park staff</li>
<li>Creating a 100,000 acre biological corridor between Corcovado and Piedras Blancas National Parks</li>
<li>Creating sustainable, bush-based job opportunities</li>
<li>Establishing a network of marine protected areas</li>
</ul>

<p>
To learn more about The Nature Conservancy, please visit their <a href="http://www.nature.org/">website</a>.
</p>

