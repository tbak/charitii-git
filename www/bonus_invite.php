<?php

class Bonus
{
	const c_num_addresses = 5;
	public $did_submit = FALSE;

	function check_form()
	{
		if( isset($_POST['token']) )
		{
			require "common.php";
			require "db.php";
			connect_database();

			$this->did_submit = TRUE;

			// Form has already been verified by javascript.

			$sender_name = sql_quote($_POST['sender_name']);

			// TB TODO - Insert into database
			for( $i = 0; $i < self::c_num_addresses; $i++ ) {
				if( isset($_POST['name'.$i]) ) {

					$receiver_name = sql_quote($_POST['name'.$i]);
					$receiver_email = sql_quote($_POST['email'.$i]);

					$personal_message = "";
					if( isset($_POST['personal_message']) ) {
						// Note! $personal_message is not safe to insert into a database! Good thing we're not!
						// If I use sql_quote I get \n\n and stuff like that in the message -- sucks.
						$personal_message = stripslashes($_POST['personal_message']);
					}

					// Check if the email address is already in the database... (so we only send 1 email)
					$query = "SELECT 1 FROM referral_bonus WHERE receiver_email = '" . $receiver_email . "';";
					$result = mysql_query($query) or die('Query failed: ' . mysql_error());
					if( mysql_num_rows($result) > 0 )
						continue;

					$query = "INSERT IGNORE INTO referral_bonus (sender_name, receiver_name, receiver_email, used) VALUES ( '" . $sender_name . "', '" . $receiver_name . "', '" . $receiver_email . "', 0 );";
					$result = mysql_query($query) or die('Query failed: ' . mysql_error());

					// Send the email!
					$message_subject = "A Charitii invitation for " . $receiver_name;
					$message_body = "Hello " . $receiver_name . ",\n\n";

					if( $personal_message != "" ) {
						$message_body .= "============================\n";
						$message_body .= $personal_message . "\n";
						$message_body .= "============================\n\n";
					}

					$message_body .= $sender_name . " has sent you a special bonus invitation to play Charitii, the online word game that donates to charity for every correct answer.\n\n";
					$message_body .= "With this inviation you will earn a double donation bonus. Visit http://charitii.com?bonus=" . $receiver_email . " to start playing now.\n\n";
					$message_body .= "Regards,\nThe Charitii Team";
					$headers="From: noreply@charitii.com";
                    mail($receiver_email,$message_subject,$message_body,$headers);
				}
			}
		}
		
	}

	function __construct()
	{
		$this->check_form();
	}

}
$bonus = new Bonus();
?>

<div id="bonusInviteBox">


<?php
// Thank you message
// The ajax call will take care of fading all this away.
if( $bonus->did_submit ) {
	echo '<h1>Thank You!</h1>';
	//echo $_POST['sender_name'];
	echo '</div>';
	return;
}
?>





<h1>
Send Your Friends a Charitii Bonus
</h1>

You can use this form to quickly send an invitiation to your friends.
Simply enter the names and email addresses of people you'd like to share Charitii with and then click send.
<br/><br/>
<b>
As a welcome bonus, we will double their donations after 10 correct answers.
</b>
<br/><br/>

<form id="form_bonus" name="form_bonus" action="" method="">
<input type=hidden name="token" value="42" />

<table>

<tr>
<td>
Your Name:
</td>
<td>
<input type="text" size="35" maxlength="50" id="sender_name" name="sender_name" value=""/>
</td>
</tr>

<tr>
<td>
Your Personal Message:<br/>(optional)
</td>
<td>
<textarea rows="2" cols="40" id="personal_message" name="personal_message">Please try out Charitii!</textarea>
</td>
</tr>

</table>

<br/>

<center>
<table border="0" bgcolor="#ececec" cellspacing="5" width="300">
<tr><th>Name</th><th>Email Address</th><tr>

<?php
for( $i = 0; $i < Bonus::c_num_addresses; $i++ ) {

	echo '
	<tr>
	
	<td>
	<input type="text" size="35" maxlength="50" id="name' . $i . '" name="name' . $i . '" value=""/>
	</td>
	
	<td>
	<input type="text" size="35" maxlength="50" id="email' . $i . '" name="email' . $i . '" value=""/>
	</td>
	
	</tr>
	';

}
?>

<tr><td>&nbsp;</td></tr>

<tr><td>
	<div>
		<div style="float: left;">
			<input type="submit" name="submit" class="button" id="submit_btn" value="&nbsp;&nbsp;Send&nbsp;&nbsp;" />
		</div>
		<div style="float: left; margin-top: 3px; margin-left: 5px;">
			&nbsp;<img style="display: none;" id="ajaxLoad" src="test/ajax_load.gif" />
		</div>
		<div class="clear"></div>
	</div>
</td></tr>

<!--<tr><td><input type="submit" value="Send" /></td></tr>-->

</table>
</center>

</form>


<script type="text/javascript">
$("#TB_ajaxContent").addClass("bonusInviteBox");

$(function() {

	$(".button").click(function() {  

		// TB TODO - Verify that name has been set and at least one name + email address
		// has been set.
	
		//document.form1.selected.value=n;
		//document.form1.submit();
	
		var error_found = false;

		var sender_name = $("input#sender_name").val();
		var personal_message = $("textarea#personal_message").val();

		$("input#sender_name").removeClass("error");
		if( sender_name.length == 0 ) {
			$("input#sender_name").addClass("error");
			error_found = true;
		}

		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		<?php
		// This will look like "sender_name:sender_name, name0:javascript_to_get_value, email0:javascript_to_get_value, ..."
		$post_vals = 'sender_name:sender_name, personal_message:personal_message, token:"15", ';

		for( $i = 0; $i < Bonus::c_num_addresses; $i++ ) {
			echo 'var email' . $i . ' = $("input#email' . $i . '").val();';
			echo 'var name' . $i . ' = $("input#name' . $i . '").val();';
			
			$post_vals .= 'name' . $i . ':' . 'name' . $i . ' ,';
			$post_vals .= 'email' . $i . ':' . 'email' . $i . ' ,';

			echo '$("input#email' . $i . '").removeClass("error"); ';
			
			echo 'if( email' . $i . '.length > 0 && !filter.test(email' . $i . ')) { error_found = true; $("input#email' . $i . '").addClass("error"); }';

			echo '$("input#name' . $i . '").removeClass("error"); ';

			echo 'if( email' . $i . '.length > 0 && name' . $i . '.length == 0 ) { error_found = true; $("input#name' . $i . '").addClass("error"); }';
			echo 'if( name' . $i . '.length > 0 && email' . $i . '.length == 0 ) { error_found = true; $("input#email' . $i . '").addClass("error"); }';
		}
		?>
	
		if( name0.length == 0 ) {
			$("input#name0").addClass("error");
		}
		if( email0.length == 0 ) {
			$("input#email0").addClass("error");
		}

		if( error_found ) {
			alert('Please fix the red text fields and try again.');
			return false;			
		}

		$("#ajaxLoad").show();

		// Reload this page with the POST variables from the form.
		// The page will display "thank you".
		// Wait 1 second, and then close the thickbox!
		$("#TB_ajaxContent").load("bonus_invite.php", { <?php echo $post_vals; ?> }, 
			function() {
				window.setTimeout( function() { tb_remove(); }, 1000);
			});
		
		return false;		
	});  
});  

</script>

</div>
