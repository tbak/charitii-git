<?php
// I need to start the session if I am only going to load this with AJAX... 
session_start();
?>

<script src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php" type="text/javascript"></script>

<?php
global $game;
$feed_string = "popupFeedDialogAllCharities(" . $_SESSION['donated_amount0'] . "," . $_SESSION['donated_amount1'] . "," . $_SESSION['donated_amount2'] . "," . $_SESSION['donated_amount3'] . ");";
?>

<script type="text/javascript">

function popupFeedDialogAllCharities(score0, score1, score2, score3)
{
	// TB TODO - Donated amounts!
	// And TOTAL donated amounts???
	// Some kind of happy message???
	var template_data = {
		'score0' : score0,
		'score1' : score1,
		'score2' : score2,
		'score3' : score3,
		'images' : [{src: 'http://media.superdonate.org/img/fb_take-care-of-your-heart.jpg', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity0_off.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity1_off.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity2_off.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity3_off.png', href:'http://apps.facebook.com/charitii'}
		]
	};

	// $feed_template_bundle_id_allcharities
    FB.Connect.showFeedDialog(74269624648, template_data, null, "", null, FB.RequireConnect.promptConnect, null );
}

function popupFeedDialogSingleCharity(score, charity)
{
	var unit = "units";
	switch( charity )
	{
	case 0:
		unit = "ounces of water";
		break;
	case 1:
		unit = "grains of wheat";
		break;
	case 2:
		unit = "minutes of education";
		break;
	case 3:
		unit = "square inches of rain forest";
		break;
	}

	// TB TODO - Donated amounts!
	// And TOTAL donated amounts???
	// Some kind of happy message???
	var template_data = {
		'score' : score,
		'unit' : unit,
		'images' : [{src: 'http://media.superdonate.org/img/fb_take-care-of-your-heart.jpg', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity0_' + (charity == 0 ? 'on' : 'off') + '.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity1_' + (charity == 1 ? 'on' : 'off') + '.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity2_' + (charity == 2 ? 'on' : 'off') + '.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity3_' + (charity == 3 ? 'on' : 'off') + '.png', href:'http://apps.facebook.com/charitii'}
		]
	};

	// $feed_template_bundle_id_onecharity
    FB.Connect.showFeedDialog(74270344648, template_data);
}

function fb_connected(str) 	
{

	var uid = FB.Connect.get_loggedInUser();

	// Just do the wall area since we use to top menu for addtoany now
	/*
	var fb_profilePic = document.getElementById("fb_ojbarProfilePic");

	// Why does uid='loggedinuser' not work anymore???
    fb_profilePic.innerHTML =
		"<span>" + 
		"<fb:profile-pic linked='false' uid='" + uid + "' facebook-logo='true'></fb:profile-pic><br/>" + 
		"</span>";

	var fb_profileText = document.getElementById("fb_ojbarProfileText");
	fb_profileText.innerHTML = "<div class=\"miniBR\"></div>" +
		"Welcome, <fb:name uid='" + uid + "' linked='false' useyou='false'></fb:name><br/>" +
	 	"<a id='fb_logout' href=\"#\" onclick='FB.Connect.logoutAndRedirect(\"/index.php\")'>Logout of Facebook</a>";
	*/

	var fb_wallAndInvite = document.getElementById("fb_wallAndInvite");


	// It's possible for this element to not exist if we are outside of the game.
	if( fb_wallAndInvite != null )
	{

		fb_wallAndInvite.innerHTML =
			"<a href=\"#\" onclick=\"<?php echo $feed_string; ?>\"><img src=\"test/fb_wall.png\" alt=\"Post to your Facebook wall\" /></a>" +
			"<div class=\"miniBR\"></div>" +
			"<a href=\"http://apps.facebook.com/charitii/invite.php\"><img src=\"test/fb_invite.png\" alt=\"Invite your Facebook friends\" /></a>";
	
		<?php 
		// Check if you crossed the threshold (100 and 250 donated) for an auto popup wall post
		if( $game && $game->did_answer_correct ) {
			$all_amounts = array(250,100);
			foreach($all_amounts as $amount) {
	
				if( $_SESSION['donated_amount'.$_SESSION['charity_select']] == $amount ) {
					echo "popupFeedDialogSingleCharity(" . $amount . "," . $_SESSION['charity_select'] . ");";
					break;
				}			
	
			}
		}
		?>
	}

	// because this is XFBML, we need to tell Facebook to re-process the document 
	FB.XFBML.Host.parseDomTree();
}
	
function fb_not_connected(str) 
{

	// Only show the facebook connect logo if the dude is already connected to facebook (guaranteed user)
	// We now have the (probably better) addtoany widget hooked up at the top.
	/*
	// Display FB connect button where profile pic shows
	var fb_profile = document.getElementById("fb_ojbarProfilePic");
	fb_profile.innerHTML = "<br/><fb:login-button size=\"medium\" onlogin=\"fb_connected();\"></fb:login-button>";

	var fb_profileText = document.getElementById("fb_ojbarProfileText");
	fb_profileText.innerHTML = "";
	*/

	// Also display the button where the wall+invite text shows...
	// For now screw it.. Just have nothing so it's not too in your face with facebook.
	var fb_wallAndInvite = document.getElementById("fb_wallAndInvite");

	// It's possible for this element to not exist if we are outside of the game.
	if( fb_wallAndInvite != null )
	{

		fb_wallAndInvite.innerHTML = "";
	
		// Pop up the FB connect login... do I want this???
		// MAYBE WAIT FOR 3 CORRECT ANSWERS BEFORE POPPING THIS UP???
		FB.Connect.get_status().waitUntilReady( function FB_prompt_connect_dialog() {
			var status = FB.Connect.get_status().result;
	
			if( status == FB.ConnectState.appNotAuthorized )
			{
				// User not FB connected but logged into FB
				// Display the other login button since we know that they are facebook users, so it's easy for them to connect.
				fb_wallAndInvite.innerHTML = "<div class=\"miniBR\"></div> <fb:login-button onlogin=\"fb_connected();\"></fb:login-button>";
	
				// TB - MAYBE WAIT FOR 3 CORRECT ANSWERS BEFORE POPPING THIS UP???
				if( $_SESSION['num_correct'] == 3 )
				{
	
					if(!Cookie.read('fb_prompted_1')) { 
	
						// user has not been prompted in the last week
	
						// don't display again during this session
						Cookie.write('fb_prompted_1', true, {duration:7});
	
						// display the facebook connect prompt
						FB.Connect.requireSession();
					}
	
				}

				// because this is XFBML, we need to tell Facebook to re-process the document 
				FB.XFBML.Host.parseDomTree();
			}
		});

	}

	// because this is XFBML, we need to tell Facebook to re-process the document 
	//FB.XFBML.Host.parseDomTree();
}

FB.init("5a80244102538b520e6f6db71040a5a7", "xd_receiver.htm", {"ifUserConnected":fb_connected, "ifUserNotConnected":fb_not_connected} );

</script>

