<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require 'minify_page_start.php';
require('common.php');
require('game_database.php');
$game = new Game();
$game->generate_valid_question();
?>

<?php
//$common->minify_page_start();
echo_html_head_meta('');
?>

<?php
$is_first_question = !isset($_POST['token']);
?>



<body id="index">
<div id="container">
<div id="content">

<?php
$game->display_referral_bonus();
?>

<?php include('header.php'); ?>


<div id="wrapperContentBox">

<?php dialog_start(); ?>


<?php

echo_top_ad();

echo '<div class="magicRow2" id="magicRow2Game">';
require('game_box.php');
echo '</div>';
	
if( $_SESSION['num_correct'] == 0 ) {
	echo '<script type="text/javascript">startPossibleAnswerFlash(); startBounceAnswerSuggest();</script>';
}

// Show ad on first screen
echo_bottom_ad();

if( $is_first_question ) {
	require('game_first.php');
}

?>


<?php dialog_end(); ?>


</div><!--wrapperContentBox-->

<?php include('footer.php'); ?>


<?php echo_final_script_code(); ?>

</body>
</html>
<?php
$minify_page_is_dynamic = TRUE;
$minify_file_name = __FILE__;
require 'minify_page_end.php';
?>
