<?php

// http://docs.offerpalmedia.com/integration/callback


$use_cache = false;
$cache_filename = dirname(__FILE__) . '/cache/offerpal/offer_' . $_POST['uid'] . '.xml';
if( file_exists($cache_filename) ) {

	$cache_age = time() - filemtime($cache_filename); 
	
	// Cached file can't be too old
    if( $cache_age < (5 * 60) ) {
    	$use_cache = true;
    }

} 

// Remove this comment to force the cache to never be used.
$use_cache = false;

if( $use_cache ) {
	//$xml = simplexml_load_file("offers_offerpal.xml");
	$xml = simplexml_load_file($cache_filename);
}
else {
	// Generate the URL and then grab the data
	$url = "http://api110.myofferpal.com/1562bda0675dc8e17ab419c588c36818/showoffersAPI.action?callbackFormat=xml";
	$url .= "&snuid=" . $_POST["uid"];	
	
	$url .= "&userIp=" . $_SERVER['REMOTE_ADDR'];
	
	// Fake the IP for testing
	// I now have the backup system if no offers were found, so my normal IP is OK.
	// $url .= "&userIp=64.233.181.104";
	
	$url .= "&affl=" . $_POST["charity_select"];
	
	// echo $url;	
	
	$xmlstr = file_get_contents($url);	
	
	// Save to cache so we don't need to constantly request from an external server
	$f=fopen($cache_filename,"w");
	fwrite($f, $xmlstr);
	fclose($f);
	
	$xml = new SimpleXMLElement($xmlstr);
}
   
//

//echo $xml->getName() . "<br />";

$num_selected = 0;

//print $_POST["unit_name"];
//print $_POST["charity_name"];

$offer_fetch_successful = false;


$xml = $xml->offers;

for( $attempt=0; $attempt<2; $attempt++ ) {

	foreach($xml->children() as $child) {
		
		/*
		echo '<br/>***';
		echo $child->getName() . ": " . $child . "<br />";
		
		foreach($child->children() as $baby) {
			echo $baby->getName() . ": " . $baby . "<br />";
		}
		*/
		
		
		$allow = false;
		if( $attempt == 0 ) {
			// Free offers or all offers???
			// if( $child->creditCardRequired == 0 ) {	
			$allow = intval($child->amount) > 0 && rand(0,$num_selected) == 0;
		}
		else {
			$allow = true && rand(0,$num_selected) == 0;
		}	
		
		// Payouts of 0 are pay by credit card/mobile phone/etc
		if( $allow ) {
		
			$offer_name = $child->name;
			$offer_payout = intval($child->amount);
			$offer_url = $child->actionURL;
			$offer_type = $child->type;			
			$offer_description = $child->description;
			
			// Offerpal feels compelled to put this in <ul> + <li> tags...
			// which screws up the currency replacement algorithm in offer.php
			$offer_requirements = $child->instructions;
			$offer_requirements = strip_tags($offer_requirements);
			
			$offer_image = $child->imageHTML;
			
			$offer_fetch_successful = true;
			$num_selected++;
		}
				
	}
	
	if( $offer_fetch_successful ) {
		break;
	}
	
	// Fetch failed. Try again less restrictive 
}



?>