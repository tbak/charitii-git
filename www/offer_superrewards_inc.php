<?php

// This URL will present a page with a link to send 1000 charity points.
// http://super.kitnmedia.com/super/offers?h=hfakxkqnwh.311001124889&ip=127.0.0.1&uid=1&n_offers=40&custom_charity_select=0

$use_cache = false;
$cache_filename = dirname(__FILE__) . '/cache/superrewards/offer_' . $_POST['uid'] . '.xml';
if( file_exists($cache_filename) ) {

	$cache_age = time() - filemtime($cache_filename); 
	
	// Cached file can't be too old
    if( $cache_age < (5 * 60) ) {
    	$use_cache = true;
    }

} 

// Remove this comment to force the cache to never be used.
// $use_cache = false;

if( $use_cache ) {
	//$xml = simplexml_load_file("offers_superrewards.xml");
	$xml = simplexml_load_file($cache_filename);
}
else {
	// Generate the URL and then grab the data
	$url = "http://super.kitnmedia.com/super/offers?h=hfakxkqnwh.311001124889&xml=1";
	$url .= "&mode=free";
	$url .= "&ip=" . $_SERVER['REMOTE_ADDR'];
	$url .= "&uid=" . $_POST["uid"];
	$url .= "&n_offers=" . 40;
	$url .= "&custom_charity_select=" . $_POST["charity_select"];
	$xmlstr = file_get_contents($url);
	
	// echo $url;
	
	// Save to cache so we don't need to constantly request from an external server
	$f=fopen($cache_filename,"w");
	fwrite($f, $xmlstr);
	fclose($f);
	
	$xml = new SimpleXMLElement($xmlstr);
}
   
//

//echo $xml->getName() . "<br />";

$num_selected = 0;

//print $_POST["unit_name"];
//print $_POST["charity_name"];

$offer_fetch_successful = false;

for( $attempt=0; $attempt<2; $attempt++ ) {
	
	foreach($xml->children() as $child) {
		
		/*
		echo '<br/>***';
		echo $child->getName() . ": " . $child . "<br />";
		
		foreach($child->children() as $baby) {
			echo $baby->getName() . ": " . $baby . "<br />";
		}
		*/
		
		
		$allow = false;
		if( $attempt == 0 ) {
			// Free offers or all offers???
			//if( $child->type == "free" && rand(0,$num_selected) == 0 ) {
			// Payout of 0 is a pay by credit card / phone / etc that we don't want...
			$allow = intval($child->payout) > 0 && rand(0,$num_selected) == 0;
		}
		else {
			$allow = true && rand(0,$num_selected) == 0;
		}	
		
		// 
		if( $allow ) {
				
			$offer_payout = intval($child->payout);		
			
			$offer_url = $child->url;
			$offer_type = $child->type;
			$offer_image = "<img align='left' src='http://204.137.28.201/" . $child->icon . "' />";
			$offer_name = $child->name;
			$offer_description = $child->description;
			$offer_requirements = $child->requirements;			
			$offer_fetch_successful = true;
	
			$num_selected++;
		}
	}
	
	if( $offer_fetch_successful ) {
		break;
	}
	
}

?>
