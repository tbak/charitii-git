<?php
// Copyright 2009, SuperDonate.  All rights reserved.
// TB - For some reason PATH_INFO does not get set on the 1&1 servers... This should fix it...
// It's needed in SetupSources($options)...
if( isset($_SERVER['ORIG_PATH_INFO']) )
{
	$_SERVER['PATH_INFO'] = urldecode($_SERVER['ORIG_PATH_INFO']);
}

require 'minify_config.php';
require 'minify_groupsSources.php';

require 'Minify/Build.php';
$jsBuild = new Minify_Build($groupsSources['js']);
$cssBuild = new Minify_Build($groupsSources['css']);

ob_start(); 
