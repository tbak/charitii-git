<?php
// Copyright SuperDonate, Inc.

require "offer_generic_postback_inc.php";

// Offerpal postback from docs
// http://docs.offerpalmedia.com/integration/callback
// http://www.yourserver.com/anypath/reward.php?snuid=[user id]&currency=[currency credit to user]&id=[transaction id]&verifier=[signature]&affl=[additional user data]

// Sample postback!
// http://localhost/puz3/offer_offerpal_postback.php?snuid=1&currency=100&id=42&verifier=1&affl=3

function respond_success()
{
	header("Status: 200 Success!");
	exit();
}

function respond_failure()
{
	header("Status: 666 Failure");
	exit();
}

function respond_alreadyprocessed()
{
	header("Status: 403 This has already been processed. Stop sending postbacks.");
	exit();
}

// snuid: the user ID (What is a SNUID?)
// currency: the amount that the user should be credited
// id: transaction ID or the Offerpal ID shown on the offer status page - this is unique to every action for each user
// verifier: security hash signature used to verify callback authenticity(see OfferpalSECURE for more info)
// affl: (optional) affiliate is a parameter that allows the tracking of additional data
// I will use affl to hold the charity!

// Specific for superrewards
function verify_sig()
{
	// Don't bother to verify the secret for now
	// No! Verify it fool!
	$verify_sig = true;
	
	if( $verify_sig ) {
	
		// Verify
		$secret = "1327947585012813";
		$expected_sig = md5($_REQUEST["id"] . ":" . $_REQUEST["snuid"] . ":" . $_REQUEST["currency"] . ":" . $secret );
		
		if( $expected_sig != $_REQUEST['verifier'] ) {
			return false;
		}
	}
	
	return true;
}


// Specific to superrewards... offerpal does not offer a total number?
function update_user_amount($user_id, $charity_select, $transaction_id, $offer_provider)
{
	// Check if we have already processed this transaction for the user...
	$query = sprintf("select amount from offers_user where user_id=%d and transaction_id=%d and offer_provider=%d",
						$user_id, $transaction_id, $offer_provider );
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());	
	if( mysql_num_rows($result) != 0 ) {
		// We already have a transaction for this... Perhaps the offerpal
		// server did not get our previous success response. Either way, do
		// not process this transaction again.
		respond_alreadyprocessed();
	}
							
	$added_amount = $_REQUEST['currency'];
		
	insert_offer_transaction( $user_id, $charity_select, $added_amount, $transaction_id, $offer_provider ); 
}

// Offerpal specific variables used by the generic code...?
// 1 = offerpal
$offer_provider = 1;	
$transaction_id = $_REQUEST["id"];
$user_id = $_REQUEST["snuid"];
$charity_select = $_REQUEST["affl"];

if( verify_sig() == false ) {
	echo "Bad sig";
	respond_failure();
	return;
}
if( verify_userid($user_id) == false ) {
	echo "Couldn't verify user";
	respond_failure();
	return;
}
update_user_amount($user_id, $charity_select, $transaction_id, $offer_provider);
recalculate_day_amounts();

// Success!
respond_success();
return;

?>
