<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require 'minify_page_start.php';
require('game_database.php');
require('common.php');
require('progress_totals.php');

$totals = new Totals();
generic_page_start('progress');
?>
 
<h1>Stats</h1>

<p>
On this page you can see how much has already been donated. The information is arranged by day and month.
</p>

<br/>
<br/>
<?php draw_chart_for_months(600,200); ?>
<br/>
<br/>
<br/>

<br/>
<?php draw_chart_for_current_month(600,200); ?>
<br/>

<br/>
<br/>
<br/>

<b>Monthly</b>
<?php

$totals->echo_for_all_months();
?>

<br/>
<br/>

<b>Daily</b>
<?php
$totals->echo_for_current_month();
?>


<br/>
<p>
<i>Charitii.com was launched on August 26, 2008</i>
</p>
<p>
<i>Sorry, CARE is no longer available on Charitii. CARE has been paid out in full.</i>
</p>
<br/>

<?php
generic_page_end();
$minify_page_is_dynamic = TRUE;
$minify_file_name = __FILE__;
require 'minify_page_end.php';
