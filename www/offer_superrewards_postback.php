<?php
// Copyright SuperDonate, Inc.

require "offer_generic_postback_inc.php";

// This URL will present a page with a link to send 1000 charity points.
// http://super.kitnmedia.com/super/offers?h=hfakxkqnwh.311001124889&ip=127.0.0.1&uid=1&n_offers=40&custom_charity_select=0

// Sample postback (sig is invalid)
// http://localhost/puz3/offer_superrewards_postback.php?id=42&new=10&total=1000&uid=5&oid=69&sig=45&custom_charity_select=0

// SuperRewards request variables
//id: a unique identifier for this transaction
//new: points user earned by filling out offer �oid�
//total: total number of points accumulated by this user on your application
//uid: your site's user uid (facebook, myspace, custom, etc)
//oid: SuperRewards offer identifier
//sig: security hash used to verify the authenticity of the postback
// custom_etc (custom_charity_select)


// Specific for superrewards
function verify_sig()
{
	// Don't bother to verify the secret for now
	$verify_sig = true;
	
	if( $verify_sig ) {
	
		// Verify
		$secret = "35723fdf1737a8b26dd51595ab07f5ea";
		$expected_sig = md5($_REQUEST['id'] . ':' . $_REQUEST['new'] . ':' . $_REQUEST['uid'] . ':' . $secret);
		
		if( $expected_sig != $_REQUEST['sig'] ) {
			return false;
		}
	}
	
	return true;
}


// Specific to superrewards... offerpal does not offer a total number?
function update_user_amount($user_id, $charity_select, $transaction_id, $offer_provider)
{
	$query = "select sum(amount) from offers_user where user_id='" . $user_id . "'";
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	$old_user_total = 0;
	if( mysql_num_rows($result) != 0 ) {
		$row = mysql_fetch_array($result);
		$old_user_total = $row[0];
	}
	
	// Determine the new amount to add, for this date+charity+user
	$added_amount = $_REQUEST['total'] - $old_user_total;
		
	insert_offer_transaction( $user_id, $charity_select, $added_amount, $transaction_id, $offer_provider ); 
}


// superrewards specific variables used by the generic code...?
// 0 = superrewards
$offer_provider = 0;	
$transaction_id = $_REQUEST["id"];
$user_id = $_REQUEST["uid"];
$charity_select = $_REQUEST["custom_charity_select"];

if( verify_sig() == false ) {
	echo "0";
	return;
}
if( verify_userid($user_id) == false ) {
	echo "0";
	return;
}
update_user_amount($user_id, $charity_select, $transaction_id, $offer_provider);
recalculate_day_amounts();

// Success!
echo "1";
return;

?>
