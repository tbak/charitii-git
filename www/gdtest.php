<?php
 
// example1.php
 
// set the HTTP header type to PNG
header("Content-type: image/png"); 
 
// set the width and height of the new image in pixels
$width = 350;
$height = 360;
 
// create a pointer to a new true colour image
$im = ImageCreateTrueColor($width, $height); 
 
// sets background to red
$red = ImageColorAllocate($im, 255, 0, 0); 
ImageFillToBorder($im, 0, 0, $red, $red);
 
// send the new PNG image to the browser
ImagePNG($im); 
 
// destroy the reference pointer to the image in memory to free up resources
ImageDestroy($im); 
 
?>