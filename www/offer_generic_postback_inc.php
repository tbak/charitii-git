<?php
// Copyright SuperDonate, Inc.

require "common.php";
require "db.php";
connect_database();

// Generic
function verify_userid($user_id)
{
	// Verify that this user ID exists in the user database
	$query = "select TRUE from users where id = '" . $user_id . "';";
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	if( mysql_num_rows($result) == 0 ) {
		return false;		
	}
	
	return true;
}


function recalculate_day_amounts()
{
	// Add up all the amounts for the current date again and updae offers_daily
	$query = "REPLACE INTO offers_daily (DATE,charity_select, amount) (SELECT CURRENT_DATE, charity_select, SUM(amount) FROM offers_user WHERE DATE=CURRENT_DATE() GROUP BY charity_select)";
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());		
}


function insert_offer_transaction( $user_id, $charity_select, $added_amount, $transaction_id, $offer_provider ) 
{
	// If $added_amount is 0 I don't need to add this, but I guess it'd be good to have
	// a record of all transactions
	
	$query = sprintf("insert into offers_user (user_id, charity_select, date, amount, transaction_id, offer_provider) 
			values (%d,%d,CURRENT_DATE(),%d,%d,%d)",
			$user_id, $charity_select, $added_amount, $transaction_id, $offer_provider);    		
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	
	
	if( $added_amount > 0 ) {

		// Update the points for the user in user_scores
		$query = sprintf("INSERT INTO user_scores(user_id,points,charity) values (%d,%d,%d) ON DUPLICATE KEY UPDATE points=points+%d",
							$user_id, $added_amount, $charity_select, $added_amount );
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());
		
		
		// Insert a user notification telling the user that he got the points. (don't bother if added_amount is <= 0)	
		$text = sprintf("Congratulations!<br/>Your completed offer has earned<br/>%d %s for %s!",
			$added_amount, get_charity_units($charity_select), get_charity_name($charity_select) );
		$text = mysql_real_escape_string($text); 
		$query = sprintf("insert into user_notifications (user_id, text, displayed)
			values (%d,'%s',FALSE)",
			$user_id, $text);    		
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());
		
	}
	
	
}
?>
