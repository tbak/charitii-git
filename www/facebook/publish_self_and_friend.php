<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require('config.inc.php');

if ($_POST['method']=='publisher_getFeedStory') {

	$images = array(array('src' => 'http://media.superdonate.org/imgtest/robot.png',
						  'href' => 'http://apps.facebook.com/superdonate'));

	$feed = array('template_id' => $feed_template_bundle_id,
				  'template_data' => array('foo' => 42,
                                         'images' => $images)
				  );

	// The response to publisher_getFeedStory
	$data = array('method'=> 'publisher_getFeedStory',
				  'content' => array( 'feed'    => $feed));
}
else {

	$ret = "<img src='http://media.superdonate.org/imgtest/robot.png' />";

	// The reponse to publisher_getInterface
	$data = array('content'=>array('fbml' => $ret,
                                 'publishEnabled' => true,
                                 'commentEnabled' => true),
                'method' => 'publisher_getInterface');

} 

echo json_encode($data);

?>
