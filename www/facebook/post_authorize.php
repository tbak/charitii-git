<?php
// Copyright 2009, SuperDonate.  All rights reserved.
/*
The user is redirected to this page when the application is first added.
Use this page to publish a one-line story in the user's feed, and then redirect to the main canvas page.
*/

require("config.inc.php");
require_once('../common.php');

$user = $facebook->require_login();



// Initial profile box with 0 points...
require('generate_profile.php');
generate_fbml();




// This makes the user use the "foo" ref fbml in the profile page
// TB TODO - Need a fb:ref for each user or some shit like that.
// Can I just do it once on post_authorize and then just set the fb:ref when it changes??? Or what??? WHAT!!!
function set_fbml_for_profile()
{
	global $facebook;
	global $user;

	// Screw this if the user isn't added or whatever
	if( $user <= 0 )
		return;

	$profileContent = "<fb:ref handle='profile".$user."' />";
				 
	$facebook->api_client->call_method('facebook.Fbml.setRefHandle',
	array(
		   'handle' => 'bla_w'.$user ,
		   'fbml' => $profileContent, // wide box
		  ) );
	$facebook->api_client->call_method('facebook.Fbml.setRefHandle',   
	array(
		   'handle' => 'bla_n'.$user,   
		   'fbml' => $profileContent, // narrow box and main box
		  )  );
	$facebook->api_client->call_method('facebook.profile.setFBML',
	array(
		   'uid' => $user,
		  'profile' => '<fb:wide><fb:ref handle="bla_w'.$user.'" /></fb:wide><fb:narrow><fb:ref handle="bla_n'.$user.'" /></fb:narrow>',
		   'profile_main' => '<fb:ref handle="bla_n'.$user.'" />'
	  ) );
	
}

set_fbml_for_profile();







$tokens = array(
  'album'=>'The White Album',
  'artist'=>'The Beatles',
  'score0'=>0,
  'score1'=>1,
  'score2'=>2,
  'score3'=>3,
  'images'=>array(array('src'=>'http://media.superdonate.org/imgtest/robot.png',
                        'href'=>'http://apps.facebook.com/charitii'))
);

//There is no target for the story in this example, but if there was,
//$target_ids would be an array of user IDs
$target_ids = array();

$body_general = '';

$facebook->api_client->feed_publishUserAction($feed_template_bundle_id_allcharities, 
                                              $tokens , 
                                              implode(',', $target_ids), 
                                              $body_general);








$facebook->redirect('http://apps.facebook.com/charitii');

?>