<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require "config.inc.php";
$facebook->require_frame();
$user = $facebook->require_login();

/*
foreach ( $_POST as $key => $value ) {
 print $key . " " . "=" . " " . $value;
 print "<BR/>";
}
*/

// Retrieve array of friends who've already authorized the app.
$fql = 'SELECT uid FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1='.$user.') AND is_app_user = 1';
$_friends = $facebook->api_client->fql_query($fql);

// Extract the user ID's returned in the FQL request into a new array.
$friends = array();
if (is_array($_friends) && count($_friends)) {
	foreach ($_friends as $friend) {
		$friends[] = $friend['uid'];
	}
}

// Convert the array of friends into a comma-delimeted string.
$friends = implode(',', $friends);

// Prepare the invitation text that all invited users will receive.
$content =
	"<fb:name uid=\"".$user."\" firstnameonly=\"true\" useyou=\"false\" shownetwork=\"false\"/> is donating to charity for free by playing <a href=\"http://apps.facebook.com/charitii/\">Charitii</a> and thought you should try it out!\n".
	"<fb:req-choice url=\"".$facebook->get_add_url()."\" label=\"Visit Charitii\"/>";

?>
<fb:request-form
	action="invite_process.php"
	method="post"
	type="Charitii"
	content="<? echo htmlentities($content); ?>"
	image="<? echo $app_image; ?>">
    
	<fb:multi-friend-selector
		actiontext="Select the friends you want to tell about Charitii!"
		exclude_ids="<? echo $friends; ?>"
		bypass="cancel" />
</fb:request-form>
