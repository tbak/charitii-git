function popupFeedDialogAllCharities(score0, score1, score2, score3)
{
	// TB TODO - Donated amounts!
	// And TOTAL donated amounts???
	// Some kind of happy message???
	var template_data = {
		'score0' : score0,
		'score1' : score1,
		'score2' : score2,
		'score3' : score3,
		'images' : [{src: 'http://www.superdonate.com/imgtest/test_take-care-of-your-heart.jpg', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity0_off.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity1_off.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity2_off.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity3_off.png', href:'http://apps.facebook.com/charitii'}
		]
	};

	<?php
	if( $game->is_facebook )
		echo "Facebook.showFeedDialog(".$feed_template_bundle_id_allcharities.", template_data);";
	?>
	
}

function popupFeedDialogSingleCharity(score, charity)
{
	var unit = "units";
	switch( charity )
	{
	case 0:
		unit = "ounces of water";
		break;
	case 1:
		unit = "grains of wheat";
		break;
	case 2:
		unit = "minutes of education";
		break;
	case 3:
		unit = "square inches of rain forest";
		break;
	}

	// TB TODO - Donated amounts!
	// And TOTAL donated amounts???
	// Some kind of happy message???
	var template_data = {
		'score' : score,
		'unit' : unit,
		'images' : [{src: 'http://www.superdonate.com/imgtest/test_take-care-of-your-heart.jpg', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity0_' + (charity == 0 ? 'on' : 'off') + '.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity1_' + (charity == 1 ? 'on' : 'off') + '.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity2_' + (charity == 2 ? 'on' : 'off') + '.png', href:'http://apps.facebook.com/charitii'},
					{src: 'http://www.charitii.com/test/charity3_' + (charity == 3 ? 'on' : 'off') + '.png', href:'http://apps.facebook.com/charitii'}
		]
	};

	<?php
	if( $game->is_facebook )
		echo "Facebook.showFeedDialog(".$feed_template_bundle_id_onecharity.", template_data);";
	?>
	
}


var did_submit = false;
function submitAnswer(n,token)
{ 
	if( did_submit )
		return;
	did_submit = true;

	document.getElementById("selected").setValue(n);	
	document.getElementById("form1").submit();

    
	// Put a border around what you selected (and remove any previous border)
	// It appears that once you have submitted the form, you can't resubmit it... 
	// so the border effect should lock on and then stay there.
	/*
	for( var i = 0; i < 4; i++ )
	{
		var element = document.getElementById('possibleAnswer' + i );
		element.setStyle( "border", "0px" );
	}
	*/

    var element = document.getElementById('possibleAnswer' + n );
	
    //element.setStyle( "border", "2px solid #0000ff" );
    element.setStyle( "background", "#00ccff" );
    disableAnswerFocus()
}

function activateCharity(elem_id)
{
    // Turn all off
	for( var i = 0; i < 4; i++ )
	{
		var elem = document.getElementById( "charitySelect" + i );
		//elem.className = "charitySelect" + i + "Off";
		elem.setClassName( "charitySelect" + i + "Off" );


		// Growth table
		elem = document.getElementById( "growthCharity" + i );
		elem.setClassName( "" );
	}

	var targetElem = document.getElementById( 'charitySelect' + elem_id );
	// Turn me on

	//targetElem.className = "charitySelect" + elem_id + "On";
	targetElem.setClassName( "charitySelect" + elem_id + "On" );


	//document.form1.charity_select.value = elem_id;
	document.getElementById("charity_select").setValue(elem_id);


	// Growth table
	elem = document.getElementById( "growthCharity" + elem_id );
	elem.setClassName( "selected" );
}
