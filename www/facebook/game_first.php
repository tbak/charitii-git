<?php // Copyright 2009, SuperDonate.  All rights reserved. ?>
<!--<img src="http://www.charitii.com/test/charity_select.png" /><br/>-->
<center>
<div id="charitySelect">

<script type="text/javascript">
<?php
require("main.js"); 
require("flash.js");
?>
</script>

<?php
//if( !isset($_POST['token']) )
//	echo '<div id="charitySelectIntro">Select what each correct word will donate</div>';


function output_charity_select_class($id)
{
	if( $id == $_SESSION['charity_select'] )
	{
		echo 'charitySelect' . $id . 'On';
	}
	else
	{
		echo 'charitySelect' . $id . 'Off';
	}
}

?>

<div id="charitySelect0" class="<?php output_charity_select_class(0); ?>"><a onclick="javascript:activateCharity(0);"></a></div>
<div id="charitySelect1" class="<?php output_charity_select_class(1); ?>"><a onclick="javascript:activateCharity(1);"></a></div>
<div id="charitySelect2" class="<?php output_charity_select_class(2); ?>"><a onclick="javascript:activateCharity(2);"></a></div>
<div id="charitySelect3" class="<?php output_charity_select_class(3); ?>"><a onclick="javascript:activateCharity(3);"></a></div>
<div class="clear"></div>

</div>
</center> 




<?php
// Isaac says it looks spammy. So try getting rid of this ad (2 per page now)
//display_ad_top(); 
?>

<div id="mainBox">

<div id="mainRow">

<div id="mainGame">

<?php
if( $is_first_question )
{
	echo '<div id="mainGameHeader">';
	echo 'Pick the answer that best fits the <span class="hilight">crossword puzzle clue</span>.<br/>';
	echo 'Each correct answer will <span class="hilight">donate to the charity</span> you have selected above.';
	echo '</div>';
}
?>

<?php
if( !$is_first_question )
{
	$game->output_correct_answer();	
}
?>

<?php $game->output_word_select_form() ?>


</div><!--mainGame-->



<div id="mainProgress">
<div id="mainProgressDonatedAmount">
<?php

if( $game->is_facebook )
{

	// Update total donated amounts
	//$facebook->api_client->data_setUserPreference( $user_preference_highest_score, $_SESSION['num_correct'] );
	
	$amount[0] = $facebook->api_client->call_method('facebook.data.getUserPreference', array('pref_id' => $user_preference_total_donations+0));
	$amount[1] = $facebook->api_client->call_method('facebook.data.getUserPreference', array('pref_id' => $user_preference_total_donations+1));
	$amount[2] = $facebook->api_client->call_method('facebook.data.getUserPreference', array('pref_id' => $user_preference_total_donations+2));
	$amount[3] = $facebook->api_client->call_method('facebook.data.getUserPreference', array('pref_id' => $user_preference_total_donations+3));
	
	if( $game->did_answer_correct ) {
		$amount[ $_SESSION['charity_select'] ] += Game::c_ounces_earned_per_correct;
		$facebook->api_client->data_setUserPreference( $user_preference_total_donations+$_SESSION['charity_select'], $amount[ $_SESSION['charity_select'] ] );


		// Update the profile box data as well...
		require('generate_profile.php');
		generate_fbml();
	}
	
	if( $amount[0] == "" ) $amount[0] = "0";
	if( $amount[1] == "" ) $amount[1] = "0";
	if( $amount[2] == "" ) $amount[2] = "0";
	if( $amount[3] == "" ) $amount[3] = "0";

}
else {
	$amount[0] = 40;
	$amount[1] = 50;
	$amount[2] = 60;
	$amount[3] = 70;
}
?>
    
<div id="growth">
&nbsp;
<div id="youHaveDonated">
You have donated:
</div>

<?php
function selclass($charity)
{
	if( $_SESSION['charity_select'] == $charity )
		echo 'class="selected"';
}
?>

<table>
<tr>
<td><a onclick="javascript:activateCharity(0);"><img src="http://media.superdonate.org/imgtest/progress_center_small_0.png" /></a></td>
<td id="growthCharity0" <?php selclass(0);?>><span class="donationNumber"><?php echo number_format($amount[0]); ?></span><br/>ounces of water</td>
</tr>
<tr>
<td><a onclick="javascript:activateCharity(1);"><img src="http://media.superdonate.org/imgtest/progress_center_small_1.png" /></a></td>
<td id="growthCharity1" <?php selclass(1);?>><span class="donationNumber"><?php echo number_format($amount[1]); ?></span><br/>grains of wheat</td>
</tr>
<tr>
<td><a onclick="javascript:activateCharity(2);"><img src="http://media.superdonate.org/imgtest/progress_center_small_2.png" /></a></td>
<td id="growthCharity2" <?php selclass(2);?>><span class="donationNumber"><?php echo number_format($amount[2]); ?></span><br/>minutes of education</td>
</tr>
<tr>
<td><a onclick="javascript:activateCharity(3);"><img src="http://media.superdonate.org/imgtest/progress_center_small_3.png" /></a></td>
<td id="growthCharity3" <?php selclass(3);?>><span class="donationNumber"><?php echo number_format($amount[3]); ?></span><br/>sq. inches of rainforest</td>
</tr>
</table>


<a href="#" onclick="<?php echo 'popupFeedDialogAllCharities('.$amount[0].','.$amount[1].','.$amount[2].','.$amount[3].')'; ?>">
<div id="clickToPostRow">

<div id="clickToPostImage">
<img src="http://media.superdonate.org/imgtest/test_take-care-of-your-heart.jpg" width="40" height="40" />
</div>
<div id="clickToPostText">
Click to post your donation amounts<br/>
on your wall
</div>
<div class="clear"></div>

</div>
</a>

<div id="puzzleDifficulty">
<?php
echo "Puzzle difficulty: " . $_SESSION['difficulty'];
?>
</div>

</div>


<?php

// Auto show the feed dialog on 100 and 500 units donated (but only do this once!)
$did_feed_100 = $facebook->api_client->call_method('facebook.data.getUserPreference', array('pref_id' => $user_preference_did_autopopup_feed_dialog_100));
$did_feed_500 = $facebook->api_client->call_method('facebook.data.getUserPreference', array('pref_id' => $user_preference_did_autopopup_feed_dialog_500));

if( $game->is_facebook && !$did_feed_500 && $amount[$_SESSION['charity_select'] ] >= 500 )
{
	$facebook->api_client->data_setUserPreference( $user_preference_did_autopopup_feed_dialog_500, 1 );
	echo '<script type="text/javascript">popupFeedDialogSingleCharity('.$amount[$_SESSION['charity_select'] ].','.$_SESSION['charity_select'].');</script>';
}
else if( $game->is_facebook && !$did_feed_100 && ($amount[0] >= 100 || $amount[1] >= 100 || $amount[2] >= 100 || $amount[3] >= 100 ) )
{
	$facebook->api_client->data_setUserPreference( $user_preference_did_autopopup_feed_dialog_100, 1 );
	echo '<script type="text/javascript">popupFeedDialogSingleCharity('.$amount[$_SESSION['charity_select'] ].','.$_SESSION['charity_select'].');</script>';
}
?>







</div>
</div><!--mainProress-->



</div><!--mainRow-->


<div class="clear"></div>

<?php
if( $is_first_question ) {
	require('display_instructions.php');
	echo '<script type="text/javascript">initFlashEffects(); startCharitySelectEffect();</script>';
}
?>

<?php
//if( $is_first_question )
//	require("display_tips.php");
?>



<?php
display_ad_bottom();
?>



</div><!--mainBox-->





