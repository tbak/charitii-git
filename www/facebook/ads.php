<?php
// Copyright 2009, SuperDonate.  All rights reserved.
// AD stuff
global $ad_application;

$ad_network = -1;
if( $ad_application == "charitii" )
	$ad_network = 1;
else if( $ad_application == "chrosschoice" )
	$ad_network = 1;

if( $ad_network == 1 )
{
	if( !$game->is_facebook )
		return;

	// Retrieve age + sex since we can use that
	$user_details = $facebook->api_client->users_getInfo($user, array('sex','birthday'));
	$sex = $user_details[0]['sex'];
	$birthday = $user_details[0]['birthday'];

    $bday_ts = strtotime($birthday);
	$ad_age = date("Y") - date("Y", $bday_ts);
	if (date("md") < date("md", $bday_ts))
		$ad_age--;

    if($sex == "male")
		$ad_sex = "m";
	else
        $ad_sex = "f";
}

function display_ad_crosssell()
{
	global $game, $is_first_question, $ad_network;

	if( !$game->is_facebook )
		return;

	if( $ad_network == 0 )
	{
		
	}
	else if( $ad_network == 1 )
	{
		//global $ad_sex, $ad_age, $user;
		//echo "<fb:iframe src='http://rya.rockyou.com/ams/ad.php?placeguid=D7C7C27282&type=CrossSell&popup=1&fb_sig_user=".$user."&age=".$ad_age."&gender=".$ad_sex."' style='border:0px;' smartsize='true' scrolling='no' frameborder='0'/>";
		
		echo "<fb:iframe src='http://rya.rockyou.com/ams/ad.php?placeguid=D7C7C27282&type=CrossSell&popup=1' style='border:0px;' smartsize='false' width='646' height='250' scrolling='no' frameborder='0'/>";
	}
    
}

function display_ad_fbbanner()
{
	global $game, $is_first_question, $ad_network;

	// Always show this ad since otherwise there will just be weird white space
	/*
	if( $is_first_question )
		return;
	*/

	if( !$game->is_facebook )
		return;

	if( $ad_network == 0 )
	{
		
	}
	else if( $ad_network == 1 )
	{
		global $ad_sex, $ad_age, $user;
		echo "<div class='adFBBanner'>";
		echo "<fb:iframe src='http://rya.rockyou.com/ams/ad.php?placeguid=541F627283&type=Banner&popup=1&fb_sig_user=".$user."&age=".$ad_age."&gender=".$ad_sex."' style='border:0px;' width='646' height='60' scrolling='no' frameborder='0'/>";
		echo "</div>";
	}
}

function display_ad_top()
{
	global $game, $is_first_question, $ad_network;
	if( $is_first_question )
		return;

	if( !$game->is_facebook )
		return;

	if( $ad_network == 0 )
	{
		echo "<fb:iframe src='http://social.bidsystem.com/displayAd.aspx?pid=347192&plid=15965&adSize=728x90&bgColor=%23ffffff&textColor=%23000000&linkColor=%230033ff&channel=top&appid=62090' width='728' height='90' frameborder='0' border='0' scrolling='no'></fb:iframe>";
	}
	else if( $ad_network == 1 )
	{
		global $ad_sex, $ad_age, $user;

		// Tip: Append &fb_sig_user=[facebook user id]&age=xx&gender=x to your iframe src to get higher eCPM. (gender = m or f) 
        //echo "<fb:iframe src='http://rya.rockyou.com/ams/ad.php?placeguid=663B827269&type=Leaderboard&popup=1' style='border:0px;' width='728' height='90' scrolling='no' frameborder='0'/>";
		echo "<fb:iframe src='http://rya.rockyou.com/ams/ad.php?placeguid=663B827269&type=Leaderboard&popup=1&fb_sig_user=".$user."&age=".$ad_age."&gender=".$ad_sex."' style='border:0px;' width='728' height='90' scrolling='no' frameborder='0'/>";
	}
}

function display_ad_bottom()
{
	global $game, $is_first_question, $ad_network;
	if( $is_first_question )
		return;

	if( !$game->is_facebook )
		return;

	if( $ad_network == 0 )
	{
		echo "<fb:iframe src='http://social.bidsystem.com/displayAd.aspx?pid=347192&plid=15965&adSize=728x90&bgColor=%23ffffff&textColor=%23000000&linkColor=%230033ff&channel=bottom&appid=62090' width='728' height='90' frameborder='0' border='0' scrolling='no'></fb:iframe>";
	}
	else if( $ad_network == 1 )
	{
		global $ad_sex, $ad_age, $user;

		// Tip: Append &fb_sig_user=[facebook user id]&age=xx&gender=x to your iframe src to get higher eCPM. (gender = m or f) 
        //echo "<fb:iframe src='http://rya.rockyou.com/ams/ad.php?placeguid=4B1FC27271&type=Leaderboard&popup=1' style='border:0px;' width='728' height='90' scrolling='no' frameborder='0'/>";
		echo "<fb:iframe src='http://rya.rockyou.com/ams/ad.php?placeguid=4B1FC27271&type=Leaderboard&popup=1&fb_sig_user=".$user."&age=".$ad_age."&gender=".$ad_sex."' style='border:0px;' width='728' height='90' scrolling='no' frameborder='0'/>";
	}
}

?>
