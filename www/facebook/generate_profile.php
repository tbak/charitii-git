<?php
// Copyright 2009, SuperDonate.  All rights reserved.
//require("config.inc.php");
//generate_fbml();

function get_ordinal($number) {
	// get first digit
	$digit = abs($number) % 10;
	$ext = 'th';
	$ext = ((abs($number) %100 < 21 && abs($number) %100 > 4) ? 'th' : (($digit < 4) ? ($digit < 3) ? ($digit < 2) ? ($digit < 1) ? 'th' : 'st' : 'nd' : 'rd' : 'th'));
	return $number.$ext;
}

function build_profile_box()
{
	global $user,$facebook;


	$amount[0] = $facebook->api_client->call_method('facebook.data.getUserPreference', array('pref_id' => $user_preference_total_donations+0));
	$amount[1] = $facebook->api_client->call_method('facebook.data.getUserPreference', array('pref_id' => $user_preference_total_donations+1));
	$amount[2] = $facebook->api_client->call_method('facebook.data.getUserPreference', array('pref_id' => $user_preference_total_donations+2));
	$amount[3] = $facebook->api_client->call_method('facebook.data.getUserPreference', array('pref_id' => $user_preference_total_donations+3));

	if( $amount[0] == "" ) $amount[0] = "0";
	if( $amount[1] == "" ) $amount[1] = "0";
	if( $amount[2] == "" ) $amount[2] = "0";
	if( $amount[3] == "" ) $amount[3] = "0";


	$str = "";

	$str .= '<fb:subtitle><a href="http://apps.facebook.com/charitii">Play and donate now</a></fb:subtitle>';

	//$str .= "My name is " . "<fb:name uid=\"" . $user . "\" firstnameonly=\"true\" useyou=\"false\" shownetwork=\"false\" />";

	$str .= '<fb:name uid="'.$user.'" firstnameonly="true" useyou="false" shownetwork="false" /> has donated<br/>';

	//"I have donated ".$amount[0]." Wheat, ".$amount[1]." Water, ".$amount[2]." Education, ".$amount[3]." Rainforest<br/>";

	//$user_details = $facebook->api_client->users_getInfo($user, array('last_name','first_name'));
	//$first_name = $user_details[0]['first_name'];
	//$str .= $first_name . ' got a CrossChoice high score of <b>'.$highest_score.'</b> points.<br/><br/>';
	//$str .= $first_name . ' is ranked <b>#'.get_ordinal($position).'</b> out of all players.';

    $str .= '    
	<table>
	<tr>
	<td><a href="http://apps.facebook.com/superdonate"><img src="http://media.superdonate.org/imgtest/progress_center_small_0.png" /></a></td>
	<td><b>' . number_format($amount[0]) . '</b><br/>ounces of water</td>
	</tr>
	<tr>
	<td><a href="http://apps.facebook.com/superdonate"><img src="http://media.superdonate.org/imgtest/progress_center_small_1.png" /></a></td>
	<td><b>' . number_format($amount[1]) . '</b><br/>grains of wheat</td>
	</tr>
	<tr>
	<td><a href="http://apps.facebook.com/superdonate"><img src="http://media.superdonate.org/imgtest/progress_center_small_2.png" /></a></td>
	<td><b>' . number_format($amount[2]) . '</b><br/>minutes of education</td>
	</tr>
	<tr>
	<td><a href="http://apps.facebook.com/superdonate"><img src="http://media.superdonate.org/imgtest/progress_center_small_3.png" /></a></td>
	<td><b>' . number_format($amount[3]) . '</b><br/>sq. inches of rainforest</td>
	</tr>
	</table>';

	$str .= '<br/>Charitii is a multiple choice crossword puzzle that donates to charity for each correct answer.';

	$str .= '<br/><br/><a href="http://apps.facebook.com/charitii">Play and donate now<img src="http://media.superdonate.org/imgtest/robot16x16.png" /></a><br/>';
	

	return $str;
}


function generate_fbml()
{
	global $facebook, $appid, $user;


	// Use my infinite session
	// Array ( [session_key] => 23022050cabff32b8c5213d4-636045770 [uid] => 636045770 [expires] => 0 )
	/*
    $facebook->api_client->user = 636045770;
	$facebook->api_client->session_key = "23022050cabff32b8c5213d4-636045770";
	$facebook->api_client->expires = 0;
	*/

	

    //$fbml = "bar ref handle";
	$fbml = build_profile_box();

	$facebook->api_client->fbml_setRefHandle("profile".$user, $fbml);

	//echo "YER FBML WAS SET TO: <br/><br/>" . $fbml . "<br/><br/>";

	// This string is searched for by the java admin tool to indicate success
	//echo "***SUCCESS***";

}

?>