<?php
// Copyright 2009, SuperDonate.  All rights reserved.
?>

<br/><br/>
<div class="magicRow2">
	<div class="magicRowItemChart">
		<a href="http://www.charitii.com/progress.php">
			<?php draw_chart_for_months(380,170); ?>	
		</a>
	</div>
	<div class="magicRowItemChart">
		<a href="http://www.charitii.com/progress.php">
			<?php draw_chart_for_current_month(380,170); ?>	
		</a>
	</div>
	<div class="clear"> </div>
</div>
