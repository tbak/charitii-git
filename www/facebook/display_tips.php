<?php // Copyright 2009, SuperDonate.  All rights reserved. ?>
<div class="magicRow2PicBox" >

<div id="tips">
<h2>How to solve the word puzzles</h2>
<p>The puzzles are presented as clues in a style found in popular crossword puzzles. The question topics can be about any subject matter. There are a few tips that can help you understand the clues better.</p>
<ul>
<li>Just like regular crossword puzzles, answers will have no spaces or punctuation between words. This means that if you see "ONSALE" as one of the answers, it should be read as "ON SALE". Knowing how to read the answers is part of the game!</li>
<li>Clues that are plural (more than one) must have a plural answer. For example, if the clue is "Squirrels", the answer must be plural. "RODENTS" could be an answer, but "RODENT" will never be correct since it is not plural. You can use this knowledge to filter out some of the possible answers.</li>
<li>A clue with three dashes indicates that you must fill in the blank with the correct answer. For example, "_ _ _ cheese" would be solved with "BLUE cheese", while "Cheese _ _ _" would be solved with "Cheese BURGER". </li>
<li>If a clue has an abbreviation, the answer must also be an abbreviation. If you see "Government org.", you must find an abbreviated government organization in the possible answers below, such as "FBI" or "CIA". Similarly, if a clue ends with "abbr.", the answer must be an abbreviation. </li>
<li>A clue that ends with a question mark (?) usually indicates a clever play on words or an unusual interpretation of the words. You may have to think outside of the box to come up with the right answer. </li>
<li>"e.g." stands for the Latin phrase "exempli gratia", which translates to "for example". It indicates that the clue is an example of one of the answers. "Football, e.g." is an example of a SPORT, so that is the answer. </li>
</ul>
</div>

</div>
