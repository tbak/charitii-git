//
// Copyright 2009, SuperDonate.  All rights reserved.
// FLASH EFFECT
// 

var bodyLoadTime = 0;

function d2h(d) 
{
	return d.toString(16);
}

function flashCell( cell, timeSinceChange, duration, startColor, endColor )
{
    if( timeSinceChange < duration )
	{
		//console.log("flashCell... timeSinceChange" + timeSinceChange + "  duration" + duration );

		var factor = timeSinceChange / duration;

        var r = Math.floor( startColor.r + (( endColor.r-startColor.r) * factor) );
		var g = Math.floor( startColor.g + (( endColor.g-startColor.g) * factor) );
		var b = Math.floor( startColor.b + (( endColor.b-startColor.b) * factor) );
        


		// TB TODO - Is there no way to set the style with a decimal value instead???
		/*

		// Make it hex, 2 digits always (0C instead of just C)
		if( r < 16 ) { r = '0' + d2h(r) } else { r = d2h(r) }
		if( g < 16 ) { g = '0' + d2h(g) } else { g = d2h(g) }
		if( b < 16 ) { b = '0' + d2h(b) } else { b = d2h(b) }

        
		var col = '#' + r + g + b;

		// TB FB
		//cell.style.backgroundColor = col;

        
		cell.setStyle( "backgroundColor", col );
		*/

        var color = r+', '+g+', '+b; 
		cell.setStyle('backgroundColor', 'rgb('+color+')'); 
	}
	else
	{

				// TB FB
		//cell.style.backgroundColor = '#ffffff';
		//cell.setStyle( "backgroundColor", "#ffffff" );

		var color = endColor.r+', '+endColor.g+', '+endColor.b; 
		cell.setStyle('backgroundColor', 'rgb('+color+')'); 
	}


}

function Color( r, g, b )
{
	this.r = r;
	this.g = g;
	this.b = b;
}

function FlashData()
{
	this.startTime = 0;
	this.duration = 0;
	this.element = 0;
	this.addTime = 0;
	this.enabled = 0;

	//this.startColor;
	//this.endColor;

	// TB TODO - start + end color!
}

var lastUpdateMS = 0;
var numFlashes = 0;
var flashes = [];
var updateTriggered = false;
function addFlash( element, startTime, duration, startColor, endColor, finalColor )
{
	if( element == null )
	{
		return;
	}

	var flash = new FlashData();
    
	flash.startTime = startTime;
	flash.duration = duration;
	flash.element = element;

	var currentDate = new Date();
	flash.addTime = currentDate.getTime();
	flash.enabled = true;

	flash.startColor = startColor;
	flash.endColor = endColor;
	flash.finalColor = finalColor;

	// Add to the list
	flashes[numFlashes] = flash;
    numFlashes++;
}

function startCorrectAnswerEffect()
{
	var startColor = new Color(255,255,255);
	var endColor = new Color(128,255,128);
    
	addFlash( document.getElementById('mainGameAnswerCorrect'), 0, 250, startColor, endColor, endColor );
	addFlash( document.getElementById('mainGameAnswerCorrect'), 250, 250, startColor, endColor, endColor );
	addFlash( document.getElementById('mainGameAnswerCorrect'), 500, 250, startColor, endColor, endColor );

	if( !updateTriggered )
		update();
}

var didStartPossibleAnswerFlash = false;
function startPossibleAnswerFlash()
{
	if( didStartPossibleAnswerFlash )
		return;
	didStartPossibleAnswerFlash = true;

	// It's kind of irritating?
	//return;

	var startColor = new Color(255,255,255);
	var endColor = new Color(128,255,128);
	var finalColor = new Color(255,255,255);

	//console.log("adding");
    addFlash( document.getElementById('possibleAnswer0'), 0, 300, startColor, endColor, finalColor );
	addFlash( document.getElementById('possibleAnswer1'), 100, 300, startColor, endColor, finalColor );
	addFlash( document.getElementById('possibleAnswer2'), 200, 300, startColor, endColor, finalColor );
	addFlash( document.getElementById('possibleAnswer3'), 300, 300, startColor, endColor, finalColor );
	
	//console.log("done");

	if( !updateTriggered )
		update();
}

function update()
{
	updateTriggered = true;

	var currentDate = new Date();
	var time = currentDate.getTime();
    
	//console.log("update");
    if( bodyLoadTime > 0 && time - bodyLoadTime > 5000 )
	{
		
		// End everything
		return;
	}

	for( var i = 0; i < numFlashes; i++ )
	{
		var flash = flashes[i];
		if( !flash.enabled )
		{
			continue;
		}

		var diff = time - flash.addTime;

		if( diff > flash.startTime )
		{
			flashCell( flash.element, diff - flash.startTime, flash.duration, flash.startColor, flash.endColor );
		}

		if( diff - flash.startTime > flash.duration )
		{
			var color = flash.finalColor.r+', '+flash.finalColor.g+', '+flash.finalColor.b; 
			flash.element.setStyle('backgroundColor', 'rgb('+color+')'); 

			flash.enabled = false;
		}
	}


	lastUpdateMS = currentDate.getTime();

   	timeout = 50; // 20 fps

	// TB FB
	//window.setTimeout("update();", timeout);
	setTimeout( function() { update(); }, timeout);
}

var charitySelectEffectStartTime = 0;

function initFlashEffects()
{
	// In a regular web page this is set in body.onload but I don't think that exists for facebook???
	var currentDate = new Date();
	bodyLoadTime = currentDate.getTime();
}

function startCharitySelectEffect()
{
	var currentDate = new Date();
	charitySelectEffectStartTime = currentDate.getTime();

	updateCharitySelectEffect();
}

function updateCharitySelectEffect()
{
	//console.log("CS2");

	var currentDate = new Date();
	var time = currentDate.getTime();
	var diff = time - charitySelectEffectStartTime;

	if( diff < 150 )
	{
		activateCharity(0);
	}
	else if( diff < 300 )
	{
		activateCharity(1);
	}
	else if( diff < 450 )
	{
		activateCharity(2);
	}
	else if( diff < 600 )
	{
		activateCharity(3);
	}
	else if( diff < 750 )
	{
		activateCharity(2);
	}
	else if( diff < 900 )
	{
		activateCharity(1);
	}
	else if( diff < 1150 )
	{
		activateCharity(0);
		startPossibleAnswerFlash();
	}
	else
	{
		return;
	}

	timeout = 50; // 20 fps

	// TB FB
    //window.setTimeout("updateCharitySelectEffect();", timeout);
	setTimeout( function() { updateCharitySelectEffect(); }, timeout);
}

var answerIsTouched = [];
var answerIntensity = [];
var answerUpdateTriggered = false;
var answerFocusStartColor = new Color(128,255,128);
var answerFocusEndColor = new Color(255,255,255);
var answerFocusEnabled = true;
function answerMouseOver( id )
{
	//console.log("answerMouseOver");

	answerIsTouched[id] = true;

	if( !answerUpdateTriggered )
	{
		answerIntensity[0] = answerIntensity[1] = answerIntensity[2] = answerIntensity[3] = 0;
		answerIsTouched[0] = answerIsTouched[1] = answerIsTouched[2] = answerIsTouched[3] = false;
		answerIsTouched[id] = true;
		updateAnswerFocus();
	}

	//console.log("yyy");
}

function answerMouseOut( id )
{
	answerIsTouched[id] = false;
}

function disableAnswerFocus()
{
	answerFocusEnabled = false;
}

function enableAnswerfocus()
{
	answerFocusEnabled = true;
	answerUpdateTriggered = false;
}

function updateAnswerFocus()
{
	//console.log("updateAnswerFocus");
	
	if( !answerFocusEnabled )
		return;

	answerUpdateTriggered = true;
    
	for( var i = 0; i < 4; i++ )
	{

		if( answerIsTouched[i] )
		{
			answerIntensity[i] += 0.2;
		}
		else
		{
			answerIntensity[i] -= 0.4;
		}
		if( answerIntensity[i] > 1.0 )
			answerIntensity[i] = 1.0;
		else if( answerIntensity[i] < 0.0 )
			answerIntensity[i] = 0.0;

		var element = document.getElementById('possibleAnswer' + i );
		flashCell( element, 500 - answerIntensity[i]*500, 500, answerFocusStartColor, answerFocusEndColor );

		if( i == 0 )
		{
			 //console.log(500 - answerIntensity[i]*500);
		}
	}
	
	timeout = 50; // 20 fps

	// TB FB
    // window.setTimeout("updateAnswerFocus();", timeout);
	setTimeout( function() { updateAnswerFocus(); }, timeout);
}

