<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require("config.inc.php");
require_once('../common.php');
require_once('../game_database.php');
require('../progress_totals.php'); 
$totals = new Totals();

$game = new Game();

if( $game->is_facebook )
	$user = $facebook->require_login();

echo '<style>';
require_once("fb.css");
echo '</style>';

$ad_application = "charitii";
require('ads.php');
?>

<?php 
$show_home = TRUE;
require('display_header.php'); 
?>

<div id="subtitle">Stats</div>
<br/>

<p>
On this page you can see how much water, food, education and rainforest land have already been donated. The information is arranged by day and month.
</p>

<br/>
<?php draw_chart_for_months(600,200); ?>
<br/>

<br/>
<?php draw_chart_for_current_month(600,200); ?>
<br/>

<?php display_ad_bottom(); ?>

<table border="0" cellspacing="5">
<?php
$totals->echo_for_all_months();
?>
</table>
<hr>
<br>
<table border="0" cellspacing="5">
<?php
$totals->echo_for_current_month();
?>
</table>

<br/>
<p>
<i>Charitii.com was launched on August 26, 2008</i><br/>
<i>Charitii for facebook was launched on April 9, 2009</i>
</p>



<fb:google-analytics uacct="UA-5390483-8" />
