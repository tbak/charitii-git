<?php
// Copyright 2009, SuperDonate.  All rights reserved.

// TB - For some reason PATH_INFO does not get set on the 1&1 servers... This should fix it...
// It's needed in SetupSources($options)...
if( isset($_SERVER['ORIG_PATH_INFO']) )
{
	$_SERVER['PATH_INFO'] = urldecode($_SERVER['ORIG_PATH_INFO']);
}

require 'minify_config.php';
require 'minify_groupsSources.php';
require 'Minify.php';

/*
if ($minifyCachePath) {
    //Minify::useServerCache($minifyCachePath);
    Minify::setCache($minifyCachePath);
} 
*/
Minify::setCache($min_cachePath);  


$serveOptions = array(
    'groups' => $groupsSources,
	'maxAge' => 31536000 // 1 yr    
);

// pass the 'prependRelativePath' to the CSS minifier:
// No longer needed in minify 2.1
/*
if( $_SERVER['SERVER_NAME'] == 'localhost' )
{
	$serveOptions['perType'][Minify::TYPE_CSS]['prependRelativePath'] = '/puz2/';
}
else
{
	$serveOptions['perType'][Minify::TYPE_CSS]['prependRelativePath'] = '/';
}
*/


Minify::serve( 'Groups', $serveOptions );
