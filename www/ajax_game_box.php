<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require('common.php');
require('game_database.php');
$game = new Game();
$game->generate_valid_question();
$is_first_question = !isset($_POST['token']);
?>

<script type="text/javascript">
resetDidSubmit();
</script>

<?php require "game_box.php"; ?>
