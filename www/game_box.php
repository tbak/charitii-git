<?php // Copyright 2009, SuperDonate.  All rights reserved. ?>

<script type="text/javascript">initFlashEffects();</script>

<div id="mainGame">

<div id="mainGameAjaxLoad">
<img src="test/ajax_load.gif" />
</div>

<?php
$game->check_referral_bonus_game_box();
$game->check_notifications_game_box();
if( $is_first_question )
{
	echo '<div id="mainGameHeader">';
	echo 'Below is a crossword puzzle clue followed by 4 possible answers.<br/>';
	echo 'Pick the answer that best fits the crossword puzzle clue.<br/>';
	echo '</div>';
}
else {
	$game->output_correct_answer();	
}
?>

<?php $game->output_word_select_form() ?>


</div><!--mainGame-->



<div class="clear"></div>

