// Copyright SuperDonate, Inc.

function ajaxLoadGameBox(n, token)
{

	$("#magicRow2Game").load("ajax_game_box.php", {selected: n,  token: token }, 
		function() { 
			$("#mainGameAjaxLoad").hide();
		    document.body.style.cursor='auto'; 
		} 
	);

}

function ajaxLoadOffer(unit_name, charity_name, uid, charity_select)
{
	$("#superRewardsBox").load("offer.php", {unit_name:unit_name, charity_name:charity_name, uid:uid, charity_select:charity_select}, 
			function() {				 
			} 
	);
}


var num_submissions = 0;
var did_submit = false;
function submitAnswer(n,token)
{ 
	if( did_submit )
		return;
	did_submit = true;

	num_submissions++;

	// If the page is ajax loaded, explicitly reset them.
	// Otherwise this will reset when the page completely reloads itself.
	document.body.style.cursor='progress';
	$("#mainGameAjaxLoad").show();

	if( num_submissions == 5 ) {

		// Reload the entire page every once in a while to get some new ads rollin'
		num_submissions = 0;

		// For some reason chrome is setting this value to 0 if the form had previously
		// been reloaded through ajax... It's like it's setting the old form1 value???
		// Has something to do with the name of the field being "selected"??? So rename it "choice"???
		// Nope didn't fix it!

		//alert( document.form1.token.value );
		//alert( document.form1.selected.value );

		document.form1.selected.value=n;

		

		document.form1.submit();
	}
	else {		
		ajaxLoadGameBox(n,token);
	}

	var element = document.getElementById('possibleAnswer' + n );
	
	//element.style.background = "#88ffff";
	element.style.background = "#00ccff";
	
	disableAnswerFocus();
}

function resetDidSubmit()
{
	did_submit = false;

	initFlashEffects();
	resetFlashes();

	// Thickbox
	// The charity select is no longer thickbox/ajax, so we don't need this.
	//tb_init('a.thickbox, area.thickbox, input.thickbox');	
	tb_init('a#gameBoxRegister, a#mainGameSendBonusLink');

	answerMouseOut(0);
	answerMouseOut(1);
	answerMouseOut(2);
	answerMouseOut(3);
	
	enableAnswerfocus();
}

function submitOptionNumMissingLetters(n)
{ 
  document.formOptions.num_missing_letters.value=n;
  document.formOptions.submit();
}

function submitOptionDifficulty(n)
{ 
  document.formOptions.difficulty.value=n;
  document.formOptions.submit();
}

function submitOptionWordLength(n)
{ 
  document.formOptions.word_length.value=n;
  document.formOptions.submit();
}

function preload(turn)
{

}

// This should correspond with the left parameter for div#answerSuggest
var suggestMinBounceX = 280;
var suggestMaxBounceX = 300;
var suggestBounceX = suggestMaxBounceX;
var suggestBounceDir = -1;
var suggestBounceSpeed = 4;
var bounceCount = 0;
var maxBounceCount = 3;

var bodyLoadTime = 0;
/*
window.onload = function()
{
	//This is called by initflasheffects
	//var currentDate = new Date();
	//bodyLoadTime = currentDate.getTime();
	
	
	// TB TEST - bounce the correct answer suggest
	// This is now done from the start function!
	//bounceAnswerSuggest();
}
*/

function startBounceAnswerSuggest()
{
	//bounceAnswerSuggest();
	window.setTimeout("bounceAnswerSuggest();", 1000);
}

function bounceAnswerSuggest()
{
	// Disable for now.
	//return;
	// console.log("bounce! " + suggestBounceX);
	
	if( suggestBounceX <= suggestMinBounceX ) {
		suggestBounceDir = 1;
		bounceCount++;
		if( bounceCount == maxBounceCount ) {
			// Wait a period of time... and then do another bounce...?
			bounceCount = 0;
			window.setTimeout("bounceAnswerSuggest();", 5000);
			return;
		}
	}
	else if(suggestBounceX >= suggestMaxBounceX) {
		suggestBounceDir = -1;		
	}
	
	suggestBounceX += suggestBounceDir * suggestBounceSpeed;
	
	var suggest = document.getElementById( "answerSuggest" );
	if( suggest == null ) {
		return;
	}
	suggest.style.left = '' + suggestBounceX + 'px';	
		
	
	timeout = 50; // 20 fps
	window.setTimeout("bounceAnswerSuggest();", timeout);
}


function activateCharity(elem_id)
{
    // Turn all off
	for( var i = 0; i < 4; i++ )
	{
		var elem = document.getElementById( "charitySelect" + i );
		elem.className = "charitySelect" + i + "Off";

		// Growth table
		elem = document.getElementById( "growthCharity" + i );
		elem.className = "";
	}

	var targetElem = document.getElementById( 'charitySelect' + elem_id );
	// Turn me on

	targetElem.className = "charitySelect" + elem_id + "On";
	//targetElem.style.display = "none";


	document.form1.charity_select.value = elem_id;


	// Growth table
	elem = document.getElementById( "growthCharity" + elem_id );
	elem.className = "selected";
}

//
// FLASH EFFECT
// 

var bodyLoadTime = 0;

function d2h(d) 
{
	return d.toString(16);
}

function flashCell( cell, timeSinceChange, duration, startColor, endColor )
{
    if( timeSinceChange < duration )
	{
		//console.log("flashCell... timeSinceChange" + timeSinceChange + "  duration" + duration );

		var factor = timeSinceChange / duration;

        var r = Math.floor( startColor.r + (( endColor.r-startColor.r) * factor) );
		var g = Math.floor( startColor.g + (( endColor.g-startColor.g) * factor) );
		var b = Math.floor( startColor.b + (( endColor.b-startColor.b) * factor) );

        var color = r+', '+g+', '+b; 
		cell.style.backgroundColor = 'rgb('+color+')'; 
	}
	else
	{

		var color = endColor.r+', '+endColor.g+', '+endColor.b; 
		cell.style.backgroundColor = 'rgb('+color+')';
	}


}

function Color( r, g, b )
{
	this.r = r;
	this.g = g;
	this.b = b;
}

function FlashData()
{
	this.startTime = 0;
	this.duration = 0;
	this.element = 0;
	this.addTime = 0;
	this.enabled = 0;
}

var lastUpdateMS = 0;
var numFlashes = 0;
var flashes = [];
var updateTriggered = false;

function addFlash( element, startTime, duration, startColor, endColor, finalColor )
{
	if( element == null )
	{
		return;
	}

	var flash = new FlashData();
    
	flash.startTime = startTime;
	flash.duration = duration;
	flash.element = element;

	var currentDate = new Date();
	flash.addTime = currentDate.getTime();
	flash.enabled = true;

	flash.startColor = startColor;
	flash.endColor = endColor;
	flash.finalColor = finalColor;

	// Add to the list
	flashes[numFlashes] = flash;
    numFlashes++;
}

function addCommas(nStr){
	 nStr += '';
	 x = nStr.split('.');
	 x1 = x[0];
	 x2 = x.length > 1 ? '.' + x[1] : '';
	 var rgx = /(\d+)(\d{3})/;
	 while (rgx.test(x1)) {
		 x1 = x1.replace(rgx, '$1' + ',' + '$2');
	 }
	 return x1 + x2;
}

function cyclePoints(curr, target)
{
	$("#di_number").html( addCommas(curr) );

	if( curr == target )
	{
		return;
	}

	window.setTimeout(function() {
		cyclePoints(curr+1, target);
	}, 70);
}

function startCorrectAnswerEffect(oldScore, newScore)
{
	var startColor = new Color(255,255,255);
	var endColor = new Color(128,255,128);
    
	addFlash( document.getElementById('mainGameAnswerCorrect'), 0, 250, startColor, endColor, endColor );
	addFlash( document.getElementById('mainGameAnswerCorrect'), 250, 250, startColor, endColor, endColor );
	addFlash( document.getElementById('mainGameAnswerCorrect'), 500, 250, startColor, endColor, endColor );

	if( !updateTriggered ) {
		update();
	}

	cyclePoints(oldScore,newScore);
}

var didStartPossibleAnswerFlash = false;
function startPossibleAnswerFlash()
{
	if( didStartPossibleAnswerFlash )
		return;
	didStartPossibleAnswerFlash = true;

	// It's kind of irritating?
	return;

	var startColor = new Color(255,255,255);
	var endColor = new Color(128,255,128);
	var finalColor = new Color(255,255,255);

	//console.log("adding");
    //addFlash( document.getElementById('possibleAnswer0'), 0, 500, startColor, endColor, finalColor );
	addFlash( document.getElementById('possibleAnswer0'), 1000, 1000, startColor, endColor, endColor );
	addFlash( document.getElementById('possibleAnswer0'), 2000, 1000, endColor, startColor, startColor );
	
	addFlash( document.getElementById('possibleAnswer1'), 1500, 1000, startColor, endColor, endColor );
	addFlash( document.getElementById('possibleAnswer1'), 2500, 1000, endColor, startColor, startColor );
	
	addFlash( document.getElementById('possibleAnswer2'), 2000, 1000, startColor, endColor, endColor );
	addFlash( document.getElementById('possibleAnswer2'), 3000, 1000, endColor, startColor, startColor );
	
	addFlash( document.getElementById('possibleAnswer3'), 2500, 1000, startColor, endColor, endColor );
	addFlash( document.getElementById('possibleAnswer3'), 3500, 1000, endColor, startColor, startColor );
    
    
	//addFlash( document.getElementById('possibleAnswer1'), 700, 500, startColor, endColor, finalColor );
	//addFlash( document.getElementById('possibleAnswer2'), 1200, 500, startColor, endColor, finalColor );
	//addFlash( document.getElementById('possibleAnswer3'), 1700, 500, startColor, endColor, finalColor );
	//addFlash( document.getElementById('possibleAnswer2'), 1500, 500, startColor, endColor, finalColor );
	//addFlash( document.getElementById('possibleAnswer1'), 1800, 500, startColor, endColor, finalColor );
	//addFlash( document.getElementById('possibleAnswer0'), 2100, 500, startColor, endColor, finalColor );	
	
	//console.log("done");

	if( !updateTriggered )
		update();
}

function update()
{
	updateTriggered = true;

	var currentDate = new Date();
	var time = currentDate.getTime();
    
	//console.log("update");
    if( bodyLoadTime > 0 && time - bodyLoadTime > 5000 )
	{
		
		// End everything
		return;
	}

	for( var i = 0; i < numFlashes; i++ )
	{
		var flash = flashes[i];
		if( !flash.enabled )
		{
			continue;
		}

		var diff = time - flash.addTime;

		if( diff > flash.startTime )
		{
			flashCell( flash.element, diff - flash.startTime, flash.duration, flash.startColor, flash.endColor );
		}

		if( diff - flash.startTime > flash.duration )
		{
			var color = flash.finalColor.r+', '+flash.finalColor.g+', '+flash.finalColor.b; 
			flash.element.style.backgroundColor = 'rgb('+color+')'; 

			flash.enabled = false;
		}
	}


	lastUpdateMS = currentDate.getTime();

   	timeout = 50; // 20 fps

	window.setTimeout("update();", timeout);
}

var charitySelectEffectStartTime = 0;

function initFlashEffects()
{
	// In a regular web page this is set in body.onload but I don't think that exists for facebook???
	var currentDate = new Date();
	bodyLoadTime = currentDate.getTime();
}

function startCharitySelectEffect()
{
	return;

	var currentDate = new Date();
	charitySelectEffectStartTime = currentDate.getTime();

	updateCharitySelectEffect();
}

/*
function updateCharitySelectEffect()
{
	//console.log("CS2");

	var currentDate = new Date();
	var time = currentDate.getTime();
	var diff = time - charitySelectEffectStartTime;

	if( diff < 150 )
	{
		activateCharity(0);
	}
	else if( diff < 300 )
	{
		activateCharity(1);
	}
	else if( diff < 450 )
	{
		activateCharity(2);
	}
	else if( diff < 600 )
	{
		activateCharity(3);
	}
	else if( diff < 750 )
	{
		activateCharity(2);
	}
	else if( diff < 900 )
	{
		activateCharity(1);
	}
	else if( diff < 1150 )
	{
		activateCharity(0);
		startPossibleAnswerFlash();
	}
	else
	{
		return;
	}

	timeout = 50; // 20 fps

    window.setTimeout("updateCharitySelectEffect();", timeout);
}
*/

var answerIsTouched = [];
var answerIntensity = [];
var answerUpdateTriggered = false;
var answerFocusStartColor = new Color(128,255,128);
var answerFocusEndColor = new Color(255,255,255);
var answerFocusEnabled = true;
function answerMouseOver( id )
{
	//console.log("answerMouseOver");

	answerIsTouched[id] = true;

	if( !answerUpdateTriggered )
	{
		answerIntensity[0] = answerIntensity[1] = answerIntensity[2] = answerIntensity[3] = 0;
		answerIsTouched[0] = answerIsTouched[1] = answerIsTouched[2] = answerIsTouched[3] = false;
		answerIsTouched[id] = true;
		updateAnswerFocus();
	}

	//console.log("yyy");
}

function answerMouseOut( id )
{
	answerIsTouched[id] = false;
}

function disableAnswerFocus()
{
	answerFocusEnabled = false;
}

function enableAnswerfocus()
{
	answerFocusEnabled = true;
	answerUpdateTriggered = false;
}

function updateAnswerFocus()
{
	if( !answerFocusEnabled )
		return;

	answerUpdateTriggered = true;
    
	for( var i = 0; i < 4; i++ )
	{

		if( answerIsTouched[i] )
		{
			answerIntensity[i] += 0.2;
		}
		else
		{
			answerIntensity[i] -= 0.4;
		}
		if( answerIntensity[i] > 1.0 )
			answerIntensity[i] = 1.0;
		else if( answerIntensity[i] < 0.0 )
			answerIntensity[i] = 0.0;

		var element = document.getElementById('possibleAnswer' + i );
		flashCell( element, 500 - answerIntensity[i]*500, 500, answerFocusStartColor, answerFocusEndColor );

		if( i == 0 )
		{
			 //console.log(500 - answerIntensity[i]*500);
		}
	}
	
	timeout = 50; // 20 fps

	// TB FB
	window.setTimeout( function() { updateAnswerFocus(); }, timeout);
}

function resetFlashes()
{
	lastUpdateMS = 0;
	numFlashes = 0;
	flashes = [];
	updateTriggered = false;
	
}
