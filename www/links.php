<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require 'minify_page_start.php';
require('common.php');
generic_page_start('links');
?>

<h1>Charitii Resources</h1>
<br><br>

<h2>Charity Sites</h2>

<p>
<a href="http://www.superdonate.org">SuperDonate</a> - Automatically donate to charty just by leaving your computer on
</p>

<p>
<a href="http://www.charitywater.org">Charity: water</a>
</p>

<p>
<a href="http://www.invisibleyouthnetwork.net">Invisible Youth Network</a>
</p>

<p>
<a href="http://www.nature.org">The Nature Conservancy</a>
</p>

<p>
<a href="http://theoaktree.org">The Oaktree Foundation</a>
</p>

<p>
<a href="http://www.philippineaid.org/">Philippine Aid Society</a>
</p>


<br><br>
<h2>Puzzle Sites</h2>

<p>
<a href="http://www.crosschoice.com">CrossChoice</a></li>
</p>

<p>
<a href="http://www.puzzles.ca">Livewire Puzzles</a>
</p>

<p>
<a href="http://www.3smartcubes.com">IQ Test</a>
<br/>
Take our PhD certified IQ Tests. We also have loads of PhD certified Aptitude & other Self-Assessment Tests.
</p>

<br/>
<br/>

<?php 
generic_page_end();
$minify_file_name = __FILE__;
require 'minify_page_end.php';
