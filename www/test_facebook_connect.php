<?php
// Copyright 2009, SuperDonate.  All rights reserved.
//require 'minify_page_start.php';
require('common.php');
require('game_database.php');
$game = new Game();

?>

<?php
//$common->minify_page_start();
echo_html_head_meta('');
?>


<body id="index">
<div id="container">
<div id="content">

<?php include('header.php'); ?>


<div id="wrapperContentBox">

<?php dialog_start(); ?>

<?php
function output_charity_select_class($id)
{
	if( $id == $_SESSION['charity_select'] )
	{
		echo 'charitySelect' . $id . 'On';
	}
	else
	{
		echo 'charitySelect' . $id . 'Off';
	}
}
?>

<?php if( !isset($_GET['cs']) && !Common::c_display_charity_select ) echo '<!--'; ?>

<div id="charitySelect">

<?php
$is_first_question = !isset($_POST['token']);
if( $is_first_question )
	echo '<div id="charitySelectIntro">Select what each correct word will donate</div>';
?>

<div id="charitySelect0" class="<?php output_charity_select_class(0); ?>"><a href="javascript:activateCharity(0);"></a></div>
<div id="charitySelect1" class="<?php output_charity_select_class(1); ?>"><a href="javascript:activateCharity(1);"></a></div>
<div id="charitySelect2" class="<?php output_charity_select_class(2); ?>"><a href="javascript:activateCharity(2);"></a></div>
<div id="charitySelect3" class="<?php output_charity_select_class(3); ?>"><a href="javascript:activateCharity(3);"></a></div>
<div class="clear"></div>

</div>

<?php if( !isset($_GET['cs']) && !Common::c_display_charity_select ) echo '-->'; ?>






<?php

// This initial page is not the same as what you see when you've started playing
// The token will always be posted if you select a word or click on hit reload
// but if you are coming from elsewhere or you restart the page the game should restart
// TB TODO - This is a sucky way of knowing if you're in a game!!!
//error_log("---\r\n", 3, "c:/program files/php/log.log");
//error_log('ID=PHPSESSID=' . session_id() . "\r\n", 3, "c:/program files/php/log.log");
if( !isset($_POST['token']) || !isset($_SESSION['num_correct']) )
{
	// This invisible img tag ensures that the image is done loading
	// before the animation starts...
	// I moved this to the game_first condition so it always displays from the front page!!! (and I got rid of the charity_select condition - people will have to reselect their charity each time)
	//if( !isset($_SESSION['charity_select']) )
		echo '<img src="test/charity_select.png" style="display:none" onload="startCharitySelectEffect();" />';


    $game->start_new();
	$game->generate_valid_question();
	require('game_box.php');
	require('game_first.php');
}
else
{
	$game->generate_valid_question();
	require('game_box.php');
}

?>




<?php dialog_end(); ?>


</div><!--wrapperContentBox-->


<?php include('footer.php'); ?>



<?php 
if( !$is_first_question ) {
	echo_ultra_left_ad();
	echo_peel_away_ad();
}
?>

<?php
require('facebook_connect.php');
?>


<?php echo_final_script_code(); ?>

</body>
</html>
<?php
$minify_page_is_dynamic = TRUE;
$minify_file_name = __FILE__;
//require 'minify_page_end.php';
?>
