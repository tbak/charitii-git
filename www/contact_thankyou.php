<?php 
// Copyright 2009, SuperDonate.  All rights reserved.
require ('minify_page_start.php');
require('common.php');
generic_page_start('contact_thankyou');
?>

<h1>Contact</h1>

<h2>Thank you for your comment!</h2>

<p>
<a href="index.php">Click to return to the game</a>
</p>

<?php 
generic_page_end();
$minify_file_name = __FILE__;
require 'minify_page_end.php';
