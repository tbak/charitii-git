<html>

<head>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="test_dialog.css" type="text/css" media="all" charset="utf-8" />
	<!--[if lt IE 7]><link rel="stylesheet" href="wwie6.css" type="text/css" charset="utf-8" /><![endif]-->
	<script language="JavaScript" type="text/javascript" src="main.js"></script>  
	<meta name="description" content="Donate clean drinking water to those in need by playing a fun word game." />
	<meta name="keywords" content="water, words, donate, clean, drinking, people, world, charity, charitable, donation, international, poverty, crossword, word, game, teach, teachers, teaching, learn, well, africa, study, students, education, school" />
	<meta name="robots" content="all" />
	<link rel="shortcut icon" href="favicon.ico">
</head>

<body>


<div class="dialog">
 <div class="content">
  <div class="t"></div>
  <!-- Your content goes here -->
  <h1>Even <em>More</em> Rounded Corners With CSS</h1>
  <p>Here is a very simple example dialog.</p>
  <p>Note that if gradients are used, you will need a "min-height" (or fixed height) rule on the body of the dialog. If these examples appear funny at the bottom, it is because they do not enforce the min-height rule.</p>

 </div>
 <div class="b"><div></div></div>
</div>


</body>
</html>
