<?php
require 'minify_page_start.php';
require 'common.php';
generic_page_start('invite');
?>

<div id="invitePage">

<?php
include('../openinviter/openinviter.php');
$inviter=new OpenInviter();
$oi_services=$inviter->getPlugins();
if (isset($_POST['provider_box'])) 
{
    if (isset($oi_services['email'][$_POST['provider_box']])) $plugType='email';
    elseif (isset($oi_services['social'][$_POST['provider_box']])) $plugType='social';
    else $plugType='';
}
else $plugType = '';
function ers($ers)
{
    if (!empty($ers))
    {
        $contents="<table cellspacing='0' cellpadding='0' style='border:1px solid red;' align='center' class='tbErrorMsgGrad'><tr><td valign='middle' style='padding:3px' valign='middle' class='tbErrorMsg'><img src='test/openinviter_ers.gif'></td><td valign='middle' style='color:red;padding:5px;'>";
        foreach ($ers as $key=>$error)
            $contents.="{$error}<br/>";
        $contents.="</td></tr></table>";
        return $contents;
    }
}
    
function oks($oks)
{
	if (!empty($oks))
    {
        $contents="<table border='0' cellspacing='0' cellpadding='10' style='border:1px solid #5897FE;' align='center' class='tbInfoMsgGrad'><tr><td valign='middle' valign='middle' class='tbInfoMsg'><img src='test/openinviter_oks.gif' ></td><td valign='middle' style='color:#5897FE;padding:5px;'> ";
        foreach ($oks as $key=>$msg)
            $contents.="{$msg}<br >";
        $contents.="</td></tr></table><br >";
        return $contents;
    }
}

if (!empty($_POST['step'])) $step=$_POST['step'];
else $step='get_contacts';

$ers=array();$oks=array();$import_ok=false;$done=false;
if ($_SERVER['REQUEST_METHOD']=='POST')
{
    if ($step=='get_contacts')
    {
        if (empty($_POST['email_box']))
            $ers['email']="Email missing";
        if (empty($_POST['password_box']))
            $ers['password']="Password missing";
        if (empty($_POST['provider_box']))
            $ers['provider']="Provider missing";
        if (count($ers)==0)
        {
            $inviter->startPlugin($_POST['provider_box']);
            $internal=$inviter->getInternalError();
            if ($internal)
                $ers['inviter']=$internal;
            elseif (!$inviter->login($_POST['email_box'],$_POST['password_box']))
            {
                $internal=$inviter->getInternalError();
                $ers['login']=($internal?$internal:"Login failed. Please check the email and password you have provided and try again");
            }
            elseif (false===$contacts=$inviter->getMyContacts())
                $ers['contacts']="Unable to get contacts.";
            else
            {
                $import_ok=true;
                $step='send_invites';
                $_POST['oi_session_id']=$inviter->plugin->getSessionID();
                $_POST['message_box']='';
            }
        }
    }
	elseif ($step=='send_invites')
    {
		if (empty($_POST['provider_box'])) $ers['provider']='Provider missing';
        else
        {
            $inviter->startPlugin($_POST['provider_box']);
            $internal=$inviter->getInternalError();
            if ($internal) $ers['internal']=$internal;
            else
            {
                if (empty($_POST['email_box'])) $ers['inviter']='Inviter information missing';
                if (empty($_POST['oi_session_id'])) $ers['session_id']='No active session';
                if (empty($_POST['message_box'])) $ers['message_body']='Message missing';
                else $_POST['message_box']=strip_tags($_POST['message_box']);
                $selected_contacts=array();$contacts=array();
                $message=array('subject'=>$inviter->settings['message_subject'],'body'=>$inviter->settings['message_body'],'attachment'=>"\n\r".$_POST['message_box']);
                if ($inviter->showContacts())
                {
                    foreach ($_POST as $key=>$val)
                        if (strpos($key,'check_')!==false)
                            $selected_contacts[$_POST['email_'.$val]]=$_POST['name_'.$val];
                        elseif (strpos($key,'email_')!==false)
                        {
                            $temp=explode('_',$key);$counter=$temp[1];
                            if (is_numeric($temp[1])) $contacts[$val]=$_POST['name_'.$temp[1]];
                        }
                    if (count($selected_contacts)==0) $ers['contacts']="You haven't selected any contacts to invite";
                }
            }
        }
        if (count($ers)==0)
        {
            $sendMessage=$inviter->sendMessage($_POST['oi_session_id'],$message,$selected_contacts);
            $inviter->logout();
            if ($sendMessage===-1)
            {
                //$message_footer="\r\n\r\nThis invite was sent using OpenInviter technology.";
				$message_footer="\r\n";
                $message_subject=$_POST['email_box'].$message['subject'];
                $message_body=$message['body'].$message['attachment'].$message_footer; 
                $headers="From: {$_POST['email_box']}";
                foreach ($selected_contacts as $email=>$name)
                    mail($email,$message_subject,$message_body,$headers);
                $oks['mails']="Mails sent successfully";
            }
            elseif ($sendMessage===false)
            {
                $internal=$inviter->getInternalError();
                $ers['internal']=($internal?$internal:"There were errors while sending your invites.<br/>Please try again later!");
            }
            else $oks['internal']="Invites sent successfully!";
            $done=true;
        }
    }
}
else
{
    $_POST['email_box']='';
    $_POST['password_box']='';
    $_POST['provider_box']='';
}

$contents="<script type='text/javascript'>
    function toggleAll(element) 
    {
        var form = document.forms.openinviter, z = 0;
        for(z=0; z<form.length;z++)
        {
            if(form[z].type == 'checkbox')
                form[z].checked = element.checked;
        }
    }
</script>";

// Fill in the select field for either EMAIL or SOCIAL providers
// I'd like to have them separated so people don't get confused!
function createInputChunkForType($contents, $inviter, $type, $providers, $fill_values)
{
$contents.="<table align='center' class='thTable' cellspacing='0' cellpadding='0' style='border:none;'>
            <tr class='thTableRow'><td align='right'><label for='email_box'>Email Address</label></td><td><input class='thTextbox' type='text' name='email_box'" . ($fill_values ? " value='{$_POST['email_box']}'" : "") . "/></td></tr>
            <tr class='thTableRow'><td align='right'><label for='password_box'>Password</label></td><td><input class='thTextbox' type='password' name='password_box'" . ($fill_values ? " value='{$_POST['password_box']}'" : "") . "/></td></tr>
            <tr class='thTableRow'><td align='right'><label for='provider_box'>Provider</label></td><td><select class='thSelect' name='provider_box'><option value=''></option>";
        //foreach ($oi_services as $type=>$providers) 
        //{
            $contents.="<option disabled='disabled'>".$inviter->pluginTypes[$type]."</option>";
            foreach ($providers as $provider=>$details)
                $contents.="<option value='{$provider}'".($_POST['provider_box']==$provider?' selected':'').">{$details['name']}</option>";
        //}
        $contents.="</select></td></tr>
			<tr class='thTableImportantRow'><td></td><td colspan='1' align='center'><input class='thButton' type='submit' name='import' value='Import Contacts' /></td></tr>
        </table>";

		//<tr class='thTableImportantRow'><td colspan='2' align='center'><input class='thButton' type='submit' name='import' value='Import Contacts' /></td></tr>

		return $contents;
}

// This is moved to the specific steps so that it appears in the correct location
//$contents.=ers($ers).oks($oks);

if (!$done)
{
    if ($step=='get_contacts')
    {
		$contents.='<div id="miniFAQ">
			It\'s easy to tell your friends about Charitii! Help spread the word about the free charity word game.<br/><br/>
			Simply fill out the login information for your email or social network account and click "import contacts". In the next page you can choose which people you wish to send a message to, and you can edit the message before sending it.<br/><br/>
			We promise to only use your login information to send your message to the people you select from your contact list. We will not do anything else with this information. We will not store your login information nor your contact list anywhere.
			</div>
			<br/>
			<hr/>
			<br/>';

		$contents.='<div id="contactLogins">';
		$contents.='<div class="singleLogin">';
		$contents.="Email<br/>";
		$contents.='<div class="examples">(GMail, Yahoo!, Live/Hotmail, AOL, etc)</div><br/>';
		$contents.="<form action='' method='post' name='openinviter'>";
		$fill_values = FALSE;
		if( !isset($_POST['attemptedLoginType']) || $_POST['attemptedLoginType'] == "email" )
		{
			$contents.=ers($ers).oks($oks);
			$fill_values = TRUE;
		}
		$contents = createInputChunkFortype($contents, $inviter, "email", $oi_services["email"], $fill_values);
		$contents.="<input type='hidden' name='step' value='get_contacts' />";
		$contents.="<input type='hidden' name='attemptedLoginType' value='email' />";
		$contents.="</form>";
		$contents.="</div>";

		$contents.='<div class="singleLogin">';
		$contents.="Social Network<br/>";
		$contents.='<div class="examples">(Facebook, Twitter, MySpace, Flickr, etc)</div><br/>';
		$contents.="<form action='' method='post' name='openinviter2'>";
		$fill_values = FALSE;
		if( isset($_POST['attemptedLoginType']) && $_POST['attemptedLoginType'] == "social" )
		{
			$contents.=ers($ers).oks($oks);
			$fill_values = TRUE;
		}
		$contents = createInputChunkFortype($contents, $inviter, "social", $oi_services["social"], $fill_values);
		$contents.="<input type='hidden' name='step' value='get_contacts' />";
		$contents.="<input type='hidden' name='attemptedLoginType' value='social' />";
        $contents.="</form>";
		$contents.="</div>";

		$contents.='<div class="clear"></div>';

		// contactLogins
		$contents.="</div>";
    }
    else if( $step=='send_invites')
	{
		$contents.='<div id="miniFAQ">
			Select which friends you want to send a message to, edit the message if you\'d like, and click "Send Invites".<br/>
			</div>
			<br/>';

		$colspan = $plugType=='email'? "3":"2";
		if( !isset($_POST['message_box']) || $_POST['message_box'] == "" )
		{
			$_POST['message_box'] = 
				"Hi,\n\nI thought you might want to try using Charitii. You can donate to the charity of your choice for free simply by playing a fun word game. \n\nCheck it out at www.charitii.com\n\nThanks!";
		}

		$contents.='<div id="messagePanes">';
		$contents.='<div class="singlePane">';

		$contents.="<form action='' method='post' name='openinviter'>".ers($ers).oks($oks);
        $contents.="<table class='thTable' align='center' cellspacing='0' cellpadding='0' style='border:none;'>
				<tr class='thTableHeader'><td colspan='".$colspan."'>Message</td></tr>
                <tr class='thTableRow'><td colspan='".$colspan."'><textarea rows='15' cols='9' name='message_box' class='thTextArea' style='width:300px;'>{$_POST['message_box']}</textarea></td></tr>
                <tr class='thTableFooter'><td align='center' colspan='".$colspan."'><input type='submit' name='send' value='Send Invites' class='thButton' /></td></tr>
            </table>";

		$contents.="</div>";

		// <tr class='thTableRow'><td align='right' valign='top'><label for='message_box'>Message</label></td><td><textarea rows='5' cols='50' name='message_box' class='thTextArea' style='width:300px;'>{$_POST['message_box']}</textarea></td></tr>
    
        if ($inviter->showContacts())
        {
			$contents.='<div class="singlePane">';
            $contents.="<table class='thTable' align='center' cellspacing='0' cellpadding='0'><tr class='thTableHeader'><td colspan='".$colspan."'>Your contacts</td></tr>";
            if (count($contacts)==0)
                $contents.="<tr class='thTableOddRow'><td align='center' style='padding:20px;' colspan='".$colspan."'>You do not have any contacts in your address book.</td></tr>";
            else
            {
                $contents.="<tr class='thTableDesc'><td><input type='checkbox' onChange='toggleAll(this)' name='toggle_all' title='Select/Deselect all' checked />Invite?</td><td>Name</td>".($plugType == 'email' ?"<td>E-mail</td>":"")."</tr>";
                $odd=true;$counter=0;
                foreach ($contacts as $email=>$name)
                {
                    $counter++;
                    if ($odd) $class='thTableOddRow'; else $class='thTableEvenRow';
                    $contents.="<tr class='{$class}'><td><input name='check_{$counter}' value='{$counter}' type='checkbox' class='thCheckbox' checked /><input type='hidden' name='email_{$counter}' value='{$email}'><input type='hidden' name='name_{$counter}' value='{$name}' /></td><td>{$name}</td>".($plugType == 'email' ?"<td>{$email}</td>":"")."</tr>";
                    $odd=!$odd;
                }
                //$contents.="<tr class='thTableFooter'><td colspan='".$colspan."' style='padding:3px;'><input type='submit' name='send' value='Send invites' class='thButton' /></td></tr>";
            }
            $contents.="</table>";
			$contents.="</div>";
        }
        $contents.="<input type='hidden' name='step' value='send_invites' />
            <input type='hidden' name='provider_box' value='{$_POST['provider_box']}' />
            <input type='hidden' name='email_box' value='{$_POST['email_box']}' />
            <input type='hidden' name='oi_session_id' value='{$_POST['oi_session_id']}' />";

		$contents.="</form>";

		// messagePanes
		$contents.="</div>";
    }
	else
	{
		$contents.="<br/>INTERNAL ERROR:Unknown step<br/>";
	}
}
else
{
	// Done!
	$contents.=ers($ers).oks($oks);
	$contents.='<br/>';
	$contents.='<center>';
	$contents.='<a href="invite.php">Send more invites</a><br/>';
	$contents.='<a href="index.php">Return to SuperDonate</a>';
	$contents.='</center>';
}

echo $contents;
?>

<!--<div class="clear"></div>-->

<br/>
<br/>



</div>



<?php
generic_page_end();
$minify_page_is_dynamic = TRUE;
$minify_file_name = __FILE__;
require 'minify_page_end.php';

