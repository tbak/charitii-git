<?php
require('common.php');
require('db.php');
connect_database();

function isBadWord($n)
{
    $badwords = array('fuck', 'shit', 'cunt', 'asshole', 'dickhead', 'pussy', 'cock');
    
	foreach( $badwords as $word ) {
		if( stristr( $n, $word ) ) {			
			return TRUE;
		}
	}
	return FALSE;

}


function checkField($variable_name, $err_index, $human_text)
{
	global $error;
	
	if( strlen($variable_name) < 5 || strlen($variable_name) > 10 ) {
		$error[$err_index] = $human_text . " must be between 5 and 10 characters";
		return FALSE;
	}
	
	return TRUE;
}

function checkCaptcha($value)
{
	global $error;
	
	if( !isset($_SESSION['security_code']) ) {
		$error['login_captcha'] = 'Please enter these numbers.';
		
		return FALSE;
	}
	else if( $value != $_SESSION['security_code'] ) {
		$error['login_captcha'] = 'Please try again.';
		
		// TB - Reset the security code session variable so that you can only post once
		unset($_SESSION['security_code']);
		
		return FALSE;
	}
	else {
		return TRUE;
	}
	

}

function isUserNameTaken($name) 
{
	// User name exists in the user database
	$query = "select TRUE from users where username = '" . $name . "';";
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	if( mysql_num_rows($result) == 0 ) {
		return FALSE;
	}
	
	return TRUE;	
}


$success = TRUE;
$error['login_username'] = FALSE;
$error['login_password'] = FALSE;
$error['login_email'] = FALSE;
$error['login_captcha'] = FALSE;

if( !isset($_POST['token']) ) {
	$login_username = '';	
	$login_email = '';
}
else {
	
	$login_username = isset($_POST['login_username']) ? mysql_real_escape_string($_POST['login_username']) : '';
	$login_password = isset($_POST['login_password']) ? mysql_real_escape_string($_POST['login_password']) : '';
	$login_password2 = isset($_POST['login_password2']) ? mysql_real_escape_string($_POST['login_password2']) : '';
	$login_email = isset($_POST['login_email']) ? mysql_real_escape_string($_POST['login_email']) : '';
	$login_captcha = isset($_POST['login_captcha']) ? $_POST['login_captcha'] : '';
		
	if( isBadWord($login_username) ) {
		$error['login_username'] = "Please enter a valid user name";
		$success = false;	
	}
	
	$success = checkField($login_username, 'login_username', 'user name') & checkField($login_password, 'login_password', 'password');
	
	// Check if the user name is already in the database		
	if( !$error['login_username'] && isUserNameTaken($login_username) ) {
		$error['login_username'] = "Username is already taken";
		$success = false;
	}
	
	if( strlen($login_email) > 30 ) {
		$error['login_email'] = "Email address is too long";
		$success = false;
	}
	
	// TB TODO - Check that passwords match
	if( !$error['login_password'] && strcmp($login_password,$login_password2) != 0 ) {
		$error['login_password'] = "The two passwords did not match.";
		$success = false;
	}
	
	// TB TODO - Check that captcha is correct
	if( $success ) {
		$success = checkCaptcha($login_captcha);
	}	
	
	if( $success )
	{
		// At this point, create a new user!
		
		
		if( isset($_SESSION['user_id']) ) {
			// We have an anonymous user accuont already, so update that.
			$query = sprintf("update users set username='%s', password_md5='%s', email='%s' where id='%d'",
								$login_username, md5($login_password), $login_email, $_SESSION['user_id'] );			
			$result = mysql_query($query) or die('Query failed: ' . mysql_error());				
		}
		else {
			// Otherwise insert a new user
			$query = sprintf("insert into users (username, password_md5, email, create_date) values
								('%s', '%s', '%s', CURRENT_DATE() )",
								$login_username, md5($login_password), $login_email );			
			$result = mysql_query($query) or die('Query failed: ' . mysql_error());

			$_SESSION['user_id'] = mysql_insert_id();
		}

		user_login( $_SESSION['user_id'] );
		
		// Jump to the game page now that the dude is created.
		if( isset($_POST['isajax']) ) {
			echo '<br/>Logging in...';
			echo '<script type="text/JavaScript">location.href = "index.php";</script>';				
		} 
		else {
			header('Location:index.php');	
		}
			
		exit();
	}
	
}

?>

<html>
<head>

</head>

<body>

<!-- Chrome ignores <style> tag inside <head> if loaded with AJAX... so it needs to be here... -->
<style>
	body {
		font-family:"lucida grande",tahoma,verdana,arial,sans-serif;
		font-size: 11px;	
	}
	div#panelHolder {
		padding: 20px 0 0 10px;
	}
	div.formField {
		margin-top: 10px;
	}
	span.error {
		color: #ff0000; 
		margin-left: 10px;
	}
	h1 {
		margin: 0;
		padding: 0;
	}
	label {
		display: block;
	}
	span#ajaxLoad {
	    margin-left: 20px;
	    z-index: 100;
	    display: none;
	}
</style>


<div id="panelHolder">

<h1>CREATE A NEW CHARITII ACCOUNT</h1>

<p>Enter a little information and you're done!</p>

<form onsubmit="return submit_check();" id="form_register" name="form_register" action="register2.php" method="post">

<div class="formField">
<label for="login_username">username:</label>
<input type="text" size="15" maxlength="10" id="login_username" name="login_username" value="<?php echo $login_username ?>"/>
<span class="error"><?php if($error['login_username']) echo $error['login_username']; ?></span>
</div>

<div class="formField">
<label for="login_email">email (optional):</label>
<input type="text" size="15" maxlength="30" id="login_email" name="login_email" value="<?php echo $login_email ?>"/>
<span class="error"><?php if($error['login_email']) echo $error['login_email']; ?></span>
</div>

<div class="formField">
<label for="login_password">password:</label>
<input type="password" size="15" maxlength="10" id="login_password" name="login_password" />
<span class="error"><?php if($error['login_password']) echo $error['login_password']; ?></span>
</div>

<div class="formField">
<label for="login_password2">verify password:</label>
<input type="password" size="15" maxlength="10" id="login_password2" name="login_password2" />
</div>

<div class="formField">
<?php 
// I am appending foo=uniqid() to the image reference so that it always reloads (unique URL). 
// If this page is loaded with AJAX, the security image stays the same...
?>
<img src="captchasecurityimage.php?foo=<?php echo uniqid();?>" />
<label for="login_captcha">Enter these numbers (spam detection):</label>
<input type="text" size="15" maxlength="10" id="login_captcha" name="login_captcha" />
<span class="error"><?php if($error['login_captcha']) echo $error['login_captcha']; ?></span>
</div>

<br/>


<input type=hidden name="token" value="4" />

<input class="button" type="submit" value="create account" />
<span id="ajaxLoad"><img src="test/ajax_load.gif" /></span>
</form>

<script type="text/JavaScript">
function submit_check() {

	$("#ajaxLoad").show();
	
	$("#TB_ajaxContent").load("register2.php", 
		{	
			isajax:1,
			token:4,
			login_username:document.getElementById('login_username').value,
			login_password:document.getElementById('login_password').value,
			login_password2:document.getElementById('login_password2').value,
			login_email:document.getElementById('login_email').value,
			login_captcha:document.getElementById('login_captcha').value 
		}, 
		function() {				 
		} 
	);

	return false;  	
		
}
</script>

</div>

</body>
</html>
