<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require 'minify_page_start.php';
require 'common.php';
generic_page_start('faq');
?>

<script type="text/javascript" language="JavaScript">
function expandFaq(param)
{
	hideAll();
	var elem = document.getElementById('faq' + param);
	elem.style.display = "";

	/*
	// Expand the single item, and collapse the otthers...
	// This way there will only ever be one expanded item...
	for( var x = 0; x <= 19; x++ )
	{
		var elem = document.getElementById('faq' + x);

		if( !elem ) continue;

		if( x == param )
		{
			elem.style.display = "";
		}
		else
		{
			elem.style.display = "none";
		}

	}  
	*/

}

function hideAll()
{
	for( var x = 0; x <= 19; x++ )
	{
		var elem = document.getElementById('faq' + x);

		if( !elem ) continue;

		elem.style.display = "none";
	}  
}
</script>

<h1>Frequently Asked Questions</h1>

<div><!--Generic div to contain the left and right faq panels-->
<div id="faqLeft">

<h4><a href="javascript:expandFaq(1)">
What is Charitii.com?
</a></h4>

<h4><a href="javascript:expandFaq(2)">
How do I play Charitii.com? 
</a></h4>

<h4><a href="javascript:expandFaq(3)">
What are some tips for solving Charitii.com puzzles?
</a></h4>

<h4><a href="javascript:expandFaq(4)">
How does the Charitii.com website work?
</a></h4>

<h4><a href="javascript:expandFaq(15)">
What is the CharitiiChooser?
</a></h4>

<h4><a href="javascript:expandFaq(5)">
How is the difficulty level for each puzzle determined?
</a></h4>

<h4><a href="javascript:expandFaq(6)">
How many puzzles are there?
</a></h4>

<h4><a href="javascript:expandFaq(7)">
What is the Options page?
</a></h4>

<h4><a href="javascript:expandFaq(8)">
Are there any banners or logos I can use to link to Charitii.com?
</a></h4>

<h4><a href="javascript:expandFaq(9)">
How does Charitii.com keep track of my donations?
</a></h4>

<h4><a href="javascript:expandFaq(12)">
Where does the money for the donations come from?
</a></h4>

<h4><a href="javascript:expandFaq(13)">
What is charity: water?
</a></h4>

<h4><a href="javascript:expandFaq(14)">
Where has charity: water already donated water?
</a></h4>

<h4><a href="javascript:expandFaq(10)">
Why is water important?
</a></h4>

<h4><a href="javascript:expandFaq(19)">
Why is hunger important?
</a></h4>

<h4><a href="javascript:expandFaq(17)">
What is The Oaktree Foundation?
</a></h4>

<h4><a href="javascript:expandFaq(18)">
What is The Nature Conservancy?
</a></h4>

</div>
<?php //apply_round_borders(2); ?>

<div id="faqRight">

<div id="faq0">
<p>Click on a question to reveal the answer</p>
</div>

<?php require "faq_text.php"; ?>

<script type="text/javascript" language="JavaScript">
expandFaq(0);
</script>

</div> <!-- faqRight -->

</div><!--END Generic div to contain the left and right faq panels-->
<div class="clear"></div>

<br/><br/>

<?php
generic_page_end();
$minify_file_name = __FILE__;
require 'minify_page_end.php';

