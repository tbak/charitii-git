<?php
// Copyright 2009, SuperDonate.  All rights reserved.

$base = realpath(dirname(__FILE__) );
$groupsSources = array(
    'js' => array(
        "{$base}/main.js",
        "{$base}/lib/jquery-1.3.2.min.js",
		"{$base}/lib/thickbox.js"
    ),
	'css' => array( "{$base}/ww.css", "{$base}/lib/thickbox.css" )
);
unset($base);