<?php
require('../common.php');
require('../game_database.php');
$game = new Game();

// THE CODE TO CALL IN PHPMYSQL IS IN ADMIN/CHECK_DIFFICULTY.PHP
	/*
	This looks good: (FEB 4, 2009: THIS IS THE ONE TO USE)

	SET @factor = (SELECT count(*) from puzzles where enabled = 1);
	SET @factor = 60 / 10000;
	SET @count=0;
	update puzzles set difficulty_scaled = @factor * (SELECT @count:=@count + 1) ORDER BY difficulty_calculated;

	*/


function iterate_over_difficulties()
{

	// This would work but it's too slow...

	$query = "SELECT count(*) FROM puzzles WHERE enabled = 1";
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	$row = mysql_fetch_array($result);
	$num_puzzles = $row[0];
	echo 'Num puzzles: ' . $num_puzzles . '<br>';

	$factor = 60 / $num_puzzles;
	echo 'Factor = ' . $factor . '<br>';


	$query = "SELECT id FROM puzzles WHERE enabled = 1 order by difficulty_calculated asc";
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
    
	for( $i = 0; $i < mysql_num_rows($result); $i++ )
	{
		$row = mysql_fetch_array($result);

		$query2 = "update puzzles SET difficulty_scaled = " . $i * $factor . " where id = " . $row[0];
		$result2 = mysql_query($query2) or die('Query failed: ' . mysql_error());

		if( $i % 100 == 0 )
		{
			//echo $row['id'] . '<br>';
			echo $i . '<br>';
			ob_flush();
		}
		
	}
	
}

function scale_range( $range_min, $range_max, $scale_factor )
{
	$query2 = "update puzzles SET difficulty_scaled = difficulty_calculated * " . $scale_factor . ' where difficulty_calculated >= ' . $range_min . ' AND difficulty_calculated < ' . $range_max;
	$result2 = mysql_query($query2) or die('Query failed: ' . mysql_error());
}

function scale_difficulties()
{
	// Evenly distribute difficulties between 0 and 50!
	$query = "SELECT sum(difficulty_calculated) FROM puzzles WHERE enabled = 1";
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	$row = mysql_fetch_array($result);
	$difficulty_total = $row[0];
	echo 'Total difficulty: ' . $difficulty_total . '<br>';

	$query = "SELECT count(*) FROM puzzles WHERE enabled = 1";
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	$row = mysql_fetch_array($result);
	$num_puzzles = $row[0];
	echo 'Num puzzles: ' . $num_puzzles . '<br>';

	// 0 - 10 --> 20%
	// 6

	$avg_difficulty = $difficulty_total / $num_puzzles;
	echo 'Avg difficulty = ' . $avg_difficulty . '<br>';

	
	$scale_factor = 30 / $avg_difficulty;
	echo 'scale factor = ' . $scale_factor . '<br>';

	$query2 = "update puzzles SET difficulty_scaled = difficulty_calculated * " . $scale_factor;
	$result2 = mysql_query($query2) or die('Query failed: ' . mysql_error());
	

	/*
	scale_range( 0, 10, 1.0 );
	scale_range( 10, 20, 0.9 );
	scale_range( 20, 30, 0.8 );
	scale_range( 30, 40, 0.7 );
	scale_range( 40, 50, 0.6 );
	*/


	//$scaling_factor = $
}

function test_range( $min_val, $max_val )
{
	$query = "SELECT count(*) FROM puzzles WHERE enabled = 1 AND difficulty_calculated >= " . $min_val . " AND difficulty_calculated <= " . $max_val;
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	$row = mysql_fetch_array($result);
	echo 'Min=' . $min_val . '  Max=' . $max_val . '   Count = ' . $row[0];


	$query = "SELECT count(*) FROM puzzles WHERE enabled = 1 AND difficulty_scaled >= " . $min_val . " AND difficulty_scaled <= " . $max_val;
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	$row = mysql_fetch_array($result);
	echo '  ---- New count = ' . $row[0] . '<br>';


	$query = "SELECT count(*) FROM puzzles WHERE enabled = 1 AND difficulty_original >= " . $min_val . " AND difficulty_original <= " . $max_val;
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	$row = mysql_fetch_array($result);
	echo '  ---- Original = ' . $row[0] . '<br><br>';
}

//iterate_over_difficulties();

//scale_difficulties();

// These ranges need to sync up with the chunk ranges!!! Annoying!
//test_range(10,10);
test_range(0,10);
test_range(10,20);
test_range(20,30);
test_range(30,40);
test_range(40,50);
test_range(50,100);
