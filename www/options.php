<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require 'minify_page_start.php';
require('common.php');
require('game_database.php');
class Options
{
	function PrintOption( $option_name, $option_value, $jsSubmitFunc, $text, $default )
	{
		// Depending on whether the option is enabled or disabled, use a different div???
		//<li><noscript><input type="submit" name="option_num_missing_letters" value="0"> </noscript><a href="javascript: submitOptionNumMissingLetters('0')">No missing letters (easiest)</a></li>

		$is_selected = false;
		if( !isset($_COOKIE[$option_name]) )
		{
			if( $default == $option_value )
			{
				$is_selected = true;
			}
		}
		else
		{
			if( $_COOKIE[$option_name] == $option_value )
			{
				$is_selected = true;
			}
		}

		echo '<td class="' . ($is_selected ? 'enabledOption' : 'disabledOption') . '"><noscript><input type="submit" name="' . $option_name . '" value="' . $option_value . '"> </noscript><a href="javascript: ' . $jsSubmitFunc . "('" . $option_value . "')" . '">' . $text . '</a></td>';

	}

	function __construct() 
	{
		// echo $_COOKIE['difficulty'];

		if( isset($_POST['difficulty']) )
		{
			//echo "difficulty changed to: " . $_POST['difficulty'];
			$_SESSION['difficulty'] = $_POST['difficulty'];
            setcookie( 'difficulty', $_POST['difficulty'], time()+60*60*24*30 );
			$_COOKIE['difficulty'] = $_POST['difficulty'];
		}
		else
		{
			if( !isset($_COOKIE['difficulty']) )
			{
				setcookie( 'difficulty', -1, time()+60*60*24*30 );
				$_COOKIE['difficulty'] = -1;
			}
		}

		if( isset($_POST['num_missing_letters']) )
		{
			//echo "num missing letters changed to: " . $_POST['num_missing_letters'];
			$_SESSION['num_missing_letters'] = $_POST['num_missing_letters'];
			
			setcookie( 'num_missing_letters', $_POST['num_missing_letters'], time()+60*60*24*30 );
			$_COOKIE['num_missing_letters'] = $_POST['num_missing_letters'];
		}
		else
		{
			if( !isset($_COOKIE['num_missing_letters']) )
			{
				setcookie( 'num_missing_letters', -1, time()+60*60*24*30 );
				$_COOKIE['num_missing_letters'] = -1;
			}
		}

		if( isset($_POST['word_length']) )
		{
			//echo "word length changed to: " . $_POST['word_length'];

			setcookie( 'word_length', $_POST['word_length'], time()+60*60*24*30 );
			$_COOKIE['word_length'] = $_POST['word_length'];
			set_word_lengths_from_cookie_value( $_POST['word_length'] );
		}
		else
		{
			if( !isset($_COOKIE['word_length']) )
			{
				setcookie( 'word_length', 0, time()+60*60*24*30 );
				$_COOKIE['word_length'] = 0;
			}
		}
	}
}

$options = new Options();
generic_page_start('options');
?>
<h1>Options</h1>

<p>
Click on the buttons below to change the behavior of Charitii.com. Your settings will be saved in a cookie for the next time you play.
</p>

<div id="optionSelector">

<form id="formOptions" name="formOptions" action="options.php" method="POST" autocomplete="OFF" />  
<input type=hidden name="token_submit" value="<?php echo $options->token_submit ?>" />
<input type=hidden name="difficulty" value="<?php echo $_COOKIE['difficulty'] ?>" />
<input type=hidden name="num_missing_letters" value="<?php echo $_COOKIE['num_missing_letters'] ?>" />
<input type=hidden name="word_length" value="<?php echo $_COOKIE['word_length'] ?>" />


<div class="optionGroup">
<h2 class="foo">Starting Difficulty</h2>
	
<table><tr>
<?php
$options->PrintOption( 'difficulty', -1, 'submitOptionDifficulty', 'Automatic (default)', -1 );
$options->PrintOption( 'difficulty', 0, 'submitOptionDifficulty', '1 (easiest)', 0 );
$options->PrintOption( 'difficulty', 10, 'submitOptionDifficulty', '10', 0 );
$options->PrintOption( 'difficulty', 20, 'submitOptionDifficulty', '20', 0 );
$options->PrintOption( 'difficulty', 30, 'submitOptionDifficulty', '30', 0 );
$options->PrintOption( 'difficulty', 40, 'submitOptionDifficulty', '40', 0 );
$options->PrintOption( 'difficulty', 50, 'submitOptionDifficulty', '50 (hardest)', 0 );
?>
</tr></table>
</div>

<div class="optionGroup">
<h2 class="foo">Number of missing letters</h2>

<table><tr>
<?php
$options->PrintOption( 'num_missing_letters', -1, 'submitOptionNumMissingLetters', 'Automatic (default)', -1 );
$options->PrintOption( 'num_missing_letters', 0, 'submitOptionNumMissingLetters', '0 (easiest)', -1 );
$options->PrintOption( 'num_missing_letters', 1, 'submitOptionNumMissingLetters', '1', -1 );
$options->PrintOption( 'num_missing_letters', 2, 'submitOptionNumMissingLetters', '2', -1 );
$options->PrintOption( 'num_missing_letters', 3, 'submitOptionNumMissingLetters', '3 (hardest)', -1 );
?>
</tr></table>
</div>

<div class="optionGroup">
<h2 class="foo">Word length</h2>

<table><tr>
<?php
$options->PrintOption( 'word_length', 0, 'submitOptionWordLength', 'Any word length (default)', 0 );
$options->PrintOption( 'word_length', 1, 'submitOptionWordLength', 'Short words only', 0 );
$options->PrintOption( 'word_length', 2, 'submitOptionWordLength', 'Long words only', 0 );
?>
</tr></table>
</div>

<p>
<a class="backToGame" href="index.php">Start game with these options!</a>
</p>

</form>


</div>


<?php
generic_page_end();
$minify_page_is_dynamic = TRUE;
$minify_file_name = __FILE__;
require 'minify_page_end.php';
