<?php
require('common.php');
require('db.php');
connect_database();

$success = TRUE;
$error['login_username'] = FALSE;
$error['login_password'] = FALSE;

if( !isset($_POST['token']) ) {
	$login_username = '';	
	$login_email = '';
}
else {
	
	$login_username = isset($_POST['login_username']) ? mysql_real_escape_string($_POST['login_username']) : '';
	$login_password = isset($_POST['login_password']) ? mysql_real_escape_string($_POST['login_password']) : '';
	
	if( strlen($login_username) == 0 ) {
		$error['login_username'] = "Enter a username";
		$success = false;
	}
	
	if( strlen($login_password) == 0 ) {
		$error['login_password'] = "Enter a password";
		$success = false;
	}
	
	if( $success ) {
		// Does this user/password combo exist?
		$query = "select id from users where username='" . $login_username . "' and password_md5='" . md5($login_password) . "';";
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());
		if( mysql_num_rows($result) == 0 ) {
			$error['login_username'] = "Username and password do not match";
			$success = false;
		}
		else {
			// Fetch the ID associated with this user and log the dude in!
			$row = mysql_fetch_array($result);
			user_login( $row[0] );
			
			// Jump to the game page now that the dude is logged in.
			if( isset($_POST['isajax']) ) {
				echo '<br/>Logging in...';
				echo '<script type="text/JavaScript">location.href = "index.php";</script>';				
			} 
			else {
				header('Location:index.php');	
			}
			
			exit();
			
		}		
	}
}
?>

<html>
<head>

</head>

<body>

<!-- Chrome ignores <style> tag inside <head> if loaded with AJAX... so it needs to be here... -->
<style>
	body {
		font-family:"lucida grande",tahoma,verdana,arial,sans-serif;
		font-size: 11px;	
	}
	div#panelHolder {
		padding: 20px 0 0 10px;
	}
	div.formField {
		margin-top: 10px;
	}
	span.error {
		color: #ff0000; 
		margin-left: 10px;
	}
	h1 {
		margin: 0;
		padding: 0;
	}
	label {
		display: block;
	}
	span#ajaxLoad {
	    margin-left: 20px;
	    z-index: 100;
	    display: none;
	}
</style>

<div id="panelHolder">

<h1>SIGN IN TO CHARITII</h1>

<form onsubmit="return submit_check();" id="form_login" name="form_login" action="login2.php" method="post">

<div class="formField">
<label for="login_username">username:</label>
<input type="text" size="15" maxlength="10" id="login_username" name="login_username" value="<?php echo $login_username ?>"/>
<span class="error"><?php if($error['login_username']) echo $error['login_username']; ?></span>
</div>

<div class="formField">
<label for="login_password">password:</label>
<input type="password" size="15" maxlength="10" id="login_password" name="login_password" />
<span class="error"><?php if($error['login_password']) echo $error['login_password']; ?></span>
</div>

<br/>

<input type="hidden" name="token" value="4" />

<input class="button" type="submit" value="login" />
<span id="ajaxLoad"><img src="test/ajax_load.gif" /></span>
</form>

<script type="text/JavaScript">
function submit_check() {

	$("#ajaxLoad").show();
	
	$("#TB_ajaxContent").load("login2.php", {isajax:1,token:4,login_username:document.getElementById('login_username').value, login_password:document.getElementById('login_password').value }, 
		function() {				 
		} 
	);

	return false;  	
		
}
function ajax_load_register() {

	// Open inside the thickbox instead!
	
    var newURL = "register2.php?height=400&width=500";
    tb_show("", newURL);

    return false; 
		
}
</script>



<br/><br/>

Don't have a Charitii account yet? <a href="register2.php" onclick="return ajax_load_register();">Register</a> in seconds.

</div>

</body>
</html>
