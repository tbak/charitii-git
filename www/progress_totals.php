<?php
// Copyright 2009, SuperDonate.  All rights reserved.
class Totals
{
	const c_num_charities = 5;

	// Given this charity ID, what order is it in the table?
	// We need this information so that we know how many columns to skip
	// if a particular charity does not have any entry for that date...
	// We are getting charities by alphabetical order, but this order is not the order of their IDs...
	private $charity_order;
	
	function echo_for_all_months()
	{

		echo '<table class="prettyTable">';
						
		echo '<tr><th scope="col"><b>Date</b></th><th scope="col"><b>Charity: water</b></th><th scope="col"><b>Invisible Youth Network</b></th><th scope="col"><b>Nature Conservancy</b></th><th scope="col">Oaktree Foundation</b></th><th scope="col"><b>Philippine Aid Society</b></th></tr>';
		
		
		// Select questions transactions joined with completed offers transactions!		
		//$query = "select date_format(ask_date, '%M, %Y'), sum(num_correct), charity_select, ask_date from transactions_daily where 1 GROUP BY 1, charity_select ORDER BY ask_date asc, charity_select asc;";
		//$result = mysql_query($query) or die('Query failed: ' . mysql_error());
		$query = "select date_format(ask_date, '%M, %Y'), sum(num_correct), transactions_daily.charity_select, ask_date, sum(offers_daily.amount) from (charities LEFT JOIN transactions_daily ON transactions_daily.charity_select = charities.id) LEFT JOIN offers_daily ON transactions_daily.ask_date = offers_daily.date AND transactions_daily.charity_select = offers_daily.charity_select where charities.enabled = TRUE GROUP BY 1, charity_select ORDER BY DATE_FORMAT(ask_date, '%Y%m') asc, charities.name asc;";
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());

		for( $i = 0; $i < self::c_num_charities; $i++ ) {
			$grand_total[$i] = 0;
		}

		$cs = 0;
		$first_time = TRUE;
		$total = 0;
		$orig_date = 'foo';
		for( $i = 0; $i < mysql_num_rows($result); $i++ )
		{
			$row = mysql_fetch_array($result);

			if( $orig_date != $row[0] )
			{
				// We have skipped to a new date... so make a new row!!!

				if( !$first_time )
				{
					// Finish the current row...
					// Fill in the rest of the columns.
					for( $cs = $cs; $cs < self::c_num_charities; $cs++ )
					{
						echo '<td><p align="center">--</p></td>';
					}
	
					// Total
					// Don't show the total column anymore
					// echo '<td><b>' . number_format($total) . '</b></td>';
				}

				// Next row:
				// Date
				echo '<tr><th scope="row">' . $row[0] . '</th>';
				
				// Reset the charity column
				$cs = 0;

				$first_time = FALSE;
				$total = 0;
				$orig_date = $row[0];
			}

			// Fill in blank columns
			// $charity_order maps from this charity's ID to it's absolute ordering among all charities.
			for( $cs = $cs; $cs < $this->charity_order[ $row[2] ]; $cs++ )
			{
				echo '<td><p align="center">--</p></td>';
			}

			// Fill in the actual data! 
			// $cs = $row[2] at this point
			$val = $row[1] * Game::c_ounces_earned_per_correct;
			
			// Add any amounts for completed offers
			$val += $row[4];
			
			$total += $val;
						
			echo '<td>' . number_format($val) . '</td>';
			
			//$grand_total[$cs] += $val;

			$cs++;		
		}

		// Finish the leftover final row!
		// Finish the current row...
		// Fill in the rest of the columns.
		for( $cs = $cs; $cs < self::c_num_charities; $cs++ )
		{
			echo '<td>--</td>';
		}

		// Total
		// Don't show the total column anymore		
		// echo '<td><b>' . number_format($total) . '</b></td>';


		// Now echo the grand total row
		/*			
		echo '<tr><td>' . 'GRAND TOTAL' . '</td>';
		
		for( $i = 0; $i < self::c_num_charities; $i++ )
			echo '<td><b>' . number_format($grand_total[$i]) . '</b></td>';
			
		$super_grand_total = 0;
		for( $i = 0; $i < self::c_num_charities; $i++ ) {
			$super_grand_total += $grand_total[$i]; 
		}
		echo '<td><b>' . number_format($super_grand_total) . '</b></td>';
		
		echo '</tr>';
		*/

		echo '</table>';
	}

	function echo_for_current_month()
	{
		echo '<table class="prettyTable">';
				
		echo '<tr><th scope="col"><b>Date</b></th><th scope="col"><b>Charity: water</b></th><th scope="col"><b>Invisible Youth Network</b></th><th scope="col"><b>Nature Conservancy</b></th><th scope="col">Oaktree Foundation</b></th><th scope="col"><b>Philippine Aid Society</b></th></tr>';

		$year = date("Y");
		$month = date("m");

		$start_date_str = $year . '-' . $month . '-01';
		$end_date_str = $year . '-' . $month . '-31';
	
		// Select questions transactions joined with completed offers transactions!
		//$query = "select date_format(ask_date, '%M %d, %Y'), num_correct, charity_select, ask_date from transactions_daily WHERE ask_date >= '" . $start_date_str . "' AND ask_date <= '" . $end_date_str . "' order by ask_date asc, charity_select asc;";
		//$result = mysql_query($query) or die('Query failed: ' . mysql_error());
		
		
		//$query = "SELECT date_format(ask_date, '%M %d, %Y'), num_correct, transactions_daily.charity_select, ask_date, offers_daily.amount from transactions_daily LEFT JOIN offers_daily ON transactions_daily.ask_date = offers_daily.date AND transactions_daily.charity_select = offers_daily.charity_select WHERE ask_date >= '" . $start_date_str . "' AND ask_date <= '" . $end_date_str . "' order by ask_date asc, charity_select asc;";
		$query = "SELECT date_format(ask_date, '%M %d, %Y'), num_correct, transactions_daily.charity_select, ask_date, offers_daily.amount from (charities LEFT JOIN transactions_daily ON transactions_daily.charity_select = charities.id) LEFT JOIN offers_daily ON transactions_daily.ask_date = offers_daily.date AND transactions_daily.charity_select = offers_daily.charity_select WHERE charities.enabled = TRUE AND ask_date >= '" . $start_date_str . "' AND ask_date <= '" . $end_date_str . "' order by ask_date asc, charities.name asc;";
		//echo $query;
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());
		

		$cs = 0;
		$first_time = TRUE;
		$total = 0;
		$orig_date = 0;
		for( $i = 0; $i < mysql_num_rows($result); $i++ )
		{
			$row = mysql_fetch_array($result);

			if( $orig_date != $row[3] )
			{
				// We have skipped to a new date... so make a new row!!!

				if( !$first_time )
				{
					// Finish the current row...
					// Fill in the rest of the columns.
					for( $cs = $cs; $cs < self::c_num_charities; $cs++ )
					{
						echo '<td><p align="center">--</p></td>';
					}
	
					// Total
					// Don't show the total column anymore
					// echo '<td><b>' . number_format($total) . '</b></td>';
				}
				
				// Next row:
				// Date
				echo '<tr><th scope="row">' . $row[0] . '</th>';

				// Reset the charity column
				$cs = 0;

				$first_time = FALSE;
				$total = 0;
				$orig_date = $row[3];
			}

			// Fill in blank columns
			// $charity_order maps from this charity's ID to it's absolute ordering among all charities.
			for( $cs = $cs; $cs < $this->charity_order[ $row[2] ]; $cs++ )
			{
				echo '<td><p align="center">--</p></td>';
			}

			// Fill in the actual data! 
			// $cs = $row[2] at this point
			$val = $row[1] * Game::c_ounces_earned_per_correct;
			
			// Add any amounts for completed offers
			$val += $row[4];
			
			$total += $val;
			echo '<td>' . number_format($val) . '</td>';
			$cs++;

		}


		// Finish the leftover final row!
		// Finish the current row...
		// Fill in the rest of the columns.
		for( $cs = $cs; $cs < self::c_num_charities; $cs++ )
		{
			echo '<td>--</td>';
		}

		// Total
		// Don't show the total column anymore
		// echo '<td><b>' . number_format($total) . '</b></td>';
		echo '</table>';

	}
  
	function Totals() 
	{
		// Initializing crap
		connect_database();
		
		// charity_id --> order in list
		$this->charity_order[0] = 0;
		//$this->charity_order[1] = 0;
		$this->charity_order[2] = 3;
		$this->charity_order[3] = 2;
		$this->charity_order[4] = 4;		
		$this->charity_order[5] = 1;
	}
}
?>
