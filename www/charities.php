<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require 'minify_page_start.php';
require('common.php');
// I need game_database for current charity name.
require('game_database.php');
$game = new Game();
generic_page_start('charities');
?>

<h1>Select the charity to play for</h1>

<style>



div.partner {
    margin-bottom: 20px;
    margin-left: 10px;
    margin-right: 10px;
}

div.partner a {
	display: block;
	border: 3px solid #aaaaaa;
	padding: 10px;
	background: #ffffff;
}

div.partner a:hover {
	border: 3px solid #0000cc;
}

a, a:link, a:visited, a:hover {
    text-decoration: none;
    color: #000000;
}

a:link img, a:visited img { 
	border: none;
}

table tr td.logo {
    width: 150px;
}

table tr td.name {
    font-weight: bold;
    font-size: 1.2em;
	color: #0033aa;    
    padding-left: 20px;
}

div.partner img {
    vertical-align: middle;
    display: block;
    margin-left: auto;
    margin-right: auto

}


</style>


<script>
function click_effect(n)
{ 
	var element = document.getElementById('partnerlink' + n );
	element.style.border = "3px solid #00aa00";
}
</script>




<div class="partner" id="partner0">
	<a id="partnerlink0" onclick="click_effect(0);" href="index.php?charity_select=0" target="_top">
	<table><tr>
			<td class="logo">
				<img src="http://media.superdonate.org/client_data/charitylogo_0.png">
			</td>
			<td class="name">
				charity: water
			</td>
	<tr/></table>

	<div class="partnerText">
		charity: water is a nonprofit organization stimulating greater global awareness about extreme poverty, educating the public, and provoking compassionate and intelligent giving. 100% of public donations go directly toward water projects on the ground, while administrative costs are covered by a separate set of donors, grants, and our board.
	</div>
	</a>
</div>




<div class="partner" id="partner5">
	<a id="partnerlink5" onclick="click_effect(5);" href="index.php?charity_select=5" target="_top">
	<table><tr>
			<td class="logo">
				<img src="http://media.superdonate.org/client_data/charitylogo_5.png">
			</td>
			<td class="name">
				Invisible Youth Network
			</td>
	<tr/></table>

	<div class="partnerText">
		Invisible Youth is an expansive online network that provides resources and support for an estimated 1.5 million homeless at risk youth. Our influence is felt every day in cities all across America. Our mission is carried out through both partner organizations and volunteers who take to the streets to find, stabilize and otherwise help homeless and at risk youth to improve their quality of life.
		<br/><br/>
		Our focus reaches past the streets and lends itself to deterrence, research, resources, seminars, training and workshops that are provided to the communities and via the Internet. Despite our motto "Advocating for America's Youth," our endeavors are focused on all homeless and at risk youth with no regard to their geographic location. Mahatma Gandhi said, "You must be the change you wish to see in the world," and with that in mind, The Invisible Youth Network envisions better lives for children who cannot see it themselves..
	</div>
	</a>
</div>



<div class="partner" id="partner3">
	<a id="partnerlink3" onclick="click_effect(3);" href="index.php?charity_select=3" target="_top">
	<table><tr>
			<td class="logo">
				<img src="http://media.superdonate.org/client_data/charitylogo_3.png">
			</td>
			<td class="name">
				The Nature Conservancy
			</td>
	<tr/></table>

	<div class="partnerText">
		The Nature Conservancy is a global organization dedicated to protecting the lands and waters on which the diversity of life depends. We envision a world where forest, grasslands, deserts, rivers and oceans are healthy; where the connection between natural systems and the quality of human life is valued; and where the places that sustain all life endure for future generations. We are one of the world's largest private, non-profit, nongovernmental conservation organizations. Yet we operate at the local level, working in hundreds of communities on the ground to conserve natural diversity and sustain livelihoods.
	</div>
	</a>
</div>



<div class="partner" id="partner2">
	<a id="partnerlink2" onclick="click_effect(2);" href="index.php?charity_select=2" target="_top">
	<table><tr>
			<td class="logo">
				<img src="http://media.superdonate.org/client_data/charitylogo_2.png">
			</td>
			<td class="name">
				The Oaktree Foundation
			</td>
	<tr/></table>

	<div class="partnerText">
		The Oaktree Foundation is an aid and development organisation run entirely by volunteers under 26. At Oaktree we raise awareness in the community by running programs and events, we lobby the government and we fundraise for our overseas projects. We have partnered with developing communities in 6 countries to provide over 40,000 young people with increased educational opportunities. We believe that education is the most powerful force we have to change the world.
	</div>
	</a>
</div>





<div class="partner" id="partner4">
	<a id="partnerlink4" onclick="click_effect(4);" href="index.php?charity_select=4" target="_top">
	<table><tr>
			<td class="logo">
				<img src="http://media.superdonate.org/client_data/charitylogo_4.png">
			</td>
			<td class="name">
				Philippine Aid Society
			</td>
	<tr/></table>

	<div class="partnerText">
		Philippine Aid Society provides a variety of charitable services in the Philippines focused on eliminating poverty. Our four-fold plan of action includes feeding, caring for, educating, and employing the needy. Young men and women from poor households have the opportunity to pursue higher education and street children have a brighter future through our scholarship programs. Our livelihood programs are designed to take the poor and destitute and give them job skills and the means for self production so they can provide for themselves and for their families. Finally, we respond to natural disasters and immediate needs e.g. medical or nutritional.
	</div>
	</a>
</div>


<br/>
<a href="index.php" style="color:#0000ff; text-decoration:underline;">Keep your current charity (<?php echo $game->get_curr_charity_name();?>).</a> 

<br/><br/><br/>
<i>Sorry, CARE is no longer available on Charitii. CARE has been paid out in full.</i>

<?php
generic_page_end();
$minify_page_is_dynamic = TRUE;
$minify_file_name = __FILE__;
require 'minify_page_end.php';