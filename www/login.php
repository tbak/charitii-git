<?php 
// Copyright 2009, SuperDonate.  All rights reserved.
require ('minify_page_start.php');
require('common.php');
require('game_database.php');
connect_database();

function strstr_array( $haystack, $needle ) {
	if ( !is_array( $haystack ) ) {
		return FALSE;
	}
	foreach ( $haystack as $element ) {
		echo $needle;
		if ( strstr( $element, $needle ) ) {
			return $element;
		}
	}
	return FALSE;
}

function isValidName($n)
{
    $badwords = array('fuck', 'shit', 'cunt', 'asshole', 'dickhead', 'pussy', 'cock');
	if( strstr_array($badwords, $n) )
	{
		return FALSE;
	}
	return TRUE;
}

$success = FALSE;
$error['login_individual'] = FALSE;
$error['login_group'] = FALSE;
if( 	( isset($_POST['login_individual']) && strlen($_POST['login_individual'])>0 ) || 
		( isset($_POST['login_group']) && strlen($_POST['login_group'])>0 )) 
{
	$success = TRUE;
}

// Verify something was entered
if( isset($_POST['login_individual']) && strlen($_POST['login_individual'])>0 )
{
	$_POST['login_individual'] = sql_quote($_POST['login_individual']);
	$_POST['login_individual'] = strtolower($_POST['login_individual']);
	if( !isValidName( $_POST['login_individual'] ))
	{
		$error['login_individual'] = "Name is not valid";
		$success = FALSE;
	}
}
else
{
	$_POST['login_individual'] = "";
}

// Verify something was entered
if( isset($_POST['login_group']) && strlen($_POST['login_group'])>0 )
{
	$_POST['login_group'] = sql_quote($_POST['login_group']);
	$_POST['login_group'] = strtolower($_POST['login_group']);

	if( !isValidName( $_POST['login_group'] ))
	{
		$error['login_group'] = "Name is not valid";
		$success = FALSE;
	}
}
else
{
	$_POST['login_group'] = "";
}

if( $success )
{
	if( isset($_POST['login_individual']) && strlen($_POST['login_individual']) )
	{
		$_SESSION['login_individual'] = $_POST['login_individual'];

		$_SESSION['login_individual_points0'] = 0;
		$_SESSION['login_individual_points1'] = 0;
		$_SESSION['login_individual_points2'] = 0;
		$_SESSION['login_individual_points3'] = 0;

		$query = "select points, charity from user_stats where is_group='0' AND name='" . $_SESSION['login_individual'] . "' ;";
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());
		for( $i = 0; $i < mysql_num_rows($result); $i++ )
		{
			$row = mysql_fetch_array($result);
			$_SESSION['login_individual_points' . $row[1]] = $row[0];
		}

	}

	if( isset($_POST['login_group']) && strlen($_POST['login_group']) )
	{
		$_SESSION['login_group'] = $_POST['login_group'];

		$_SESSION['login_group_points0'] = 0;
		$_SESSION['login_group_points1'] = 0;
		$_SESSION['login_group_points2'] = 0;
		$_SESSION['login_group_points3'] = 0;

		$query = "select points, charity from user_stats where is_group='1' AND name='" . $_SESSION['login_group'] . "' ;";
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());
		for( $i = 0; $i < mysql_num_rows($result); $i++ )
		{
			$row = mysql_fetch_array($result);
			$_SESSION['login_group_points' . $row[1]] = $row[0];
		}

	}

	// Jump to the game page now that the dude is logged in.
    header('Location:index.php');
	//header('Location:login.php');
	exit();
}

generic_page_start('login');
?>

<h1>Login</h1>

<p>Sign up to track your personal donations!</p>

<p>Simply enter in a name for yourself and/or your team and click "Login". Stats will be kept for both your individual name and your team name.</p>

<br/>

<form id="form_login" name="form_login" action="login.php" method="post">

<table border="0" bgcolor="#ececec" cellspacing="5">
<tr><td>Name <?php if($error['login_individual']) echo '*error'; ?></td></tr>
<tr><td><input type="text" size="30" maxlength="19" name="login_individual" value="<?php echo $_POST['login_individual']; ?>"/></td></tr>
<tr><td>Team Name <?php if($error['login_group']) echo '*error'; ?></td></tr>
<tr><td><input type="text" size="30" maxlength="19" name="login_group" value="<?php echo $_POST['login_group']; ?>"/></td></tr>
<tr><td><input type="submit" value="Login" /></td></tr>
</table>
</form>

<?php 
generic_page_end();
$minify_page_is_dynamic = TRUE;
$minify_file_name = __FILE__;
require 'minify_page_end.php';
?>
