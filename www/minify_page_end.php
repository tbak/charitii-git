<?php
// Copyright 2009, SuperDonate.  All rights reserved.
/*
TB - Modifications I made:
Global settings
$minify_page_is_dynamic = TRUE; (so that you always send a fresh file)
$minify_file_name = __FILE__; (since __FILE__ will always be "minify_page_end.php" which is useless.
*/
?>

<?php
$content = ob_get_clean();

require 'Minify.php';

/*
if ($minifyCachePath) {
    //Minify::useServerCache($minifyCachePath);
    Minify::setCache($minifyCachePath);
}
*/
Minify::setCache($min_cachePath); 

if( !isset($minify_page_is_dynamic) || !$minify_page_is_dynamic )
{

	$pageLastUpdate = max(
		filemtime($minify_file_name)
		,$jsBuild->lastModified
		,$cssBuild->lastModified
	);
}
else
{
	//$pageLastUpdate = time();
	$pageLastUpdate = time() + 1000;
}

Minify::serve('Page', array(
    'content' => $content
    ,'id' => $minify_file_name
    ,'lastModifiedTime' => $pageLastUpdate
    // also minify the CSS/JS inside the HTML 
    ,'minifyAll' => true
));
