<?php
// Copyright 2009, SuperDonate.  All rights reserved.

 // game + database stuff - only relevant for game p1 + p2
// Eventually database will be relevant for puzzle submissions, too!

/*
// TB TEMP -- This SQL statement will update the calculated difficulty for all rows

UPDATE puzzles SET difficulty_calculated =
IF( num_respond < 100,
	( difficulty_original * ((100-num_respond)/100) + (num_incorrect/num_respond) * 100 *(num_respond/100)  ),
	( (num_incorrect/num_respond) * 100 )
)
WHERE num_respond > 0 AND enabled = 1;

// This updates the difficulty faster:
UPDATE puzzles SET difficulty_calculated =
		IF( num_respond = 0 ,
			difficulty_original,
			IF( num_respond < 20,
				( difficulty_original * ((20-num_respond)/20) + (num_incorrect/num_respond) * 100 *(num_respond/20)  ),
				( (num_incorrect/num_respond) * 100 )
			)
		)

// TB TEMP -- This SQL statement will condense all of the transactions into a daily transaction table
INSERT INTO transactions_daily (ask_date, num_correct, num_respond)
  SELECT date_format(ask_date, '%Y%m%d'), SUM(did_answer_correct), SUM(did_respond)
  FROM transactions
  WHERE 1=1
  GROUP BY 1;
  ORDER BY 1 ASC;

Note that it inserts for all transactions, including those dates that are already in the daily transaction table.
I need some way to then filter out the older duplicate transaction_daily rows.
I like constantly inserting so that any changes to transaction rows in the past will be reflected in the daily tables...

*/

require "db.php";


// Game management
class Game 
{
	// TB TODO - Enable/disable charities?	
	const c_num_answers = 4;
	const c_initial_difficulty_progression_num_questions = 5;
	const c_initial_difficulty_progression_inc = 4;
	const c_initial_difficulty_progression_dec = 3;
	const c_ounces_earned_per_correct = 10;
	const c_referral_bonus_num_correct_needed = 10;

	const c_insert_into_transactions = FALSE;

	public $token;
	public $did_submit;
	public $did_answer_correct;
	public $clue;
	public $words;
	public $puzzle_id;
	public $correct_answer_slot;
	public $is_facebook = FALSE;

	// The clue and correct answer from the previous page
	public $previous_clue;
	public $previous_correct_word;  // this word does not have blanks inserted.
	public $previous_correct_word_with_missing_letters;
	public $previous_num_missing_letters;

	protected $initial_difficulty_progression_enabled;

	function insert_transaction()
	{
		//
		// Post a new transaction to the database
		// 
		if( self::c_insert_into_transactions )
		{
			$query = "INSERT INTO transactions (puzzle_id, ask_date, ip_address) VALUES ('" . $this->puzzle_id . "', NOW(), inet_aton('" . $_SERVER['REMOTE_ADDR'] . "') )";
			// Useful for the future perhaps? FROM_UNIXTIME(" . time() . ")
			$result = mysql_query($query) or die('Query failed: ' . mysql_error());

			// Keep track of the transaction id so that we can update it with the results of the answer.
			$_SESSION['transaction_id'] = mysql_insert_id();
		}
		else
		{
			$_SESSION['transaction_id'] = -1;
		}
		
	}

	function update_transaction()
	{
		//
		// Update the transaction!
		//

		if( self::c_insert_into_transactions )
		{
			$query = "UPDATE transactions SET did_respond='1', did_answer_correct='$this->did_answer_correct' WHERE id=" . $_SESSION['transaction_id'];
			$result = mysql_query($query) or die('Query failed: ' . mysql_error());
		}

		$query = "INSERT INTO transactions_daily (ask_date, num_correct, num_respond, charity_select) VALUES ( NOW(), '" . $this->did_answer_correct . "', '1', '". $_SESSION['charity_select'] . "' )
					ON DUPLICATE KEY UPDATE num_correct=num_correct + " . $this->did_answer_correct . ", num_respond=num_respond+1; ";
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());


		
		//
		// Update user database
		//
		
		// Get the user num_correct/num_respond values to update
		// Also update the charity that the user is using, so that it'll return to that
		// when the user logs back in!
		$query = "SELECT num_correct, num_respond from users where id = " . $_SESSION['user_id'] . ";";		
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());

		if( mysql_num_rows($result) > 0 ) {
			$row = mysql_fetch_assoc($result);
			$corr = $row['num_correct'] + $this->did_answer_correct;			  
			$resp = $row['num_respond'] + 1;
			
			$query = sprintf("update users set num_correct=%d, num_respond=%d, charity_select=%d where id=%d",
							$corr, $resp, $_SESSION['charity_select'], $_SESSION['user_id'] );
			$result = mysql_query($query) or die('Query failed: ' . mysql_error());			
		}
			
					
		
		// Store it again
		$stored_points = $_SESSION['donated_amount' . $_SESSION['charity_select'] ];		
		$query = sprintf("INSERT INTO user_scores(user_id,points,charity) values (%d,%d,%d) ON DUPLICATE KEY UPDATE points=%d",
							$_SESSION['user_id'], $stored_points, $_SESSION['charity_select'], $stored_points );
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());
							
	}

	private function update_puzzle_word_stats()
	{
		// This string dynamically updates the difficulty based on how many right/wrong answers there were
		// The difficulty starts at the original baseline difficulty (based on the day of the week), but
		// then progresses towards depending only on the right/wrong results (after 20 results).
		// num_respond can be 0 since it could have been initially answered with missing letters...
		$difficulty_calculate_string = 
		' difficulty_calculated =
		IF( num_respond = 0 ,
			difficulty_original,
			IF( num_respond < 20,
				( difficulty_original * ((20-num_respond)/20) + (num_incorrect/num_respond) * 100 *(num_respond/20)  ),
				( (num_incorrect/num_respond) * 100 )
			)
		)';

		// Either store the correct/incorrect info in the regular or missing_letter field.
		// Do this since answering questions with missing letters is much harder and should not skew the difficulty calculation
		// TB TODO - Should I just get rid of num_respond to avoid any confusion?
		$inc_string = '';
		if( $this->previous_num_missing_letters == 0 )
		{
			if( $this->did_answer_correct )
			{
				$inc_string = 'num_respond=num_respond+1 , num_correct=num_correct+1';
			}
			else
			{
				$inc_string = 'num_respond=num_respond+1 , num_incorrect=num_incorrect+1';
			}
		}
		else
		{
			// Do not alter num_respond since it's used for the difficulty calculation...
			if( $this->did_answer_correct )
			{
				$inc_string = 'num_correct_missing_letters=num_correct_missing_letters+1';
			}
			else
			{
				$inc_string = 'num_incorrect_missing_letters=num_incorrect_missing_letters+1';
			}
		}

		$query = 'UPDATE puzzles SET ' . $inc_string . ', ' . $difficulty_calculate_string . ' WHERE id=' . $_SESSION['puzzle_id'];

		//echo $query;
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	}

	private function check_token()
	{
		// Simple spam check
		// Check that the posted token field matches the token in the session. 
		if( isset($_SESSION['token']) )
		{
			if( !isset($_POST['token']) || $_SESSION['token'] != $_POST['token'] )
			{
				//echo "spammer!";
				// Person probably just hit the reload button on his browser... just act as though
				// nothing happened (no choice was made)
				unset($_POST['selected']);
			}

		}
	
		// Generate the token for the next page game page load
		$secret = 'magicalspambustingnumpties';

		// Ensure that the new token is different than the current token
		if( !isset($_SESSION['token']) )
		{
			$_SESSION['token'] = 0;
		}
		$this->token = $_SESSION['token'];
		while( $_SESSION['token'] == $this->token )
		{
			$this->token = md5(rand(1, 1000).$secret);
		}
		$_SESSION['token'] = $this->token;

	}

	public function get_num_played()
	{
		return $_SESSION['num_correct'] + $_SESSION['num_incorrect'];
	}
	
	function check_user_state()
	{
		//echo 'CHECK USER';
		
		if( !isset($_SESSION['user_id']) ) {
			//echo 'START NEW';
    		$this->start_new();
		}
		else {
			// Load the user score from the database here?
			// This way the session variable is consistent with what's in the database...
			$query = "SELECT id, points from user_scores where user_id = " . $_SESSION['user_id'] . " and charity=".$_SESSION['charity_select'].";";
			$result = mysql_query($query) or die('Query failed: ' . mysql_error());
			if( mysql_num_rows($result) > 0 ) {
				$row = mysql_fetch_assoc($result);
				$_SESSION['donated_amount' . $_SESSION['charity_select'] ] = $row['points'];
			}
		}
		
		$this->check_debug_variables();		
	}

	function generate_valid_question()
	{		
		// TB - Why is this called here if it's already being called in check_user_state()? 
		// $this->check_debug_variables();

		// Try 4 times to get a proper question... This should be more than enough...
		$num_attempts = 4;
		$force_common_word_length = false;
		for( $attempt = 0; $attempt < $num_attempts; $attempt++ )
		{
			if( $attempt == $num_attempts - 1 ) {
				$force_common_word_length = true;
			}
			
			if( $this->generate_next_question($force_common_word_length) == false && $attempt == $num_attempts-1 )
			{
				// TB TODO - This will not do obviously...
				// Maybe send a refresh header?
				// Or more attempts?
				mail('tom@thomasbak.com','CH-Unable to generate complete question','difficulty ='.$_SESSION['difficulty'],'From: Puz <Puz@Puz.com>' );
				die("Unable to generate complete question! Someone has been notified. Please refresh your browser for another quetsion.");
			}
			else
			{
				break;
			}
		}
	}

		
	function start_new()
	{		
		if( isset($_COOKIE['user_id']) ) {
			// When a user logs out this cookie is destroyed.
			// So this cookie is mostly just useful for anonymous users that return
			// to the game later, since they have no way of logging out to clear the cookie.
			user_login($_COOKIE['user_id']);
		}
		else {
			// We don't know who this is -- so reset everything.
			create_new_anonymous_user();
		}		

		// These also need to be reset.
		$this->set_previous_values();

	}

	function check_debug_variables()
	{
		//
		// DEBUGGING GET VARIABLES
		// TB TODO - Get rid of these for production!!!
		//
        if( isset($_GET['num_missing_letters']) )
		{
			$_SESSION['num_missing_letters'] = sql_quote($_GET['num_missing_letters']);
		}

		if( isset($_GET['difficulty']) )
		{
			$_SESSION['difficulty'] = sql_quote($_GET['difficulty']);
		}

		if( isset($_GET['min_word_length'] ))
		{
			$_SESSION['min_word_length'] = sql_quote($_GET['min_word_length']);
		}

		if( isset($_GET['max_word_length'] ))
		{
			$_SESSION['max_word_length'] = sql_quote($_GET['max_word_length']);
		}

		// Make sure that the word lengths are not impossible or silly
		if( $_SESSION['min_word_length'] > 10 )
		{
			$_SESSION['min_word_length'] = 10;
		}
		if( $_SESSION['max_word_length'] > 0 && $_SESSION['min_word_length'] < 3 )
		{
			// All words must be at least 3 characters
			$_SESSION['min_word_length'] = 3;
		}
		if( $_SESSION['max_word_length'] > 0 && $_SESSION['max_word_length'] < 3 )
		{
			$_SESSION['max_word_length'] = 3;
		}
		if( $_SESSION['max_word_length'] > 0 && $_SESSION['min_word_length'] > 0 && $_SESSION['max_word_length'] < $_SESSION['min_word_length'] )
		{
			$_SESSION['max_word_length'] = $_SESSION['min_word_length'];
		}

	}

	private function check_answer()
	{
	

		// Initially, difficulty progression moves much quicker!
		// You get +6 if you're right, and -5 if you're wrong.

		// Check previous answer...
		if( $_POST['selected'] == $_SESSION['correct_answer_slot'] )
		{
			$_SESSION['num_correct']++;
			$this->did_answer_correct = 1;
			$_SESSION['num_correct_in_a_row']++;
			$_SESSION['num_incorrect_in_a_row'] = 0;


			$_SESSION['donated_amount' . $_SESSION['charity_select'] ] += self::c_ounces_earned_per_correct;
            
			$inc = 0;
			if( $this->initial_difficulty_progression_enabled && $this->get_num_played() <= self::c_initial_difficulty_progression_num_questions )
			{
				$inc = self::c_initial_difficulty_progression_inc;
			}
			else
			{
				// Must get 2 in a row correct to increase level!
				/*
				if( $_SESSION['num_correct_in_a_row'] >= 2 && $_SESSION['num_correct_in_a_row'] % 2 == 0 )
				{
					$inc = 1;
				}
				*/
				// TB TEST - That way above is too hard! You need 4 correct in a row followed by
				// a single incorrect just to go up 1 level higher! It's too hard!
				// Instead, the first correct answer doensn't level you up, but then every subsequent
				// one does.				
				if( $_SESSION['num_correct_in_a_row'] >= 2 ) {
					$inc = 1;
				}				
				
			}

			$_SESSION['difficulty'] = $_SESSION['difficulty'] + $inc;

			// Update the number of missing letters based on how many you've gotten right in a row
			// Unless an option has been set to not mess with it...
			if( !isset($_COOKIE['num_missing_letters']) || $_COOKIE['num_missing_letters'] == -1 )
			{
				if( $_SESSION['num_correct_in_a_row'] >= 8 )
				{
					$_SESSION['num_missing_letters'] = 2;
				}
				else if( $_SESSION['num_correct_in_a_row'] >= 4 )
				{
					$_SESSION['num_missing_letters'] = 1;
				}
			}
		}
		else
		{
			$_SESSION['num_incorrect']++;
			$_SESSION['num_correct_in_a_row'] = 0;
			$_SESSION['num_incorrect_in_a_row']++;
			$this->did_answer_correct = 0;

			$dec = 0;
			if( $this->initial_difficulty_progression_enabled && $this->get_num_played() <= self::c_initial_difficulty_progression_num_questions )
			{
				$dec = self::c_initial_difficulty_progression_dec;
			}
			else
			{
				$dec = 1;
			}
			$_SESSION['difficulty'] = $_SESSION['difficulty'] - $dec;

			// Revert back to no missing letters if too many have been answered wrong
			if( !isset($_COOKIE['num_missing_letters']) || $_COOKIE['num_missing_letters'] == -1 )
			{
				// Always reset back down to 0 missing letters when you miss a question
				// But make the missing letters appear sooner?
				// That should keep things changing more frequently...
				$_SESSION['num_missing_letters'] = 0;

				/*
				if( $_SESSION['num_incorrect_in_a_row'] >= 3 )
				{
					$_SESSION['num_missing_letters'] = 0;
				}
				else
				{
					// Reduce to 1 always
					if( $_SESSION['num_missing_letters'] > 1 )
					{
						$_SESSION['num_missing_letters'] = 1;
					}
					
				}
				*/
			}
		}

		if( $_SESSION['difficulty'] > 99 )
		{
			$_SESSION['difficulty'] = 99;
		}
		else if( $_SESSION['difficulty'] < 0 )
		{
			$_SESSION['difficulty'] = 0;
		}

		$this->update_transaction();
		$this->update_puzzle_word_stats();
	}

	private function remove_letters_from_word( $i )
	{
		// TB TEST - Remove random letters!
		// TB TODO - Is there any way to not constantly check random indices for a non-blank space? It's slow...
		$word_length = strlen( $this->words[$i] );
	
		// Always have at least 2 letters showing
		$num_letters_to_remove = $_SESSION['num_missing_letters'];
		if( $num_letters_to_remove > $word_length - 2 )
		{
			$num_letters_to_remove = $word_length - 2;
		}

		while( $num_letters_to_remove > 0 )
		{
			$r = rand(0,$word_length-1);
			if( $this->words[$i][ $r ] != '#' )
			{
				$this->words[$i][ $r ] = '#';
				$num_letters_to_remove--;
			}
		}
	}

	private function set_previous_values()
	{
		// Remember some values from the last page view before they get overwritten so that we can display them later
		$this->previous_clue = isset($_SESSION['clue']) ? $_SESSION['clue'] : 0;
		$this->previous_correct_word = isset($_SESSION['correct_word']) ? $_SESSION['correct_word'] : 0;
		$this->previous_num_missing_letters = isset($_SESSION['num_missing_letters']) ? $_SESSION['num_missing_letters'] : 0;
		$this->previous_correct_word_with_missing_letters = isset($_SESSION['correct_word_with_missing_letters']) ? $_SESSION['correct_word_with_missing_letters'] : 0;
	}

	private function are_words_too_similar( $w0, $w1 )
	{
		if( strlen($w0) != strlen($w1) )
		{
			return false;
		}

		// There must at least be this many letters that are different
		// word 0: AAAA
		// word 1: AABC
		// If there are 2 missing letters, A A _ _ could be displayed for both words.
		// But if there is one more letter that is different, they can't possibly show the same thing.
		$min_letters_different = $_SESSION['num_missing_letters'] + 1;

		$num_different = 0;
		for( $i = 0; $i < strlen($w0); $i++ )
		{
			if( $w0[$i] != $w1[$i] )
			{
				$num_different++;
				if( $num_different >= $min_letters_different )
				{
					return false;
				}
			}
		}

		//echo 'Too similar: ' . $w0 . ' + ' . $w1 . '<br>';

		return true;
	}

	private function generate_next_question($force_common_word_length)
	{

		//
		// Generate the next question
		// 
		$this->puzzle_id = 0;
		$this->correct_answer_slot = rand(0,3);

		// Chunk this up for now
		$diff_chunk = 10;
		if( $_SESSION['difficulty'] < 5 ) $diff_chunk = 5;
		else if( $_SESSION['difficulty'] < 15 ) $diff_chunk = 15;
		else if( $_SESSION['difficulty'] < 25 ) $diff_chunk = 25;
		else if( $_SESSION['difficulty'] < 35 ) $diff_chunk = 35;
		else if( $_SESSION['difficulty'] < 45 ) $diff_chunk = 45;
		else if( $_SESSION['difficulty'] < 55 ) $diff_chunk = 55;
		else $diff_chunk = 60;

		$diff_min = $diff_chunk-10;
		$diff_max = $diff_chunk;

		if( $diff_chunk > 50 )
		{
			$diff_min = 50;
			$diff_max = 100;
		}

		// All possible answers have the same length...
		// A word length setting of 0 = no preference...
		// So initially pick a word length to use
		// Most of the time use words from 4-6 letters (most typical word size)
		$r = rand(0,100);
		if( $r < 65 ) {
			$min = 4;
			$max = 6;
		}
		else if( $r < 80 ) {
			$min = 3;
			$max = 3;
		}
		else if( $r < 90 ) {
			$min = 7;
			$max = 7;
		}
		else if( $r < 95 ) {
			$min = 8;
			$max = 8;
		}
		else {
			$min = 9;
			$max = 10;
		}
		
		// If this is the first question, it's always 5 letters long
		// This ensures that the bouncing suggest box will always fit.
		// 5 letter answers are also the most normal.
		if( $_SESSION['num_correct'] == 0 ) {			
			$min = 5;
			$max = 5;
		}
		
		// A word length setting of 0 = no preference (default)...
		// Otherwise use the min maxes generated above!
		if( $_SESSION['min_word_length'] > 0 ) {
			$min = $_SESSION['min_word_length'];
		}
		if( $_SESSION['max_word_length'] > 0 ) {
			$max = $_SESSION['max_word_length'];
		}	

		// Check any word length variable. If we completely failed
		// finding a valid quiestion + answer sequence in the past, get
		// a word with 5 letters since those are the most common
		if( $force_common_word_length ) {
			$max = 5;
			$min = 5;	
		}
		
		$word_length_condition = ' AND answer_strlen = ' . rand($min,$max) . ' ';
		
		
		$found_question = false;
		for( $find_question_attempt = 0; $find_question_attempt < 2; $find_question_attempt++ ) {
			// First try to get a question with this difficulty level and the required word length condition.
			// If that fails, just get ANY question with this difficulty...
			if( $find_question_attempt == 1 ) {
				$word_length_condition = '';				
			}
			$query = "SELECT id, clue, answer FROM puzzles WHERE difficulty_scaled >= " . $diff_min . " and difficulty_scaled <= " . $diff_max . $word_length_condition . " AND enabled = 1 ORDER BY RAND() LIMIT 1;";
			$result = mysql_query($query) or die('Query failed: ' . mysql_error());
			if( mysql_num_rows($result) >= 1 ) {
				$found_question = true;
				
				// Get the correct word first, regardless of what slot it goes into...
				// We need to know what the word is so that we don't include alternative words that are the same or very similar...
				$row = mysql_fetch_assoc($result);
				$this->words[$this->correct_answer_slot] = $row['answer'];
				$this->clue = utf8_encode($row['clue']);
				$this->puzzle_id = $row['id'];
				// Store the correct word in this session variable before it gets blank spaces inserted...
				$_SESSION['correct_word'] = $this->words[$this->correct_answer_slot];
				
				break;
			}
		}
		
		if( !$found_question ) {
			mail('tom@thomasbak.com','CH-generate_next_question() could not find a valid question','difficulty ='.$_SESSION['difficulty'],'From: Puz <Puz@Puz.com>' );
			return false;
		}
				
		// Now we need to find some other potential answers that have the same word length!
		
		// We need to reform the word length condition since it is possible that a potential answer
		// of an arbitrary length was returned (in the case that a particular difficulty level doesn't have
		// any words with that length!
		$word_length_condition = ' answer_strlen = ' . strlen($this->words[$this->correct_answer_slot]) . ' '; 

		$query = "SELECT answer FROM puzzles WHERE " . $word_length_condition . " AND enabled = 1 ORDER BY RAND() LIMIT 10;";		
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());

		
		// Keep getting possible answers until we get enough that work
		$success = false;
		$current_word_slot = 0;
		while( $row = mysql_fetch_array($result) ) {
			
			// Check if this is the solution slot (we've already filled that in)
			if( $current_word_slot == $this->correct_answer_slot )
			{
				$current_word_slot++;
			}			

			// Have we filled in all of the slots? Then we're done!
			if( $current_word_slot == self::c_num_answers )
			{
				$success = true;
				break;
			}
			
            // Test for too-similar words against the clue word
			$too_similar = false;
			if( $this->are_words_too_similar($row[0], $this->clue) )
			{
				// We can't use this word...
				$too_similar = true;				
			}

			// TB - Now test against all the previous answer words, too!
			// We have to do the correct answer separately first since it could be located at any index...
			for( $test_index = 0; $test_index < $current_word_slot; $test_index++ )
			{
				if( $this->are_words_too_similar($row[0], $this->words[$test_index]) )
				{
					$too_similar = true;					
				}
			}
			if( $too_similar )
			{
				continue;
			}						

			$this->words[$current_word_slot] = $row[0];

			// OK! Slot is filled! Go to next word!
			$current_word_slot++;
		} 
		
		if( !$success )
		{
			// We were unable to find all the alternate words needed... :(
			return false;
		}

		if( $_SESSION['num_missing_letters'] )
		{
			for( $i = 0; $i < self::c_num_answers; $i++ )
			{
				$this->remove_letters_from_word($i);
			}
		}

		// Not necessary
		// mysql_free_result($result);

		// Now store the word again with letters missing 
		// With these two strings, we can colorize what letters were missing in the next page.
		$_SESSION['correct_word_with_missing_letters'] = $this->words[$this->correct_answer_slot];

		$this->insert_transaction();

        // Set up session variables needed for the next page.
		// We will post what the user selected later, and then we just need to compare what he picked
		// with the correct_answer_slot session variable.
		$_SESSION['correct_answer_slot'] = $this->correct_answer_slot;
		$_SESSION['clue'] = $this->clue;
		$_SESSION['word0'] = $this->words[0];
		$_SESSION['word1'] = $this->words[1];
		$_SESSION['word2'] = $this->words[2];
		$_SESSION['word3'] = $this->words[3];
		$_SESSION['puzzle_id'] = $this->puzzle_id;
		
		
		// echo "clue is: " . $row["clue"] . "<br>";
		// echo "solution is: " . $row["answer"] . "<br>";

		// Success!
		return true;
	}

	function print_difficulty_level()
	{
		echo '<div id="mainProgressDifficultyLevel">';
		echo 'DIFFICULTY LEVEL ';
		if( $this->initial_difficulty_progression_enabled && $this->get_num_played() < self::c_initial_difficulty_progression_num_questions )
		{
			echo '**';
		}
		else
		{
			echo ($_SESSION['difficulty'] + 1);
		}

		if( $_SESSION['num_missing_letters'] > 0 )
		{
			echo '<br>(' . $_SESSION['num_missing_letters'] . ' hidden letter' . ($_SESSION['num_missing_letters'] > 1 ? 's' : '') . ')';
		}

		echo '</div>';
		
	}

	function print_previous_word_colorize()
	{
		// Colorize the missing letters differently, eh?
		// TB TODO - This should use the CSS stuff I guess???

		if( $this->previous_num_missing_letters == 0 )
		{
			// Just spit out the word if there are no missing letters --> faster
			echo $this->previous_correct_word;
		}
		else
		{
			for( $i = 0; $i < strlen($this->previous_correct_word); $i++ )
			{
				if( $this->previous_correct_word_with_missing_letters[$i] == '#' )
				{
					echo '<span style="color:blue">' . $this->previous_correct_word[$i] . '</span>';
				}
				else
				{
					echo $this->previous_correct_word_with_missing_letters[$i];
				}
				
			}
		}
		
	}

	function get_curr_donated_amount()
	{
		return $_SESSION['donated_amount' . $_SESSION['charity_select'] ];
	}

	function get_curr_charity_name()
	{
		return get_charity_name($_SESSION['charity_select']);		
	}
	
	function get_curr_charity_units()
	{
        return "points";
	}

	function output_word_select_form()
	{

		// Button images with text inside of them
		echo '<div id="mainGamePuzzle">';

		$len = strlen($this->clue);
		echo '<div id="mainGamePuzzleQuestion">';
		if( $len > 35 )
			echo '<span class="tiny">';
		else if( $len > 25 )
			echo '<span class="small">';
		echo "&raquo; ".$this->clue;
	
		if( $len > 25 )
			echo '</span>';
		echo '</div>';

		echo '<div class="possibleAnswerList">';
	

		echo '<form id="form1" name="form1" action="index.php" method="post">';
		/*AUTOCOMPLETE="OFF"*/
		
		echo '<input type="hidden" id="selected" name="selected" />';
		echo '<input type="hidden" name="token" value="' . $this->token . '" />';
		echo '<input type="hidden" id="charity_select" name="charity_select" value="' . $_SESSION['charity_select'] . '" />';


		for( $i = 0; $i < self::c_num_answers; $i++ )
		{

			echo '<div class="possibleAnswer" id="possibleAnswer' . $i . '" >';

			/*
			// Google bug fix: If the noscript input here has the same name as the "selected" hidden input value, and you reload the
			// form through AJAX, and THEN try to regularly reload the page through a form submit, the value of document.form.selected.value will be undefined!
			// This code will fail (alert will say undefined even though it was JUST set):
			// document.form1.selected.value=1;
			// alert( document.form1.selected.value );
			// So I need to rename selected to selected_noscript, and then handle that when the form is sent. Retarded!

			if( !$this->is_facebook )
                echo '<noscript><input type="submit" name="selected_noscript" value="' . $i . '"/></noscript>';

            echo '<a onclick="submitAnswer(' . "'{$i}', '{$this->token}'" . ')"  onmouseover="answerMouseOver(' . $i . ') ' . '" onmouseout="answerMouseOut(' . $i . ')' . '" >' . '<span>' . $this->words[$i] . '</span>' . '</a>';

            */
			
            // TB TEST - Show correct answer for first question???
			if( $_SESSION['num_correct'] == 0 && $i == $_SESSION['correct_answer_slot'] )
				echo '<div id="answerSuggest">Click this answer to donate!</div>';

				
            if( !$this->is_facebook )
                echo '<noscript><input type="submit" name="selected_noscript" value="' . $i . '"/></noscript>';
			
			echo '<a onclick="submitAnswer(' . "'{$i}', '{$this->token}'" . ')"  onmouseover="answerMouseOver(' . $i . ') ' . '" onmouseout="answerMouseOut(' . $i . ')" >';
			
			echo '<table class="checkerRow"><tr>';
			
			$len = strlen($this->words[$i]);
			
			$remaining_boxes = 12-$len-2;
			$leftlen = floor($remaining_boxes/2);
			$rightlen = floor($remaining_boxes/2) + $remaining_boxes%2;
			
			for( $b = 0; $b < $leftlen; $b++ ) {
				echo '<td class="other">&nbsp;</td>';
			}
			
			echo '<td class="filled"></td>';
			
			for( $b = 0; $b < strlen($this->words[$i]); $b++ ) {
				$letter = $this->words[$i][$b];
				if( $letter == '#' ) {
					$letter = '&nbsp;';
				}
				echo '<td>' . $letter . '</td>';
			}
			
			echo '<td class="filled"></td>';
			
			for( $b = 0; $b < $rightlen; $b++ ) {
				echo '<td class="other">&nbsp;</td>';
			}	
			
			echo '</tr></table>';
			echo '</a>';
			
			
			echo '</div>';
			
		}
		
		echo '</form>';
		echo '</div><!--possibleAnswerList-->';

		// Allow sending a bonus invite if you have played a few rounds and are not currently playing for a bonus
		if( !isset($_SESSION['referral_bonus_email']) && $this->get_curr_donated_amount() >= 30 && !$this->is_facebook ) {
			echo '
			<div id="mainGameSendBonus">
				<a id="mainGameSendBonusLink" class="thickbox" href="bonus_invite.php?KeepThis=true&height=500&width=600">Send a Charitii bonus to a friend</a>
			</div>';
		}

		echo '</div><!--mainGamePuzzle-->';

		if( !$this->is_facebook )
		{

			echo '
			<div id="mainGamePuzzleRight">
			<div id="mainGamePuzzleDonationInfo">
				
				<div id="di_youhave">
					You have won
				</div>

				<div id="di_number">' . number_format($this->get_curr_donated_amount()) . '</div> <div id="di_unit">' . $this->get_curr_charity_units() . '</div>';

				
				
				$this->print_difficulty_level();

				echo '<div id="fb_wallAndInvite"></div>';

			echo '</div>';			

            /*
			if( !isset($_SESSION['user_name']) ) {
				echo '<div>';
				echo 'Want to join? <a id="gameBoxRegister" class="thickbox" href="register2.php?height=430&width=500">Register</a> in seconds';			
				echo '</div>';
			}
            */

						
			echo '</div>';
			echo '<div class="clear"></div>';		

		}

		//apply_round_borders(0);


	}

	function output_correct_answer()
	{

		if( $this->did_answer_correct )
		{
			echo '<div id="mainGameAnswerCorrect">';

			echo '<div id="correct">CORRECT! <span>You won 10 ' . $this->get_curr_charity_units() . '</span></div>';
			
			// Use the small text DIV if the clue was very long...
			if( strlen($this->previous_clue) > 25 )
			{
				echo '<div id="correctDefSmall">';
			}
			else
			{
				echo '<div id="correctDef">';
			}
		}
		else
		{
			echo '<div id="mainGameAnswerIncorrect">';
			if( strlen($this->previous_clue) > 25 )
			{
				echo '<div id="incorrectDefSmall">';
			}
			else
			{
				echo '<div id="incorrectDef">';
			}
		}

		echo $this->previous_clue . ' = ';
		$this->print_previous_word_colorize();

		echo '</div>';

		echo '</div>';


		// Kick off any effects now that the HTML is constructed
		if( $this->did_answer_correct )
		{
			if( $_SESSION['user_id'] == 1 ) {
				echo 'curr:' . $this->get_curr_donated_amount();
				echo 'minus:' . $this->get_curr_donated_amount()-10;
				echo 'sess:' . $_SESSION['donated_amount' . $_SESSION['charity_select'] ];
				echo 'char:' . $_SESSION['charity_select'];
			}
			
			echo '<script type="text/javascript" language="JavaScript">startCorrectAnswerEffect(' . ($this->get_curr_donated_amount()-10) . ',' . $this->get_curr_donated_amount() . ');</script>';
		}
	}

	
	function check_referral_bonus_game_box()
	{

		if( !isset($_SESSION['referral_bonus_email']) ) {
			return;
		}

		if( $_SESSION['num_correct'] == self::c_referral_bonus_num_correct_needed ) {
			// Display congratulations message
			echo '<div class="mainGameNotification">';
			echo '<b>Congratulations!</b><br/>';
			echo 'You earned the double charity bonus!<br/>';
			echo '</div>';

			echo '<script type="text/javascript">$("#referralBonus").hide();</script>';

			// Apply the 10 extra correct answers to the currently selected charity.
			$query = "INSERT INTO transactions_daily (ask_date, num_correct, num_respond, charity_select) VALUES ( NOW(), '" . self::c_referral_bonus_num_correct_needed . "', '1', '". $_SESSION['charity_select'] . "' )
					ON DUPLICATE KEY UPDATE num_correct=num_correct + " . self::c_referral_bonus_num_correct_needed . ", num_respond=num_respond + " . self::c_referral_bonus_num_correct_needed . " ; ";
			$result = mysql_query($query) or die('Query failed: ' . mysql_error());

			// Update the num_correct value and scores...
			$_SESSION['donated_amount' . $_SESSION['charity_select'] ] += self::c_referral_bonus_num_correct_needed * self::c_ounces_earned_per_correct;

			// This email address is now used and can not play the bonus game again.
			$query = "UPDATE referral_bonus SET used='1' WHERE receiver_email='" . $_SESSION['referral_bonus_email'] . "';";
			$result = mysql_query($query) or die('Query failed: ' . mysql_error());

			// Make sure that the message goes away next frame.
			unset($_SESSION['referral_bonus_email']);
		}
		else {
			echo '<div class="mainGameNotification">';
			$num = self::c_referral_bonus_num_correct_needed - $_SESSION['num_correct'];
			$plural = ($num > 1 ? 's' : '');
			echo 'Answer <b>' . $num . '</b> more question' . $plural . ' to earn the bonus';
			echo '</div>';
		}
	}

	// Process referral bonus display stuff at the top of the page
	function display_referral_bonus()
	{
		if( isset($_GET['bonus']) ) {

			// Ensure that the email address is registered in the database.
			// This way people can't just use random email GET addresses to get more bonuses

			$_GET['bonus'] = sql_quote($_GET['bonus']);

			$query = "SELECT sender_name, receiver_name FROM referral_bonus WHERE receiver_email = '" . $_GET['bonus'] . "' AND used='0';";
			$result = mysql_query($query) or die('Query failed: ' . mysql_error());
			if( mysql_num_rows($result) == 0 )
				return;
			$row = mysql_fetch_assoc($result);
			
			$_SESSION['referral_bonus_email'] = $_GET['bonus'];
			$_SESSION['referral_bonus_sender_name'] = $row['sender_name'];
			$_SESSION['referral_bonus_receiver_name'] = $row['receiver_name'];
		}

		if( !isset($_SESSION['referral_bonus_email']) ||
			!isset($_SESSION['referral_bonus_sender_name']) ||
			!isset($_SESSION['referral_bonus_receiver_name']) ) {
			return;
		}

		// Everything past this point is bonus land.

		// Only show this once. The main game box will then show how many more answers are needed.
		if( $this->get_num_played() == 0 ) {
			echo '<div id="referralBonus">';
				
			echo '
			Hello ' . $_SESSION['referral_bonus_receiver_name'] . '. ' . $_SESSION['referral_bonus_sender_name'] . ' sent you an invitation to try Charitii.<br/>
			As a special welcome bonus, answer ' . self::c_referral_bonus_num_correct_needed . ' puzzles correctly to double your donation amount!';

			echo '</div>';	
		}

	}
	
	function check_notifications_game_box()
	{
		// Check the database if there is anything for this user
		$query = "SELECT id, text from user_notifications where user_id = " . $_SESSION['user_id'] . " and displayed=FALSE LIMIT 1;";
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());
		if( mysql_num_rows($result) == 0 )
			return;
			
		$row = mysql_fetch_assoc($result);
		
		// Display it!		
		
		echo '<div class="mainGameNotification">';
		echo $row['text'];
		echo '</div>';
		
		// Mark the notification as viewed
		$query = "UPDATE user_notifications set displayed=1 where id=".$row['id'].";";
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	}

    function Game() 
	{
		
		// Check if we are running stuff from facebook. Some things are done a little differently...
        if( isset($_POST['fb_sig_in_canvas']) && $_POST['fb_sig_in_canvas'] == "1" )
		{
			$this->is_facebook = TRUE;
		}

		// Fix retarded google bug. Search "selected_noscript" in this file for details.
		if( isset($_POST['selected_noscript']) ) {
			$_POST['selected'] = $_POST['selected_noscript'];
		}

		// Initializing crap
		connect_database();
		$this->check_token();

		$this->set_previous_values();

		
		// Facebook still uses the post value set by javascript to set the charity
		if( $this->is_facebook && isset($_POST['charity_select']) ) {
			$_GET['charity_select'] = $_POST['charity_select'];			
		}
		
		// This get variable is set by the change_charity.php
		// We need to call this BEFORE check_user_state since we download the score
		// from the database using $_SESSION['charity_select'], which must be correct...
		if( isset($_GET['charity_select']) )
		{
			$_GET['charity_select'] = sql_quote($_GET['charity_select']);
			$_SESSION['charity_select'] = $_GET['charity_select'];
		}
		
		$this->check_user_state();

		// Basic status variables
		if( isset($_POST['selected']) && isset($_SESSION['correct_answer_slot']) )
		{
			$_POST['selected'] = sql_quote($_POST['selected']);
			$this->did_submit = true;
			$this->check_answer();
		}
		else
		{
			$this->did_submit = false;
		}
		
		//$this->initial_difficulty_progression_enabled = !isset($_COOKIE['difficulty']) || $_COOKIE['difficulty']==-1;
		$this->initial_difficulty_progression_enabled = false;
	}

}

?>
