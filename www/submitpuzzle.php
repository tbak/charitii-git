<?php
// Copyright 2009, SuperDonate.  All rights reserved.

require 'minify_page_start.php';
require('common.php');
require('game_database.php');

class SubmitPuzzle
{
	public $did_submit;
	public $token_submit;
	public $error_captcha;

	private function check_token()
	{
		// Simple spam check
		// Check that the posted token field matches the token in the session. 
		if( isset($_SESSION['token_submit']) )
		{
			if( !isset($_POST['token_submit']) || $_SESSION['token_submit'] != $_POST['token_submit'] )
			{
				//echo "spammer!";
				// Person probably just hit the reload button on his browser... just act as though
				// nothing happened (no choice was made)
				unset($_POST['token_submit']);
			}
		}
	
		// Generate the token for the next page game page load
		$secret = 'magicalspambustingnumpties';
		$this->token_submit = md5(rand(1, 10000).$secret);
		$_SESSION['token_submit'] = $this->token_submit;
	}

	function insert_puzzle()
	{
		$_POST['submit_clue'] = sql_quote($_POST['submit_clue']);
		$_POST['submit_answer'] = sql_quote($_POST['submit_answer']);
		$_POST['submit_author'] = sql_quote($_POST['submit_author']);

		//
		// Post a new puzzle to the database
		// Use IGNORE so that duplicate clue/answer pairs are silently ignored... but the user still is happy!
		// 
		$enabled = 0;
		$query = "INSERT IGNORE INTO puzzles (clue, answer, source_date, add_date, source_name, difficulty_original, file_name, enabled, answer_strlen) VALUES ('" . $_POST['submit_clue'] . "','" . $_POST['submit_answer'] . "'," . " FROM_UNIXTIME(" . time() . "), FROM_UNIXTIME(" . time() . "), '" . $_POST['submit_author'] . "', 26, 'OL', $enabled, " . strlen($_POST['submit_answer']) . ")";
		//echo $query;
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	}

	private function check_captcha()
	{
		$this->error_captcha = 0;

		if( !isset($_POST['contact_captcha']) || empty($_POST['contact_captcha']) )
		{
			$this->error_captcha = 1;
		}
		else if( !isset($_SESSION['security_code']) || $_POST['contact_captcha'] != $_SESSION['security_code'] )
		{
			$this->error_captcha = 1;
		}

		// TB - Reset the security code session variable so that you can only post once
		unset($_SESSION['security_code']);
	}

	function __construct() 
	{
		// Initializing crap
		connect_database();
		$this->check_token();

		// Basic status variables
		if( isset($_POST['token_submit']) )
		{
			$this->did_submit = true;
			$this->check_captcha();
			if( !$this->error_captcha )
			{
				$this->insert_puzzle();
			}
		}
		else
		{
			$this->did_submit = false;
		}
	}
}

$submit = new SubmitPuzzle();
generic_page_start('submitpuzzle');
?>

<?php
if( $submit->did_submit )
{

	if( !$submit->error_captcha )
	{
		echo '<h2>';
		echo "You just submitted:</h2><br>" . $_POST['submit_clue'] . " = " . $_POST['submit_answer'] . "<br><br>";
		echo "<h2>Thank you for your submission!<br><br><br><br>";
		echo '</h2>';
	}
	else
	{
	    echo '<h2>';
		echo "The security captcha numbers did not match. Please try again.<br><br><br>";
		echo '</h2>'; 
	}
	
}
?>

<h1>Submit New Puzzle</h1>

<br>
<h2>Help make Charitii.com better!</h2>

<p>
You can submit new puzzles to the puzzle database! After a new puzzle has been evaluated and verified, it will be playable by others who visit the site.
</p>

<br>
<h2> Enter your new puzzle here</h2>

<form id="form1" name="form1" action="submitpuzzle.php" method="POST" autocomplete="OFF">  

<table border="0" bgcolor="#ececec" cellspacing="5">

<input type=hidden name="token_submit" value="<?php echo $submit->token_submit ?>" />

<?php
$text_size = 60;
if( $common->device_is_iphone )
{
	$text_size = 40;
}
?>

<tr><td>Clue</td></tr>
<tr><td><input type="text" name="submit_clue" size="<?php echo $text_size; ?>" /></td></tr>

<tr><td>Answer</td></tr>
<tr><td><input type="text" name="submit_answer" size="<?php echo $text_size; ?>" /></td></tr>

<tr><td>Your name(optional)</td></tr>
<tr><td><input type="text" name="submit_author" size="<?php echo $text_size; ?>" /></td></tr>

<tr><td>Please enter these numbers (spam detection)</td></tr>
<tr><td><img src="captchasecurityimage.php" /></td></tr>
<tr><td><input type="text" name="contact_captcha" /></td></tr>

<tr><td><input type="submit" name="submit" value="Submit" /></td></tr>

</table>

</form>

<?php 
generic_page_end('submitpuzzle'); 
$minify_page_is_dynamic = TRUE;
$minify_file_name = __FILE__;
require 'minify_page_end.php';
