<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require 'minify_page_start.php';
require('game_database.php');
require('common.php');

class Totals
{

	function echo_values($is_group)
	{
		echo '<tr><td colspan="6"><h2>';
		if( $is_group ) 
			echo 'Teams';
		else
			echo 'Individuals';
		echo'</h2></td></tr>';

		echo '<tr><td><b>Name</b></td><td></td><td><b>Water<br>(ounces)</b></td><td><b>Wheat<br>(grains)</b></td><td><b>Education<br>(minutes)</b></td><td><b>Rainforest<br>(sq. inches)</b></td><td><b>Total</b></td></tr>';

        $query = "select name, points, charity from user_stats WHERE enabled='1' AND points>'0' AND is_group='" . $is_group . "' order by name asc, charity asc;";
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());

		//echo $query;
		//echo mysql_num_rows($result);

		$cs = 0;
		$first_time = TRUE;
		$total = 0;
		$orig_name = "jjj";
		for( $i = 0; $i < mysql_num_rows($result); $i++ )
		{
			$row = mysql_fetch_array($result);
            
			if( $orig_name != $row[0] )
			{

				// We have skipped to a new name... so make a new row!!!

				if( !$first_time )
				{
					// Finish the current row...
					// Fill in the rest of the columns.
					for( $cs = $cs; $cs < 4; $cs++ )
					{
						echo '<td>--</td>';
					}
	
					// Total
					echo '<td><b>' . number_format($total) . '</b></td>';
				}
				
				// Next row:
				// Name
				echo '<tr><td>' . $row[0] . '</td>';

				// Pretty bar
				echo '<td>|</td>';

				// Reset the charity column
				$cs = 0;

				$first_time = FALSE;
				$total = 0;
				$orig_name = $row[0];
			}

			// Fill in blank columns
			for( $cs = $cs; $cs < $row[2]; $cs++ )
			{
				echo '<td>--</td>';
			}

			// Fill in the actual data! 
			// $cs = $row[2] at this point
			$val = $row[1];
			$total += $val;
			echo '<td>' . number_format($val) . '</td>';
			$cs++;

		}


		// Finish the leftover final row!
		// Finish the current row...
		// Fill in the rest of the columns.
		for( $cs = $cs; $cs < 4; $cs++ )
		{
			echo '<td>--</td>';
		}

		// Total
		echo '<td><b>' . number_format($total) . '</b></td>';

	}

	function __construct() 
	{
		// Initializing crap
		connect_database();
	}
}

$totals = new Totals();
generic_page_start('login_stats');
?>

<h1>Login Stats</h1>

<p>
On this page you can see how much water, food, education and rainforest land have been donated by logged in individuals and teams.
</p>

<p>
<b>Thanks for helping!</b>
</p>

<br/>
<br/>

<table border="0" cellspacing="5">
<?php
$totals->echo_values(FALSE);
?>
</table>
<hr>
<br>
<table border="0" cellspacing="5">
<?php
$totals->echo_values(TRUE);
?>
</table>

<p>
<i>Charitii.com was launched on August 26, 2008</i>
</p>

<?php
generic_page_end();
$minify_page_is_dynamic = TRUE;
$minify_file_name = __FILE__;
require 'minify_page_end.php';
