<?php
// Copyright 2009, SuperDonate.  All rights reserved.
session_start();

/*
http://ajaxian.com/archives/iphone-web-development-tips-and-official-documentation-released
Getting rid of the toolbar
You can scroll down a little, enough to get rid of the toolbar via:
window.onload = function() {
  setTimeout(function(){window.scrollTo(0, 1);}, 100);
}
*/


class Common {
	public $device_is_iphone;
	public $device_is_mobile;
	const c_use_debug_layout_colors = FALSE;
	const c_display_ads = TRUE;
	const c_display_charity_select = FALSE;
	public $did_display_superdonate_ad_on_top;

	const c_num_charities = 6;
	
	const c_ad_network_google = 0;
	const c_ad_network_adtoll = 1;
	const c_ad_network_openx = 2;
	const c_ad_network_adbrite = 3;
	const c_ad_network_adbrite_and_google = 4;
	const c_ad_network_antventure = 5;
	const c_ad_network_clicksor = 6;
	const c_ad_network_widgetbucks = 7;
	const c_ad_network_superrewards = 8;
	const c_ad_network_superdonate = 9;	
	
	// Superrewards/offerpal is not giving good returns... just
	// show the SuperDonate ad I guess. :(
	//const c_ad_network = self::c_ad_network_superrewards;
	const c_ad_network = self::c_ad_network_superdonate;
	
	function minify_page_start()
	{
		require 'minify_config.php';
		require 'minify_groupsSources.php';
	
		require 'Minify/Build.php';
		$jsBuild = new Minify_Build($groupsSources['js']);
		$cssBuild = new Minify_Build($groupsSources['css']);
	
		ob_start(); 
	}

	// Snagged from http://www.hellopump.com/blog/iphone-detect-with-php
	private function is_device_iphone()
	{
		$container = $_SERVER['HTTP_USER_AGENT'];
		
		$useragents = array( 'iPhone','iPod');
		foreach ( $useragents as $useragent ) {
			if (eregi($useragent,$container)) {
				return TRUE;
			}
		}
	
		return FALSE;
	}
	
	
	// Snagged from http://www.andymoore.info/php-to-detect-mobile-phones/
	private function is_device_mobile()
	{
		$this->device_is_mobile = FALSE;
	
		// check if the user agent value claims to be windows but not windows mobile
		if(stristr($_SERVER['HTTP_USER_AGENT'],'windows')&&!stristr($_SERVER['HTTP_USER_AGENT'],'windows ce'))
		{
			$this->device_is_mobile = FALSE;
		}
	
		// check if the user agent gives away any tell tale signs it's a mobile browser
		if(eregi('up.browser|up.link|windows ce|iemobile|mini|mmp|symbian|midp|wap|phone|pocket|mobile|pda|psp',$_SERVER['HTTP_USER_AGENT']))
		{
			$this->device_is_mobile = TRUE;
		}
	
		// check the http accept header to see if wap.wml or wap.xhtml support is claimed
		if(stristr($_SERVER['HTTP_ACCEPT'],'text/vnd.wap.wml')||stristr($_SERVER['HTTP_ACCEPT'],'application/vnd.wap.xhtml+xml'))
		{
			$this->device_is_mobile = TRUE;
		}
	
		// check if there are any tell tales signs it's a mobile device from the _server headers
		if(isset($_SERVER['HTTP_X_WAP_PROFILE'])||isset($_SERVER['HTTP_PROFILE'])||isset($_SERVER['X-OperaMini-Features'])||isset($_SERVER['UA-pixels']))
		{
			$this->device_is_mobile = TRUE;
		}
	
		// build an array with the first four characters from the most common mobile user agents
		$a = array('acs-','alav','alca','amoi','audi','aste','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','opwv','palm','pana','pant','pdxg','phil','play','pluc','port','prox','qtek','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','w3c','wap-','wapa','wapi','wapp','wapr','webc','winw','winw','xda','xda-');
		// check if the first four characters of the current user agent are set as a key in the array
		if(isset($a[substr($_SERVER['HTTP_USER_AGENT'],0,4)]))
		{
			$this->device_is_mobile = TRUE;
		}
	
		$this->device_is_mobile = FALSE;
	}

	function Common()
	{
		// Logging in/out?
		if( isset($_GET['logout']) && $_GET['logout']==1 ) {
			user_logout();
		}
		
		
		// We're not using this
		//$this->device_is_iphone = $this->is_device_iphone();
		//$this->device_is_mobile = $this->is_device_mobile();

		
		// Figure out what ads to display...
		$this->ad_network = self::c_ad_network;
		if( $this->ad_network == self::c_ad_network_adbrite_and_google )
		{
			if( rand(0,1) == 0 )
			{
				$this->ad_network = self::c_ad_network_adbrite;
			}
			else
			{
				$this->ad_network = self::c_ad_network_google;
			}
		}

	}

}
$common = new Common;



function echo_html_head_meta($subcat)
{
	// Use the PHP variable instead
	/*
	<!--[if !IE]>-->
	<link media="only screen and (max-width: 480px)"
	rel="stylesheet" type="text/css" href="wwiphone.css"/>
	<!--<![endif]-->
	*/

// Another symbol to try in title: &#248;  (oe)

echo '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>&raquo; Free Charity Word Game | Word Puzzle to Donate to Charity</title>
	<meta name="description" content="Play word game puzzles to donate to your favorite charity. Donate water, food, education, or save the rainforests." />
	<meta name="keywords" content="charity, free charity, donate, charity game, word game, word puzzle" />
	<meta name="robots" content="all" />
	<link rel="shortcut icon" href="favicon.ico" />
';

	$use_minified_cssjs = TRUE;
	//$use_minified_cssjs = FALSE;
	if( $use_minified_cssjs )
	{

		global $cssBuild;
		echo '<link rel="stylesheet" href="';
		echo $cssBuild->uri('minify_m.php/css');
		echo '" type="text/css" media="all" charset="utf-8" />';
		
		global $jsBuild;
		echo '<script type="text/JavaScript" src="';
		echo $jsBuild->uri('minify_m.php/js');
		echo '"></script>';
	}
	else
	{
		echo '
		<link rel="stylesheet" href="ww.css" type="text/css" media="all" charset="utf-8" />
		<script type="text/JavaScript" src="main.js"></script>  
		<script type="text/JavaScript" src="lib/jquery-1.3.2.min.js"></script>

		<script type="text/JavaScript" src="lib/thickbox.js"></script>
		<link rel="stylesheet" href="lib/thickbox.css" type="text/css" media="screen" />
		';
	}

	// IE6 crap
	//echo '<!--[if lt IE 7]><link rel="stylesheet" href="wwie6.css" type="text/css" charset="utf-8" /><![endif]-->';
	if( strstr($_SERVER["HTTP_USER_AGENT"], "MSIE 6") ) {
		echo '<link rel="stylesheet" type="text/css" href="wwie6.css" />';
	}

	if( strstr($_SERVER["HTTP_USER_AGENT"], "MSIE 7") ||
		strstr($_SERVER["HTTP_USER_AGENT"], "MSIE 8") ) {
		echo '<link rel="stylesheet" type="text/css" href="wwie78.css" />';
	}

	// IE 5.5 crap
	if( strstr($_SERVER["HTTP_USER_AGENT"], "MSIE 5.5")) {
		echo '<link rel="stylesheet" type="text/css" href="wwie5.css" />';
	}

	// IE 7/8 crap
	//if( strstr($_SERVER["HTTP_USER_AGENT"], "MSIE 7") || strstr($_SERVER["HTTP_USER_AGENT"], "MSIE 8") ) {
	//	echo '<link rel="stylesheet" type="text/css" href="wwie78.css" />';
	//}

	//<script type="text/JavaScript" src="ajaxUtil.js"></script>
	//<script type="text/JavaScript" src="ajaxCaller.js"></script>

	global $common;
	
	if( Common::c_use_debug_layout_colors )
	{
		echo '<link rel="stylesheet" type="text/css" href="wwtestlayout.css" />';
	}
	
	if( $common->device_is_iphone )
	{  
		echo '<link rel="stylesheet" type="text/css" href="wwiphone.css" />';
	}

	if( $common->ad_network == Common::c_ad_network_openx )
	{
		echo "
		<!-- Generated by OpenX 2.7.30-beta -->
		<script type='text/javascript' src='http://d1.openx.org/spcjs.php?id=11404&amp;target=_blank'></script>
		";
	}

	echo '</head>';

}

function echo_iphone_ad2()
{
	echo '
	<script type="text/javascript"><!--
	google_ad_client = "pub-6107553673647265";
	/* 234x60, created 8/9/08 */
	google_ad_slot = "5350805814";
	google_ad_width = 234;
	google_ad_height = 60;
	//-->
	</script>
	<script type="text/javascript"
	src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
	</script>
	';
}

function echo_iphone_ad1()
{
	echo '
	<script type="text/javascript"><!--
	google_ad_client = "pub-6107553673647265";
	/* 234x60, created 8/9/08 */
	google_ad_slot = "9006686661";
	google_ad_width = 234;
	google_ad_height = 60;
	//-->
	</script>
	<script type="text/javascript"
	src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
	</script>
	';
}

function echo_peel_away_ad()
{
	global $common;

	if( $common->ad_network == Common::c_ad_network_adtoll )
	{
		echo '<SCRIPT src="http://adserve.adtoll.com/js/at_ag_40306.js" type="text/javascript"></SCRIPT>';
	}
}

function echo_topbottom_clicksor_ad()
{
echo <<<END
	<script type='text/javascript'>
	//interstitial ad
	clicksor_enable_inter = true; clicksor_maxad = 1;	 
	clicksor_hourcap = 1; clicksor_showcap = 5;
	//default banner house ad url 
	clicksor_default_url = 'http://creative.clicksor.com/pub_default_ads/109971/165018_4906_b.htm';
	clicksor_banner_border = '#ffffff'; clicksor_banner_ad_bg = '#ffffff';
	clicksor_banner_link_color = '#000000'; clicksor_banner_text_color = '#666666';
	clicksor_banner_image_banner = true; clicksor_banner_text_banner = true;
	clicksor_layer_border_color = '';
	clicksor_layer_ad_bg = ''; clicksor_layer_ad_link_color = '';
	clicksor_layer_ad_text_color = ''; clicksor_text_link_bg = '';
	clicksor_text_link_color = ''; clicksor_enable_text_link = false;
	</script>
	<script type="text/javascript" src="http://ads.clicksor.com/showAd.php?pid=109971&adtype=1&sid=165018&zone=4906"></script>
	<noscript><a href='http://www.yesads.com'>online marketing</a></noscript>
END;
}

function echo_topbottom_widgetbucks_shopping_ad()
{
	echo '<!-- START CUSTOM WIDGETBUCKS CODE --><div><script src="http://api.widgetbucks.com/script/ads.js?uid=SuSPWZZlz20mh1gJ"></script></div><!-- END CUSTOM WIDGETBUCKS CODE -->';
}

function echo_topbottom_widgetbucks_local_ad()
{
	echo '<!-- START CUSTOM WIDGETBUCKS CODE --><div><script src="http://api.widgetbucks.com/script/ads.js?uid=uFyVi8ox7oRdlmAE"></script></div><!-- END CUSTOM WIDGETBUCKS CODE -->';
}

function echo_topbottom_antventure_ad()
{
	echo'
	<!-- BEGIN STANDARD TAG - 728 x 90 - ROS: Run-of-site - DO NOT MODIFY -->
	<IFRAME FRAMEBORDER=0 MARGINWIDTH=0 MARGINHEIGHT=0 SCROLLING=NO WIDTH=728 HEIGHT=90 SRC="http://ad.antventure.com/st?ad_type=iframe&ad_size=728x90&section=672544"></IFRAME>
	<!-- END TAG -->
		';
}

function echo_topbottom_amazon_ad()
{
	if( rand(0,1) == 0 )
	{
		// Omakase
		echo '<script type="text/javascript"><!--
				amazon_ad_tag = "donatewater_charity-20"; amazon_ad_width = "728"; amazon_ad_height = "90";//--></script>
				<script type="text/javascript" src="http://www.assoc-amazon.com/s/ads.js"></script>';
	}
	else
	{
		// Deals
        echo '<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab" id="Player_4eda8bda-f21d-487e-97bd-18d5dc86cdad"  WIDTH="728px" HEIGHT="90px"> <PARAM NAME="movie" VALUE="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8009%2F4eda8bda-f21d-487e-97bd-18d5dc86cdad&Operation=GetDisplayTemplate"><PARAM NAME="quality" VALUE="high"><PARAM NAME="bgcolor" VALUE="#FFFFFF"><PARAM NAME="allowscriptaccess" VALUE="always"><embed src="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8009%2F4eda8bda-f21d-487e-97bd-18d5dc86cdad&Operation=GetDisplayTemplate" id="Player_4eda8bda-f21d-487e-97bd-18d5dc86cdad" quality="high" bgcolor="#ffffff" name="Player_4eda8bda-f21d-487e-97bd-18d5dc86cdad" allowscriptaccess="always"  type="application/x-shockwave-flash" align="middle" height="90px" width="728px"></embed></OBJECT> <NOSCRIPT><A HREF="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8009%2F4eda8bda-f21d-487e-97bd-18d5dc86cdad&Operation=NoScript">Amazon.com Widgets</A></NOSCRIPT>';
	}

	return;




	// This can go either on the top or bottom
	// Adsense does not allow the same ad on the same page, so it's more restrictive.
	if( rand(0,1) == 0 )
	{
		echo '
		<script type="text/javascript"><!--
		amazon_ad_tag = "donatewater_bestseller-20"; amazon_ad_width = "728"; amazon_ad_height = "90"; amazon_ad_logo = "hide";//--></script>
		<script type="text/javascript" src="http://www.assoc-amazon.com/s/ads.js"></script>
		';
	}
	else
	{
		$tags[0] = 'donatewater_water-20';
		$tags[1] = 'donatewater_crossword-20';

		$titles[0] = 'Hand-Picked Charitii.com Selections - You Can Help By Learning More';
		$titles[1] = 'Exercise Your Brain Every Day';

		$category_choice = rand(0,1);


		// TB TODO - I think this needs to be different from the top ad...?
		echo '
		<script type="text/javascript"><!--
		amazon_ad_tag="' . $tags[$category_choice] . '"; 
		amazon_ad_width="728"; 
		amazon_ad_height="90"; 
		amazon_color_background="EFEFCC"; 
		amazon_color_border="2F07A4"; 
		amazon_color_logo="FFFFFF"; 
		amazon_color_link="A43907"; 
		amazon_ad_logo="hide"; 
		amazon_ad_title="' . $titles[$category_choice] . '"; //--></script>
		<script type="text/javascript" src="http://www.assoc-amazon.com/s/asw.js"></script>
		';
	}

}

function echo_topbottom_google_ad()
{
	
	// Adsense
	echo '
	<!-- TB TEST AdSense -->
	<script type="text/javascript"><!--
	google_ad_client = "pub-6107553673647265";
	/* 728x90, created 7/24/08 */
	google_ad_slot = "9035754869";
	google_ad_width = 728;
	google_ad_height = 90;
	//-->
	</script>
	<script type="text/javascript"
	src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
	</script>
	<!-- END TB TEST AdSense -->
	';
}

function echo_topbottom_adbrite_ad()
{

echo <<<END
	<!-- Begin: AdBrite, Generated: 2009-06-05 11:15:06  -->
	<script type="text/javascript">
	var AdBrite_Title_Color = '0000FF';
	var AdBrite_Text_Color = '000000';
	var AdBrite_Background_Color = 'FFFFFF';
	var AdBrite_Border_Color = 'CCCCCC';
	var AdBrite_URL_Color = '008000';
	try{var AdBrite_Iframe=window.top!=window.self?2:1;var AdBrite_Referrer=document.referrer==''?document.location:document.referrer;AdBrite_Referrer=encodeURIComponent(AdBrite_Referrer);}catch(e){var AdBrite_Iframe='';var AdBrite_Referrer='';}
	</script>
	<span style="white-space:nowrap;"><script type="text/javascript">document.write(String.fromCharCode(60,83,67,82,73,80,84));document.write(' src="http://ads.adbrite.com/mb/text_group.php?sid=1198370&zs=3732385f3930&ifr='+AdBrite_Iframe+'&ref='+AdBrite_Referrer+'" type="text/javascript">');document.write(String.fromCharCode(60,47,83,67,82,73,80,84,62));</script>
	<a target="_top" href="http://www.adbrite.com/mb/commerce/purchase_form.php?opid=1198370&afsid=1"><img src="http://files.adbrite.com/mb/images/adbrite-your-ad-here-leaderboard.gif" style="background-color:#CCCCCC;border:none;padding:0;margin:0;" alt="Your Ad Here" width="14" height="90" border="0" /></a></span>
	<!-- End: AdBrite -->
END;
	
}

function echo_topbottom_adtoll_ad()
{
	echo '
    <!-- START ADTOLL.COM CODE V1.0 -->
	<STYLE>
	A.at_adv_here_40307, A.at_pow_by_40307 {font-family: Arial,Sans-Serif; font-size: 10px; font-style: normal; font-weight: normal; font-variant: normal; text-transform: none; color: #000099; text-decoration: none; }
	A.at_adv_here_40307:hover, A.at_pow_by_40307:hover { color: #0000FF; text-decoration: underline; }
	</STYLE>
	<SCRIPT type="text/javascript">
	adtoll_see_your_ad_here = 1;
	adtoll_your_text = "Advertise here";
	adtoll_show_powered_by = 1;
	adtoll_auto_play = "N";
	</SCRIPT>
	<SCRIPT src="http://adserve.adtoll.com/js/at_ag_40307.js" type="text/javascript"></SCRIPT>
	<!-- END ADTOLL.COM CODE V1.0 -->';

}

function echo_topbottom_openx_ad()
{
	echo "
	<script type='text/javascript'><!--// <![CDATA[
    /* [id41780] charitii.com - Default */
    OA_show(41780);
	// ]]> --></script><noscript><a target='_blank' href='http://d1.openx.org/ck.php?n=6590073'><img border='0' alt='' src='http://d1.openx.org/avw.php?zoneid=41780&amp;n=6590073' /></a></noscript>
	";
}

function echo_topbottom_superdonate_ad()
{
	$ad_choice = rand(0,2);
	//$ad_choice = 0;
	if( $ad_choice == 0 )
	{
		echo '
			<center>
            <a href="http://www.superdonate.org" style="display:block; border:1px solid #000000;">
			<img src="http://media.superdonate.org/banners/superdonate_728_90.png" alt="Donate your idle computer time to charity" />
			</a>			
			</center>
			<em>From the makers of Charitii.com</em><br/>
			';
	}
	else if( $ad_choice == 1 )
	{
		echo '
			<center>
            <a href="http://www.superdonate.org" style="display:block; border:1px solid #000000;">
			<img src="http://media.superdonate.org/banners/superdonate_728_90x.png" alt="Donate your idle computer time to charity" />
			</a>			
			</center>
			<em>From the makers of Charitii.com</em><br/>
			';
	}
	else
	{
		echo '
			<center>
            <em>From the makers of Charitii.com</em><br/>
            <a href="http://www.superdonate.org">
			<img src="http://media.superdonate.org/banners/superdonate_728_90y.png" alt="free donation" />
			</a>
            </center>
			';
	}

}

function echo_top_ad()
{
	// Top ad sucks.
	return;
	
	global $common;
	if( $common->ad_network == Common::c_ad_network_superrewards ) {
		return;
	}	

	if( !Common::c_display_ads ) { echo '<div id="mainAdTop"></div>'; return; }

	if( $common->device_is_iphone )
	{
		echo '<div id="mainAdTop">';
		echo_iphone_ad1();
		echo '</div>';
		return;
	}

	echo '<div id="mainAdTop">';

	// Screw the amazon ads
	// 
	// From 0 to 2 are the google ad + 2 superdonate ads.
	// Once superdonate is running, switch the ad_choice logic!
    //$ad_choice = 0;
	$common->did_display_superdonate_ad_on_top = FALSE;
	$ad_choice = 0;

	// Rare: display superdonate ad
	if( rand(0,100) < 50 )
	//if( TRUE )
	{
		$ad_choice = 1;
	}

    if( $ad_choice == 0 )
	{

		if(  $common->ad_network == Common::c_ad_network_widgetbucks )
		{
			echo_topbottom_widgetbucks_local_ad();
		}
		else if( $common->ad_network == Common::c_ad_network_clicksor ) 
		{
			echo_topbottom_clicksor_ad();
		}
		else if( $common->ad_network == Common::c_ad_network_antventure ) 
		{
			echo_topbottom_antventure_ad();
		}
		else if( $common->ad_network == Common::c_ad_network_adtoll )
		{
			echo_topbottom_adtoll_ad();
		}
		else if( $common->ad_network == Common::c_ad_network_openx )
		{
			echo_topbottom_openx_ad();
		}
		else if( $common->ad_network == Common::c_ad_network_adbrite )
		{
			echo_topbottom_adbrite_ad();
		}
		else {
			echo_topbottom_google_ad();
		}
		
	}
	else if( $ad_choice == 1 )
	{
		//$common->did_display_superdonate_ad_on_top = TRUE;
		echo_topbottom_superdonate_ad();
	}
	else
	{
		echo_topbottom_amazon_ad();
	}

	echo '</div>';
}

function echo_bottom_ad()
{
    // No more ads
    return;

	global $common;
		
	// Only show superrewards ads while playing the game
	if( $common->ad_network == Common::c_ad_network_superrewards ) {
		$uri = $_SERVER['REQUEST_URI'];
		$pos = strpos($uri,"?");
		if( $pos != false ) {
			$uri = substr($uri, 0, $pos);
		}		
		
		if( $uri != "/" &&
			$uri != "/index.php" &&
			$uri != "/puz3/" &&
			$uri != "/puz3/index.php" ) {
			
			return;
		}
		
	}

	if( !Common::c_display_ads ) { echo '<div id="mainAdBottom"></div>'; return; }

	// Special small iphone ad
   	if( $common->device_is_iphone )
	{
		echo '<div id="mainAdBottom">';
		echo_iphone_ad2();
		echo '</div>';
		return;
	}

	echo '<div id="mainAdBottom">';

	$ad_choice = 0;

	// Only display the bottom superdonate ad if it was not displayed on the top...
	// if( $common->did_display_superdonate_ad_on_top == FALSE && rand(0,100) < 10 )
	//if( TRUE )
	if( rand(0,100) < 20 )
	{
		$ad_choice = 1;
	}
	
	if( $common->ad_network == Common::c_ad_network_superdonate ) {
		$ad_choice = 1;
	}
		
	
	
	if( $ad_choice == 0 )
	{		
		if( $common->ad_network == Common::c_ad_network_superrewards )
		{
			global $game;
			
			if( rand(0,100) < 50 ) {
				echo 'Completed bonus offers will be added to your score.';
			}
			else {
				echo 'Completed bonus offers are a source of money for your favorite charity.';
			}
			echo '<div id="superRewardsBox">&nbsp;</div> <script type="text/javascript">
			ajaxLoadOffer("' . $game->get_curr_charity_units() . '", "' . $game->get_curr_charity_name() . '", ' . $_SESSION['user_id'] . ', ' . $_SESSION['charity_select'] . ' );
			</script>';
		}
		else if(  $common->ad_network == Common::c_ad_network_widgetbucks )
		{
			echo "The ads on this page help support the charity that you have selected.<br/><br/>";
			echo_topbottom_widgetbucks_shopping_ad();
		}
		else if( $common->ad_network == Common::c_ad_network_clicksor ) 
		{
			echo_topbottom_clicksor_ad();
		}
		else if( $common->ad_network == Common::c_ad_network_antventure ) 
		{
			echo_topbottom_antventure_ad();
		}
		else if( $common->ad_network == Common::c_ad_network_adtoll )
		{
			echo_topbottom_adtoll_ad();
		}
		else if( $common->ad_network == Common::c_ad_network_openx )
		{
			echo_topbottom_openx_ad();
		}
		else if( $common->ad_network == Common::c_ad_network_adbrite )
		{
			echo_topbottom_adbrite_ad();
		}		
		else
		{
			echo_topbottom_google_ad();
		}
		
	}
	else if( $ad_choice == 1 )
	{
		echo_topbottom_superdonate_ad();
    }
	else if( $ad_choice == 2 )
	{
		// Amazon
		echo_topbottom_amazon_ad();
	}
	else
	{
		echo "<div id='vu_ytplayer_vjVQa1PpcFNpWpZ5zQZ94rZFYlWmpko79WLn_ld0l8w='><a href='http://www.youtube.com/browse'>Watch the latest videos on YouTube.com</a></div><script type='text/javascript' src='http://www.youtube.com/watch_custom_player?id=vjVQa1PpcFNpWpZ5zQZ94rZFYlWmpko79WLn_ld0l8w='></script>";
	}

	echo '</div>';
	
}

function echo_main_game_ad_top()
{
	// TB TEST - Try a bigger ad!
	if( 1 )
	{
		echo
		'
		<script type="text/javascript"><!--
		google_ad_client = "pub-6107553673647265";
		/* CH INT TOP 468x60, created 9/8/08 */
		google_ad_slot = "0350824960";
		google_ad_width = 468;
		google_ad_height = 60;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
		';
		return;
	}
}

function echo_main_game_ad_bottom()
{
	// TB TEST - Try a bigger ad!
	if( 1 )
	{
		echo
		'
		<script type="text/javascript"><!--
		google_ad_client = "pub-6107553673647265";
		/* CH INT BOTT 468x60, created 9/8/08 */
		google_ad_slot = "2587769891";
		google_ad_width = 468;
		google_ad_height = 60;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
		';
		return;
	}
}



function echo_main_game_ad()
{
	if( !Common::c_display_ads ) { echo '<div id="mainGameAd"></div>'; return; }

	// TB TEST - Put ad here?
	global $common;
   	if( $common->device_is_iphone )
	{
		echo '<div id="mainGameAd">';
		echo_iphone_ad1();
		echo '</div>';
		return;
	}

	// Only adsense for now...
	echo '
	<div id="mainGameAd">
	<script type="text/javascript"><!--
	google_ad_client = "pub-6107553673647265";
	/* 468x15, created 8/6/08 */
	google_ad_slot = "2081278122";
	google_ad_width = 468;
	google_ad_height = 15;
	//-->
	</script>
	<script type="text/javascript"
	src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
	</script>
	</div>
	';
}

function echo_square_ad()
{
	// These ads are a mess...
	return;

	global $common;

	if( $common->ad_network == Common::c_ad_network_adtoll )
	{
		echo '
			<!-- START ADTOLL.COM CODE V1.0 -->
			<STYLE>
			A.at_adv_here_40308, A.at_pow_by_40308 {font-family: Arial; font-size: 10px; font-style: normal; font-weight: normal; font-variant: normal; text-transform: none; color: #000099; text-decoration: none; }
			A.at_adv_here_40308:hover, A.at_pow_by_40308:hover { color: #0000FF; text-decoration: underline; }
			</STYLE>
			<SCRIPT type="text/javascript">
			adtoll_see_your_ad_here = 1;
			adtoll_your_text = "Advertise here";
			adtoll_show_powered_by = 1;
			adtoll_auto_play = "N";
			</SCRIPT>
			<SCRIPT src="http://adserve.adtoll.com/js/at_ag_40308.js" type="text/javascript"></SCRIPT>
			<!-- END ADTOLL.COM CODE V1.0 -->';

	}
	else if( $common->ad_network == Common::c_ad_network_adbrite )
	{
		echo <<<END
		<!-- Begin: AdBrite, Generated: 2009-06-05 10:48:58  -->
		<script type="text/javascript">
		var AdBrite_Title_Color = '0000FF';
		var AdBrite_Text_Color = '000000';
		var AdBrite_Background_Color = 'FFFFFF';
		var AdBrite_Border_Color = 'CCCCCC';
		var AdBrite_URL_Color = '008000';
		try{var AdBrite_Iframe=window.top!=window.self?2:1;var AdBrite_Referrer=document.referrer==''?document.location:document.referrer;AdBrite_Referrer=encodeURIComponent(AdBrite_Referrer);}catch(e){var AdBrite_Iframe='';var AdBrite_Referrer='';}
		</script>
		<script type="text/javascript">document.write(String.fromCharCode(60,83,67,82,73,80,84));document.write(' src="http://ads.adbrite.com/mb/text_group.php?sid=1203611&zs=3330305f323530&ifr='+AdBrite_Iframe+'&ref='+AdBrite_Referrer+'" type="text/javascript">');document.write(String.fromCharCode(60,47,83,67,82,73,80,84,62));</script>
		<div><a target="_top" href="http://www.adbrite.com/mb/commerce/purchase_form.php?opid=1203611&afsid=1" style="font-weight:bold;font-family:Arial;font-size:13px;">Your Ad Here</a></div>
		<!-- End: AdBrite -->   		
END;
	}
	else if( $common->ad_network == Common::c_ad_network_openx )
	{
		echo "
		<script type='text/javascript'><!--// <![CDATA[
		/* [id41781] charitii.com - Default */
		OA_show(41781);
		// ]]> --></script><noscript><a target='_blank' href='http://d1.openx.org/ck.php?n=0ff7cc9'><img border='0' alt='' src='http://d1.openx.org/avw.php?zoneid=41781&amp;n=0ff7cc9' /></a></noscript>
		";
	}
	else 
	{
		echo '
		<script type="text/javascript"><!--
		google_ad_client = "pub-6107553673647265";
		/* CH 200x200, created 9/8/08 */
		google_ad_slot = "3330200241";
		google_ad_width = 200;
		google_ad_height = 200;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
		';
	}

}

function echo_ultra_left_ad()
{
	// Screw this thing
	return;

	// TB TEMP -- delete me when the site is live!
	/*
	echo '<div id="workInProgress">';
	echo 'Work in Progress<br>';
	echo 'Site is not live<br>';
	echo '</div>';
	*/

	// Ajax yummyness
	// ...is not working... :(
	//echo '<div id="ultraLeft"></div>';
	/*
	echo '
		<div id="ultraLeft">
		<script type="text/javascript"><!--
		loadAds();
		//-->
        </script>
		</div>
	';
	return;
	*/
	

	if( !Common::c_display_ads ) { echo '<div id="ultraLeft"></div>'; return; }

	echo '<div id="ultraLeft">';

	// TEMP!!!
	// Use test_carousel.php instead to test POST/GET double posting...
	// For now just disable the carousel widget. :(
	//echo '<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab" id="Player_2da02d7d-154f-4164-a007-0034d224621a"  WIDTH="160px" HEIGHT="600px"> <PARAM NAME="movie" VALUE="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8010%2F2da02d7d-154f-4164-a007-0034d224621a&Operation=GetDisplayTemplate"><PARAM NAME="quality" VALUE="high"><PARAM NAME="bgcolor" VALUE="#FFFFFF"><PARAM NAME="allowscriptaccess" VALUE="always"><embed src="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8010%2F2da02d7d-154f-4164-a007-0034d224621a&Operation=GetDisplayTemplate" id="Player_2da02d7d-154f-4164-a007-0034d224621a" quality="high" bgcolor="#ffffff" name="Player_2da02d7d-154f-4164-a007-0034d224621a" allowscriptaccess="always"  type="application/x-shockwave-flash" align="middle" height="600px" width="160px"></embed></OBJECT> <NOSCRIPT><A HREF="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8010%2F2da02d7d-154f-4164-a007-0034d224621a&Operation=NoScript">Amazon.com Widgets</A></NOSCRIPT>';
	//echo '</div>';

	// TB TEMP - Disable trashy youtube videos for now...?
	// Display amazon ads only -- we already have 3 googles on screen (top,bot,square)

    $ad_choice = 1;
	// Rare: display superdonate ad
	if( rand(0,100) < 10 )
	{
		//$ad_choice = rand(0,1);
		$ad_choice = 0;
	}

	if( $ad_choice == 0 )
	{
		echo '<a href="http://www.superdonate.org"><img src="http://media.superdonate.org/banners/charitii_superdonate_160_600.png" /></a>';
	}
	else if( $ad_choice == 1 )
	{
		// Adsense
	
		echo '
		<script type="text/javascript"><!--
		google_ad_client = "pub-6107553673647265";
		/* 160x600, created 8/6/08 */
		google_ad_slot = "4872558641";
		google_ad_width = 160;
		google_ad_height = 600;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
		';
	}
	else if( $ad_choice == 2 )
	{

		$amazon_ad = rand(0,3);

		switch( $amazon_ad )
		{
		case 0:
			// Deals
			echo '<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab" id="Player_44cf214e-d4ab-4239-9035-9faa524abdf4"  WIDTH="160px" HEIGHT="400px"> <PARAM NAME="movie" VALUE="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8009%2F44cf214e-d4ab-4239-9035-9faa524abdf4&Operation=GetDisplayTemplate"><PARAM NAME="quality" VALUE="high"><PARAM NAME="bgcolor" VALUE="#FFFFFF"><PARAM NAME="allowscriptaccess" VALUE="always"><embed src="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8009%2F44cf214e-d4ab-4239-9035-9faa524abdf4&Operation=GetDisplayTemplate" id="Player_44cf214e-d4ab-4239-9035-9faa524abdf4" quality="high" bgcolor="#ffffff" name="Player_44cf214e-d4ab-4239-9035-9faa524abdf4" allowscriptaccess="always"  type="application/x-shockwave-flash" align="middle" height="400px" width="160px"></embed></OBJECT> <NOSCRIPT><A HREF="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8009%2F44cf214e-d4ab-4239-9035-9faa524abdf4&Operation=NoScript">Amazon.com Widgets</A></NOSCRIPT>';
			break;
		case 1:
			// Omakase
			echo '<script type="text/javascript"><!--
					amazon_ad_tag = "donatewater_charity-20"; amazon_ad_width = "160"; amazon_ad_height = "600";//--></script>
					<script type="text/javascript" src="http://www.assoc-amazon.com/s/ads.js"></script>';
			break;
		case 2:
			// Search
            echo '<SCRIPT charset="utf-8" type="text/javascript" src="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822/US/cross02-20/8002/daa0d2fe-d973-4f93-9ce7-a9c669b5b144"> </SCRIPT> <NOSCRIPT><A HREF="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8002%2Fdaa0d2fe-d973-4f93-9ce7-a9c669b5b144&Operation=NoScript">Amazon.com Widgets</A></NOSCRIPT>';
			break;
		case 3:
			// product cloud
            echo '<SCRIPT charset="utf-8" type="text/javascript" src="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822/US/cross02-20/8006/994ea636-32bb-4303-9807-9a515667459b"> </SCRIPT> <NOSCRIPT><A HREF="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8006%2F994ea636-32bb-4303-9807-9a515667459b&Operation=NoScript">Amazon.com Widgets</A></NOSCRIPT>';
			break;
		}
		

		/*
		// TB TEMP - The carousel is double posting GET/POST = screws up IE sessions!!!
		// (wtf?)
		$amazon_ad = 1;

		if( $amazon_ad == 0 )
		{
			// Bestseller carousel
            echo '<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab" id="Player_2da02d7d-154f-4164-a007-0034d224621a"  WIDTH="160px" HEIGHT="600px"> <PARAM NAME="movie" VALUE="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8010%2F2da02d7d-154f-4164-a007-0034d224621a&Operation=GetDisplayTemplate"><PARAM NAME="quality" VALUE="high"><PARAM NAME="bgcolor" VALUE="#FFFFFF"><PARAM NAME="allowscriptaccess" VALUE="always"><embed src="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8010%2F2da02d7d-154f-4164-a007-0034d224621a&Operation=GetDisplayTemplate" id="Player_2da02d7d-154f-4164-a007-0034d224621a" quality="high" bgcolor="#ffffff" name="Player_2da02d7d-154f-4164-a007-0034d224621a" allowscriptaccess="always"  type="application/x-shockwave-flash" align="middle" height="600px" width="160px"></embed></OBJECT> <NOSCRIPT><A HREF="http://ws.amazon.com/widgets/q?ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fcross02-20%2F8010%2F2da02d7d-154f-4164-a007-0034d224621a&Operation=NoScript">Amazon.com Widgets</A></NOSCRIPT>';
		}
		else
		{
			$tags[0] = 'donatewater_water-20';
			$tags[1] = 'donatewater_crossword-20';

			$titles[0] = 'Hand-Picked Charitii.com Selections';
			$titles[1] = 'Exercise Your Brain Every Day';
	
			$category_choice = rand(0,1);

			echo '
			<script type="text/javascript"><!--
			amazon_ad_tag="' . $tags[$category_choice] . '"; 
			amazon_ad_width="160"; 
			amazon_ad_height="600"; 
			amazon_color_background="EFEFCC"; 
			amazon_color_border="2F07A4"; 
			amazon_color_logo="FFFFFF"; 
			amazon_color_link="A43907"; 
			amazon_ad_logo="hide"; 
			amazon_ad_title="' . $titles[$category_choice] . '"; //--></script>
			<script type="text/javascript" src="http://www.assoc-amazon.com/s/asw.js"></script>
			';
		}
		*/
	}
	else
	{
		echo "<div id='vu_ytplayer_vjVQa1PpcFNpWpZ5zQZ94jmGnD-mgWhU43MqkkQgYWA='><a href='http://www.youtube.com/browse'>Watch the latest videos on YouTube.com</a></div><script type='text/javascript' src='http://www.youtube.com/watch_custom_player?id=vjVQa1PpcFNpWpZ5zQZ94jmGnD-mgWhU43MqkkQgYWA='></script>";
	}
	
	echo '</div>';


	// TB TEMP -- delete me when the site is live!
	/*
	echo '<div id="workInProgress">';
	echo 'Work in Progress<br>';
	echo 'Site is not live<br>';
	echo '</div>';
	*/
}

function sql_quote( $value )
{
	if( get_magic_quotes_gpc() )
	{
		  $value = stripslashes( $value );
	}
	//check if this function exists
	if( function_exists( "mysql_real_escape_string" ) )
	{
		  $value = mysql_real_escape_string( $value );
	}
	//for PHP version < 4.3.0 use addslashes
	else
	{
		  $value = addslashes( $value );
	}
	return $value;
}

function echo_search_box_ad()
{
	if( !Common::c_display_ads ) return;

	echo '
	<form action="http://www.google.com/cse" id="cse-search-box">
	  <div>
		<input type="hidden" name="cx" value="partner-pub-6107553673647265:t8eorhpe78o" />
		<input type="hidden" name="ie" value="ISO-8859-1" />
		<input type="text" name="q" size="31" />
		<input type="submit" name="sa" value="Search" />
	  </div>
	</form>
	<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=cse-search-box&amp;lang=en"></script>
	';

/*
	echo '
    <form action="http://www.google.com/cse" id="cse-search-box" target="_blank">
	<div>
    <input type="hidden" name="cx" value="partner-pub-6107553673647265:ak73sw-9wtt" />
    <input type="hidden" name="ie" value="ISO-8859-1" />
    <input type="text" name="q" size="31" />
    <input type="submit" name="sa" value="Search" />
	</div>
	</form>
	<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=cse-search-box&amp;lang=en"></script>
	';
*/
}

function dialog_start()
{
	
/*
if( isset($_GET['logout']) )
{
	unset( $_SESSION['login_individual'] );
	unset( $_SESSION['login_group'] );
}

if( isset($_SESSION['login_individual']) || isset($_SESSION['login_group']) )
{
	echo '<div id="headerLoginInfo">';
	echo '<hr/>';
	if( isset($_SESSION['login_individual']) )
	{
		echo 'You are logged in as ' . $_SESSION['login_individual'];
		echo ' (' . $_SESSION['login_individual_points0'] . ' / ' . $_SESSION['login_individual_points1'] . ' / ' . $_SESSION['login_individual_points2'] . ' / ' . $_SESSION['login_individual_points3'] . ' points)';
	}
	if( isset($_SESSION['login_group']) )
	{
		if( isset($_SESSION['login_individual']) )
		{
			echo '<br/>';
		}
		echo 'Your team login is ' . $_SESSION['login_group'];
		echo ' (' . $_SESSION['login_group_points0'] . ' / ' . $_SESSION['login_group_points1'] . ' / ' . $_SESSION['login_group_points2'] . ' / ' . $_SESSION['login_group_points3'] . ' points)';
	}
	echo '<br/><a href="login_stats.php">view global stats</a> | <a href="index.php?logout=1">logout</a>';
	echo '<hr/>';
	echo '</div>';
}
*/
	
}

function dialog_end()
{
	/*
	echo '
	 	 	<!-- Dialog test -->
		</div>
 <div class="b"><div></div></div>
</div>
		';
	*/
}

function generic_page_start( $body_id )
{
	echo_html_head_meta($body_id);

	echo '
		<body id="' . $body_id . '">
		<div id="container">
		<div id="content">
		';

	include('header.php');

	//<div id="centerEverything">
	//<div id="ultraRight">

	echo '
	<div id="wrapperContentBox">
	';

    dialog_start();

	echo_top_ad();
	echo '<br/>';

	echo '
	<table align="center" cellpadding="0" cellspacing="0"><tr><td>
	<div id="mainGenericContent">
	';
}

function generic_page_end()
{

	echo '
	</div> <!--mainGenericContent-->
	</td></tr></table>
	';

	echo_bottom_ad();

	dialog_end();

	echo '
	</div><!--wrapperContentBox-->
	';

    include('footer.php');

	echo_final_script_code();

	echo '
	</body>
	</html>
	';
}

function echo_google_analytics()
{
 
// Async google analytics 
// http://www.stevesouders.com/blog/2009/12/01/google-analytics-goes-async/ 
echo <<<END
<script type="text/javascript" language="JavaScript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-5390483-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script');
ga.src = ('https:' == document.location.protocol ?
    'https://ssl' : 'http://www') +
    '.google-analytics.com/ga.js';
ga.setAttribute('async', 'true');
document.documentElement.firstChild.appendChild(ga);
})();
</script>
END;


// Old tracker code		
/*
echo <<<END
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5390483-1");
pageTracker._trackPageview();
</script>
END;
*/


}

function echo_final_script_code() 
{
	// TB JQUERY SLOW
	//require('facebook_connect.php');
	// This is slowing down the jquery document ready func...? So add this with AJAX...
	// Or just screw it???
	/*		
	echo '
	<div id="ajax_facebook_connect">
	</div>
	<script type="text/JavaScript">
	$(function(){
		$("#ajax_facebook_connect").load("facebook_connect.php");
	});
	</script>';
	*/

	echo_google_analytics();
}

function apply_round_borders($location)
{
	// $location
	// 0 = game
	// 1 = guide
	// 2 = faq

	//return;

	echo '
		<script type="text/javascript">
		<!-- 
		applyRoundBorders( ' . $location . ');
		//-->
		</script>
		';
}

function draw_chart_for_months($sizex, $sizey)
{		
	// Select questions transactions joined with completed offers transactions!
	//$query = "select date_format(ask_date, '%M, %Y'), sum(num_correct), charity_select, date_format(ask_date, '%b') from transactions_daily where 1 GROUP BY 1, charity_select ORDER BY ask_date asc, charity_select asc;";
	$query = "select date_format(ask_date, '%M, %Y'), sum(num_correct), transactions_daily.charity_select, date_format(ask_date, '%b'), sum(offers_daily.amount) from transactions_daily LEFT JOIN offers_daily ON transactions_daily.ask_date = offers_daily.date AND transactions_daily.charity_select = offers_daily.charity_select where 1 GROUP BY 1, charity_select ORDER BY ask_date asc, charity_select asc;";
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	
	//$str_values[0] = $str_values[1] = $str_values[2] = $str_values[3] = $str_values[4] = "";
	//$day[0] = $day[1] = $day[2] = $day[3] = $day[4] = 0;
	for( $i = 0; $i < Common::c_num_charities; $i++ ) {		
		$str_values[$i] = "";
		$day[$i] = 0;	
	}

	$str_labels = "";
	$max_value = 0;
	//$cs = -1;
	$last_date = 0;
	$orig_date = 'foo';
	$month_num = 0;
	for( $i = 0; $i < mysql_num_rows($result); $i++ )
	{
		$row = mysql_fetch_array($result);

		//if( $row[2] <= $cs )
		if( $row[0] != $orig_date )
		{
			$orig_date = $row[0];
			
			// Day is done... write it out!
			for( $c = 0; $c < Common::c_num_charities; $c++ )
			{
				
				if( $str_values[$c] != "" )
				{
					$str_values[$c] .= ',';
				}
				$str_values[$c] .= $day[$c];
			}

			// Show every other label + first and last months
			if( ($month_num == 0) || ($month_num % 2 == 0) || ($month_num == mysql_num_rows($result)-1) ) {
				$str_labels .= $last_date . '|';				
			}
			else {
				$str_labels .= '|';
			}
			$month_num++;

			//$day[0] = $day[1] = $day[2] = $day[3] = $day[4] = 0;
			for( $k = 0; $k < Common::c_num_charities; $k++ ) {			
				$day[$k] = 0;	
			}
		}

		// Now also add in the amount earned by offers to get the true value
		$val = $row[1] * Game::c_ounces_earned_per_correct;
		$val += $row[4];
		
		$day[$row[2]] = $val;
		$last_date = $row[3];

		if( $val > $max_value )
			$max_value = $val;

		//$cs = $row[2];
	}

	$max_value = $max_value + 100 - ($max_value % 100);
	//echo $max_value;

	//&amp;chm=D,0000FF,0,0,2,1

	// For some absurd reason facebook is inserting underscores between newlines for this URL...
	// So I need to shove it all in a single line... wtf???

	// Note: CARE (id=1) is no longer a part of this
	echo '
	<img src="http://chart.apis.google.com/chart?chf=bg,s,00000000&amp;chs=' . $sizex . 'x' . $sizey . '&amp;chtt=Donations+All+Months&amp;chd=t:' . $str_values[0] . '|' . $str_values[5] . '|' . $str_values[3] . '|' . $str_values[2] . '|' . $str_values[4] . '&amp;chds=0,' . $max_value . '&amp;chdl=Charity:water|Invisible Youth Network|Nature Conservancy|Oaktree Foundation|Philippine Aid Society&amp;chco=0000FF,FF0000,00FF00,FF00FF,333333&amp&amp;cht=lc&amp;chxt=x,y,x&amp;chxl=0:|' . $str_labels . '1:|0|' . $max_value/2 . '|' . $max_value . '|2:||Month"alt="Charitii donation chart" />
	';

}





function draw_chart_for_current_month($sizex, $sizey)
{
	$year = date("Y");
	$month = date("m");

	$start_date_str = $year . '-' . $month . '-01';
	$end_date_str = $year . '-' . $month . '-31';

	// Select questions transactions joined with completed offers transactions!		
	//$query = "select date_format(ask_date, '%d'), SUM(num_correct), charity_select from transactions_daily WHERE ask_date >= '" . $start_date_str . "' AND ask_date <= '" . $end_date_str . "' GROUP BY ask_date, charity_select ORDER BY ask_date asc, charity_select asc;  ";
	//$query = "select date_format(ask_date, '%d'), num_correct, charity_select from transactions_daily WHERE ask_date >= '" . $start_date_str . "' AND ask_date <= '" . $end_date_str . "' ORDER BY ask_date asc, charity_select asc;  ";
	$query = "select date_format(ask_date, '%d'), num_correct, transactions_daily.charity_select, offers_daily.amount from transactions_daily LEFT JOIN offers_daily ON transactions_daily.ask_date = offers_daily.date AND transactions_daily.charity_select = offers_daily.charity_select WHERE ask_date >= '" . $start_date_str . "' AND ask_date <= '" . $end_date_str . "' ORDER BY ask_date asc, charity_select asc;  ";
	
	//echo $query;
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());


	//$str_values[0] = $str_values[1] = $str_values[2] = $str_values[3] = $str_values[4] = "";
	//$day[0] = $day[1] = $day[2] = $day[3] = $day[4] = 0;
	for( $i = 0; $i < Common::c_num_charities; $i++ ) {
		$str_values[$i] = "";
		$day[$i] = 0;	
	}	
	
	$str_labels = "";
	$max_value = 0;
	$cs = -1;
	$last_date = 0;

	for( $i = 0; $i < mysql_num_rows($result); $i++ )
	{
		$row = mysql_fetch_array($result);

		if( $row[2] <= $cs )
		{
			// Day is done... write it out!			
			for( $c = 0; $c < Common::c_num_charities; $c++ )
			{
				
				if( $str_values[$c] != "" )
				{
					$str_values[$c] .= ',';
				}
				$str_values[$c] .= $day[$c];
			}

			// If there are too many days to show in the chart, only show every other day label... Otherwise it gets crowded
			if( mysql_num_rows($result) >= 15 && $last_date % 2 == 0 /*&& $last_date != mysql_num_rows($result)-1*/ )
				$str_labels .= '|';
			else
				$str_labels .= $last_date . '|';

			//$day[0] = $day[1] = $day[2] = $day[3] = $day[4] = 0;
			for( $k = 0; $k < Common::c_num_charities; $k++ ) {			
				$day[$k] = 0;	
			}
		}
		
		// Now also add in the amount earned by offers to get the true value
		$val = $row[1] * Game::c_ounces_earned_per_correct;
		$val += $row[3];

		$day[$row[2]] = $val;
		
		$last_date = $row[0];

		if( $val > $max_value )
			$max_value = $val;

		$cs = $row[2];
	}

	$max_value = $max_value + 100 - ($max_value % 100);
	//echo $max_value;

	//&amp;chm=D,0000FF,0,0,2,1

	// For some absurd reason facebook is inserting underscores between newlines for this URL...
	// So I need to shove it all in a single line... wtf???

	//echo '
	//<img src="http://chart.apis.google.com/chart?chs=' . $sizex . 'x' . $sizey . '&amp;chtt=Donations+This+Month&amp;chd=t:' . $str_values[0] . '|' . $str_values[1] . '|' . $str_values[2] . '|' . $str_values[3] . '&amp;chds=0,' . $max_value . '&amp;chdl=Water|Wheat|Education|Rainforest&amp;chco=0000FF,FF0000,FF00FF,00FF00,000000&amp&amp;cht=lc&amp;chxt=x,y,x&amp;chxl=0:|' . $str_labels . '1:|0|' . $max_value*10/2 . '|' . $max_value*10 . '|2:||Day"alt="Charitii donation chart" />
	//';
	
	// Note: CARE (id=1) is no longer a part of this
	echo '
	<img src="http://chart.apis.google.com/chart?chf=bg,s,00000000&amp;chs=' . $sizex . 'x' . $sizey . '&amp;chtt=Donations+This+Month&amp;chd=t:' . $str_values[0] . '|' . $str_values[5] . '|' . $str_values[3] . '|' . $str_values[2] . '|' . $str_values[4] . '&amp;chds=0,' . $max_value . '&amp;chdl=Charity:water|Invisible Youth Network|Nature Conservancy|Oaktree Foundation|Philippine Aid Society&amp;chco=0000FF,FF0000,00FF00,FF00FF,333333&amp;cht=lc&amp;chxt=x,y,x&amp;chxl=0:|' . $str_labels . '1:|0|' . $max_value/2 . '|' . $max_value . '|2:||Month"alt="Charitii donation chart" />
	';

}








function get_charity_name($d)
{
	$a = array();
	$a[0] = "charity: water";
	$a[1] = "CARE";
	$a[2] = "Oaktree Foundation";
	$a[3] = "The Nature Conservancy";
	$a[4] = "Philippine Aid Society";
	$a[5] = "Invisible Youth Network";
	
	if( isset($a[$d]) )
		return $a[$d];
	else
		return "???";
}

function get_charity_units($d)
{
	$a = array();
	$a[0] = "ounces of water";
	$a[1] = "grains of wheat";
	$a[2] = "minutes of education";
	$a[3] = "square inches of rainforest";
	
	if( isset($a[$d]) )
		return $a[$d];
	else
		return "points";
}
	




// http://php.net/manual/en/function.session-destroy.php
function session_restart()
{	
    if (session_name()=='') {    	
    	
        // Session not started yet
        session_start();     

        ///echo 'LOGOUT111!';        
    }
    else {   	
    	
        // Session was started, so destroy
        session_destroy();

        // But we do want a session started for the next request
        session_start();
        session_regenerate_id();

		// Kill the user cookie here?
		setcookie('user_id','',1);
		unset($_COOKIE['user_id']);
		
		///echo 'LOGOUT222!';
    }
}

// Used by the game and options.php
function set_word_lengths_from_cookie_value( $i )
{
	switch( $i )
	{
	case 0:
		$_SESSION['min_word_length'] = 0;
		$_SESSION['max_word_length'] = 0;
		break;
	case 1:
		$_SESSION['min_word_length'] = 0;
		$_SESSION['max_word_length'] = 5;
		break;
	case 2:
		$_SESSION['min_word_length'] = 6;
		$_SESSION['max_word_length'] = 0;
		break;
	}
}

function set_default_user_variables()
{
	$_SESSION['charity_select'] = rand(0,Common::c_num_charities-1);
	// CARE is outta here
	if( $_SESSION['charity_select'] == 1 ) {
		$_SESSION['charity_select'] = 5;
	}
				
	$_SESSION['num_correct'] = 0;
	$_SESSION['num_incorrect'] = 0;
	for( $i = 0; $i < Common::c_num_charities; $i++ ) {
		$_SESSION['donated_amount' . $i] = 0;	
	}
	
	// Clear out and init everything		
	$_SESSION['num_correct_in_a_row'] = 0;
	$_SESSION['num_incorrect_in_a_row'] = 0;
	$_SESSION['difficulty'] = 0;
	$_SESSION['num_missing_letters'] = 0;
	$_SESSION['min_word_length'] = 0;
	$_SESSION['max_word_length'] = 0;			
	
	// Stored options in cookies?
        if( isset($_COOKIE['difficulty']) )
	{
		if( $_COOKIE['difficulty'] != -1 )
		{
			$_SESSION['difficulty'] = $_COOKIE['difficulty'];
		}
	}

	if( isset($_COOKIE['num_missing_letters'] ))
	{
		if( $_COOKIE['num_missing_letters'] != -1 )
		{
			$_SESSION['num_missing_letters'] = $_COOKIE['num_missing_letters'];
		}	
	}

	if( isset($_COOKIE['word_length'] ))
	{
		set_word_lengths_from_cookie_value( $_COOKIE['word_length'] );
	}

}

// Generate a unique UID for this session
// Used by the superrewards + offerpal stuff (uid needed)
function create_new_anonymous_user()
{
	set_default_user_variables();
	
	$query = "INSERT INTO users (create_date, username, last_login_date) VALUES ( CURRENT_DATE(), NULL, CURRENT_DATE() );";
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	$_SESSION['user_id'] = mysql_insert_id();
	
	setcookie( 'user_id', $_SESSION['user_id'], time()+60*60*24*30 );
}

function user_refresh_session()
{
	// name
	// score
	// etc
}

function award_welcome_back_bonus($id)
{
	$amount = 50;
	
	// Insert a notification	
	$text = sprintf("You have received a welcome back bonus!<br/>%d %s for %s",
					$amount, get_charity_units($_SESSION['charity_select']), get_charity_name($_SESSION['charity_select']) );
	$text = mysql_real_escape_string($text);		
	$query = sprintf("INSERT INTO user_notifications (user_id, text, displayed) 
					values ('%d', '%s', FALSE)",
						$id, $text ); 
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
			
	// Increase user points
	$query = sprintf( "UPDATE user_scores set points=points+%d where user_id='%d' and charity='%d'",
						$amount, $id, $_SESSION['charity_select'] );							
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	
	// Increase charity transaction points		
	$query = sprintf( "INSERT INTO transactions_daily (ask_date, num_correct, num_respond, charity_select) 
		VALUES ( NOW(), %d, %d, %d )
		ON DUPLICATE KEY UPDATE num_correct=num_correct + %d, num_respond=num_respond + %d",
		$amount/10, $amount/10, $_SESSION['charity_select'], $amount/10, $amount/10 );				
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());

}

function user_login($id)
{
	set_default_user_variables();
	
	$query = "select *, CURRENT_DATE() from users where id = '" . $id . "';";
	$result = mysql_query($query) or die('Query failed: ' . mysql_error());
	if( mysql_num_rows($result) == 0 ) {
		// Couldn't find this user???
		session_restart();
	}
	else {
		$row = mysql_fetch_assoc($result);
		$_SESSION['user_id'] = $id;
		if( $row['username'] != NULL ) {
			// Only set this session variable for users that are not anonymous
			// This is how we know whether a user is anonymous or not!
			$_SESSION['user_name'] = $row['username'];
		}	
		if( $row['charity_select'] != NULL ) {	
			$_SESSION['charity_select'] = $row['charity_select'];
		}
		if( $row['num_correct'] != NULL ) {	
			$_SESSION['num_correct'] = $row['num_correct'];
		}
		if( $row['num_respond'] != NULL ) {	
			$_SESSION['num_respond'] = $row['num_respond'];
		}
		
		if( $row['num_correct'] >= 3 && $row['last_login_date'] != $row['CURRENT_DATE()'] ) {
			// If the user hasn't logged in for a day, give a welcome back bonus.
			// Also check that the user has actually played a few rounds of the game...
			award_welcome_back_bonus($id);
		}		
		

		// Update last logged in date
		$query = "UPDATE users set last_login_date = CURRENT_DATE() where id=".$id.";";
		$result = mysql_query($query) or die('Query failed: ' . mysql_error());		

	}
		
}

function user_logout()
{	
	// Don't bother if we're not actually logged in!
	if( !isset($_SESSION['user_name']) ) {
		return;
	}
	
	// Kill the cookie
	setcookie('user_id','',1);
	
	session_restart();
	
	// TB TODO - Go to the current page?
	// Is it even necessary to refresh?
	//header('Location:index.php');
	//exit();
}

?>
