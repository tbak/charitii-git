<?php 
// Copyright 2009, SuperDonate.  All rights reserved.
require ('minify_page_start.php');
require('common.php');
generic_page_start('banners');
?>

<h1>Banners</h1>

Charitii Flyer: <a href="banners/charitii_flyer.pdf">download here!</a><br/><br/>

<p>On this page are banners that you can use to link to Charitii.com. Please feel free to link directly to these images or copy them onto your own computer and use them in any way.</p>

<p>There are two ways you can link these banners to Charitii.com:</p>

<p><strong>#1</strong> Select the banner that you want to add to your site. Then copy the HTML code that is written below the banner into your web page.</p>

<p><strong>#2</strong> You can write your own HTML code to use these banner images. Right-click on the banner you have selected and choose "Save Image As..." or "Save Picture As...". Then link to http://www.charitii.com using the image.</p>

<p>Thank you for linking to Charitii!</p>

<br/><br/>

<table id="bannerTable" border="1" width="100%" cellspacing="0" cellpadding="15">

<tr valign="top">
<td>
<img src="banners/charitii_88_31.png" width="88" height="31" />
<br />
&lt;a href="http://www.charitii.com/"&gt;&lt;img src="http://www.charitii.com/banners/charitii_88_31.png" width="88" height="31" border="0" alt="charity word game puzzles"&gt;&lt;/a&gt;

</td>

<td>
<img src="banners/charitii_88_31_a.png" width="88" height="31" />
<br />
&lt;a href="http://www.charitii.com/"&gt;&lt;img src="http://www.charitii.com/banners/charitii_88_31_a.png" width="88" height="31" border="0" alt="free charity word game"&gt;&lt;/a&gt;
</td>
</tr>

<tr valign="top">
<td>
<img src="banners/charitii_120_60.png" width="120" height="60" />
<br />
&lt;a href="http://www.charitii.com/"&gt;&lt;img src="http://www.charitii.com/banners/charitii_120_60.png" width="120" height="60" border="0" alt="play free charity word game puzzles"&gt;&lt;/a&gt;
</td>

<td>
<img src="banners/charitii_120_240.png" width="120" height="240" />
<br />
&lt;a href="http://www.charitii.com/"&gt;&lt;img src="http://www.charitii.com/banners/charitii_120_240.png" width="120" height="240" border="0" alt="free donation word puzzles"&gt;&lt;/a&gt;
</td>

<tr valign="top">
<td>
<img src="banners/charitii_234_60.png" width="234" height="60" />
<br />
&lt;a href="http://www.charitii.com/"&gt;&lt;img src="http://www.charitii.com/banners/charitii_234_60.png" width="234" height="60" border="0" alt="free charity donation word games"&gt;&lt;/a&gt;
</td>

<td>
<img src="banners/charitii_308_155.png" width="308" height="155" />
<br />
&lt;a href="http://www.charitii.com/"&gt;&lt;img src="http://www.charitii.com/banners/charitii_308_155.png" width="308" height="155" border="0" alt="play free donation word puzzles"&gt;&lt;/a&gt;
</td>

<tr><td colspan="2">
<img src="banners/charitii_468_60.png" width="468" height="60" />
<br />
&lt;a href="http://www.charitii.com/"&gt;&lt;img src="http://www.charitii.com/banners/charitii_468_60.png" width="468" height="60" border="0" alt="free charity donation word game"&gt;&lt;/a&gt;
</td></tr>

<tr><td colspan="2">
<img src="banners/charitii_468_60_a.png" width="468" height="60" />
<br />
&lt;a href="http://www.charitii.com/"&gt;&lt;img src="http://www.charitii.com/banners/charitii_468_60_a.png" width="468" height="60" border="0" alt="play free charity word game"&gt;&lt;/a&gt;
</td></tr>

</table>

<?php 
generic_page_end();
$minify_file_name = __FILE__;
require 'minify_page_end.php';
?>
