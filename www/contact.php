<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require ('minify_page_start.php');
require('common.php');
class Contact
{
	public $token;
	public $did_submit = FALSE;

	// Report errors in input
	public $input_has_errors;
	public $error_name = 0;
	public $error_email = 0;
	public $error_captcha = 0;
	public $error_message = 0;

	private function check_token()
	{
		// Simple spam check
		// Check that the posted token field matches the token in the session. 
		if( isset($_SESSION['token']) )
		{
			if( !isset($_POST['token']) || $_SESSION['token'] != $_POST['token'] )
			{
				//echo "spammer!";
				// Person probably just hit the reload button on his browser... just act as though
				// nothing happened (no choice was made)
				unset($_POST['token']);
			}
		}
	
		// Generate the token for the next page game page load
		$secret = 'magicalspambustingnumpties';

		// Ensure that the new token is different than the current token
		$this->token = $_SESSION['token'];
		while( $_SESSION['token'] == $this->token )
		{
			$this->token = md5(rand(1, 1000).$secret);
		}
		$_SESSION['token'] = $this->token;
	}

	private function check_captcha()
	{
		$this->error_captcha = 0;

		if( !isset($_POST['contact_captcha']) || empty($_POST['contact_captcha']) )
		{
			$this->error_captcha = 1;
		}
		else if( !isset($_SESSION['security_code']) || $_POST['contact_captcha'] != $_SESSION['security_code'] )
		{
			$this->error_captcha = 1;
		}

		// TB - Reset the security code session variable so that you can only post once
		unset($_SESSION['security_code']);
	}

	private function check_email()
	{
		$this->error_email = 0;

        // Validate email field.
		// TB - This is snagged from http://formtoemail.com/FormToEmail.txt
		if(isset($_POST['contact_email']) && !empty($_POST['contact_email']))
		{
			$_POST['contact_email'] = trim($_POST['contact_email']);

			if( substr_count($_POST['contact_email'],"@") != 1 || stristr($_POST['contact_email']," "))
			{
				$this->error_email = 1;
			}
			else
			{
				$exploded_email = explode("@",$_POST['contact_email']);
				if( empty($exploded_email[0]) || strlen($exploded_email[0]) > 64 || empty($exploded_email[1]))
				{
					$this->error_email = 1;
				}
				else
				{
					if(substr_count($exploded_email[1],".") == 0)
					{
						$this->error_email = 1;
					}
					else 
					{
						$exploded_domain = explode(".",$exploded_email[1]);
						if(in_array("",$exploded_domain))
						{
							$this->error_email = 1;
						}
						else
						{
							foreach($exploded_domain as $value)
							{
								if(strlen($value) > 63 || !preg_match('/^[a-z0-9-]+$/i',$value))
                                {
									$this->error_email = 1;
									break;
								}
							}
						}
					}
				}
			}
		}
		else
		{
			$this->error_email = 1;
		}

	}
		
	private function check_message()
	{
		$this->error_message = 0;

		if( !isset($_POST['contact_message']) || empty($_POST['contact_message']) )
		{
			$this->error_message = 1;
		}
	}

	private function check_name()
	{
		$this->error_name = 0;

		if( !isset($_POST['contact_name']) || empty($_POST['contact_name']) )
		{
			$this->error_name = 1;
		}
	}

	function echo_previously_selected_reason( $reason )
	{
		if( isset($_POST['contact_reason']) && $_POST['contact_reason'] == $reason )
		{
			echo 'SELECTED = "TRUE"';
		}
	}

	public function send_email()
	{
		$my_email = 'tom@charitii.com';
		$subject = 'Charitii.com Message';
		$message = $_POST['contact_message'] . PHP_EOL . 'reason=' . $_POST['contact_reason'] . PHP_EOL;

		$from_name = '';
		if( isset($_POST['contact_name']) && !empty($_POST['contact_name']))
		{
			$from_name = stripslashes($_POST['contact_name']);
		}
        $headers = "From: {$from_name} <{$_POST['contact_email']}>";

        mail($my_email,$subject,$message,$headers);

		// TB TODO!
	}

	function echo_if_exist($name)
	{
		if( $this->did_submit && isset($_POST[$name]) )
		{
			echo $_POST[$name];
		}
		
	}

    function __construct() 
	{
		$this->check_token();

		if( isset($_POST['token']) )
		{
			$input_has_errors = FALSE;
			$this->did_submit = TRUE;

			// Check all the inputs for errors
			$this->check_captcha();
			$this->check_email();
			$this->check_message();
			$this->check_name();

			if( $this->error_captcha || $this->error_email || $this->error_message || $this->error_name )
			{ 
				$this->input_has_errors = TRUE;
			}
		}
	}

}

$contact = new Contact();
if( $contact->did_submit && !$contact->input_has_errors )
{
	// Jump to the thank you page!
	$contact->send_email();
	//require('contact_thankyou.php');
    header('Location:contact_thankyou.php');
	exit();
}

generic_page_start('contact');
?>

<h1>Contact</h1>

<p>Please check our <a href="faq.php">FAQ</a> page first for general questions about Charitii.</p>

<h3>Contact form</h3>

<p>
<?php
if( $contact->did_submit && $contact->input_has_errors )
{
	echo 'Contact form has errors.<br>Please correct them and try again.<br>';
}
else
{
	echo 'Please use this form to contact us and we will get back to you as soon as possible.<br>';
}
?>
</p>

<?php
$message_rows = 6;
$message_cols = 60;
if( $common->device_is_iphone )
{
	$message_rows = 4;
	$message_cols = 30;
}
?>

<form id="form_contact" name="form_contact" action="contact.php" method="post">
<input type=hidden name="token" value="<?php echo $contact->token ?>" />

<table border="0" bgcolor="#ececec" cellspacing="5">
<tr><td>Name <?php if($contact->error_name) echo '*error'; ?></td></tr>
<tr><td><input type="text" size="30" name="contact_name" value="<?php $contact->echo_if_exist('contact_name');?>"/></td></tr>
<tr><td>Email address <?php if($contact->error_email) echo '*error'; ?></td></tr>
<tr><td><input type="text" size="30" name="contact_email" value="<?php $contact->echo_if_exist('contact_email');?>"/></td></tr>
<tr><td>Reason for contact</td></tr>
<tr><td>
<select name="contact_reason">
<option value ="general" <?php $contact->echo_previously_selected_reason('general'); ?> >General</option>
<option value ="ads" <?php $contact->echo_previously_selected_reason('ads'); ?> >Advertising / Sponsorships</option>
<option value ="website" <?php $contact->echo_previously_selected_reason('website'); ?> >Website Problems</option>
</select>
</td></tr>
<tr><td valign="top">Message <?php if($contact->error_message) echo '*error'; ?></td></tr>
<tr><td><textarea name="contact_message" rows="<?php echo $message_rows; ?>" cols="<?php echo $message_cols; ?>"><?php $contact->echo_if_exist('contact_message');?></textarea></td></tr>
<tr><td>Please enter these numbers (spam detection) <?php if($contact->error_captcha) echo '*error'; ?></td></tr>
<tr><td><img src="captchasecurityimage.php" /></td></tr>
<tr><td><input type="text" name="contact_captcha" /></td></tr>
<tr><td><input type="submit" value="Send" /></td></tr>
</table>
</form>

<?php 
generic_page_end(); 
$minify_page_is_dynamic = TRUE;
$minify_file_name = __FILE__;
require 'minify_page_end.php';
?>
