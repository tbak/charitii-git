<?php
// Copyright 2009, SuperDonate.  All rights reserved.
require 'minify_page_start.php';
require('common.php');
connect_database();
generic_page_start('topsubmitters');
?>

<h2>Top Submitters</h2>

<?php
$query = "SELECT source_author, SUM(enabled) FROM puzzles WHERE source_title = 'OnlineSubmitTitle' GROUP BY source_author ORDER BY SUM(enabled) desc LIMIT 25";
$result = mysql_query($query) or die('Query failed: ' . mysql_error());

// Printing results in HTML
echo "<table>\n";
while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    echo "\t<tr>\n";
    foreach ($line as $col_value) {
        echo "\t\t<td>$col_value</td>\n";
    }
    echo "\t</tr>\n";
}
echo "</table>\n";
?>

<?php 
generic_page_end();
$minify_file_name = __FILE__;
require 'minify_page_end.php';












