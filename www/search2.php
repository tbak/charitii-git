<?php // Copyright 2009, SuperDonate.  All rights reserved. ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>My Google AJAX Search API Application</title>
    <script src="http://www.google.com/jsapi?key=ABQIAAAAF4MJYCS4AVEFkBRyKZvKchT1T-ysx_-kZiYzsg0fut4bts5FnhRQ50Nw02kjb67eF3Y46pkMLq_C9A" type="text/javascript"></script>
    <script language="Javascript" type="text/javascript">
    //<![CDATA[

    google.load("search", "1");

    function OnLoad() {
      // Create a search control
      var searchControl = new google.search.SearchControl();

      // Add in a full set of searchers
      //var localSearch = new google.search.LocalSearch();
      //searchControl.addSearcher(localSearch);

	  var ws = new google.search.WebSearch();
	  searchControl.addSearcher(ws);
      ws.setResultSetSize(google.search.Search.LARGE_RESULTSET);


      
	  searchControl.addSearcher(new google.search.ImageSearch());
      searchControl.addSearcher(new google.search.VideoSearch());
      searchControl.addSearcher(new google.search.BlogSearch());

      // Set the Local Search center point
      //localSearch.setCenterPoint("New York, NY");

      // Tell the searcher to draw itself and tell it where to attach
      //searchControl.draw(document.getElementById("searchcontrol"));

	// create a drawOptions object
	var drawOptions = new GdrawOptions();
	
	// tell the searcher to draw itself in linear mode
	drawOptions.setDrawMode(GSearchControl.DRAW_MODE_TABBED);
	searchControl.draw(document.getElementById("searchcontrol"), drawOptions);

      // Execute an inital search
      searchControl.execute("Google");
    }
    google.setOnLoadCallback(OnLoad);

    //]]>
    </script>


<style>

.gsc-control {
	width:600px;
}

/*
input {
	background-color: #99ccff;
	color: black;
	font-family: arial, verdana, ms sans serif;
	font-weight: bold;
	font-size: 12pt
}
*/

input.gsc-input {
	font-size: 36px;
	font-family:"Lucida Grande",Calibri,Arial,sans-serif;
	font-weight: bold;
}

  </style>


  </head>

  <body>


    <div id="searchcontrol">Loading...</div>
  </body>
</html>
